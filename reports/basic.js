var divIdCount = 0;

function nextId()
{
  return "div"+divIdCount++;
}

function displayTable(data)
{
  var i = 0;

  document.write('<script type="text/javascript" src="basic.js"></script>' +
'<link href="report.css" rel="stylesheet" type="text/css">');
  document.write("<div class='test-suite'>");
  for (i in data) {
    displayTest(data[i]);
  }
  document.write("</div>");
}

function console2html(out)
{
  out = out.replace(/\u001b\[m/g, "</font><font color='white'>");
  out = out.replace(/\u001b\[0;30m/g, "</font><font color='black'>");
  out = out.replace(/\u001b\[0;31m/g, "</font><font color='#cc6666'>");
  out = out.replace(/\u001b\[0;32m/g, "</font><font color='#33cc66'>");
  out = out.replace(/\u001b\[0;33m/g, "</font><font color='#cc9933'>");
  out = out.replace(/\u001b\[0;34m/g, "</font><font color='#3366cc'>");
  out = out.replace(/\u001b\[0;35m/g, "</font><font color='#cc33cc'>");
  out = out.replace(/\u001b\[0;36m/g, "</font><font color='#33cccc'>");
  out = out.replace(/\u001b\[0;37m/g, "</font><font color='#cccccc'>");
  out = out.replace(/\u001b\[0;38m/g, "</font><font color='#666666'>");
  out = out.replace(/\u001b\[1;30m/g, "</font><font color='black'>");
  out = out.replace(/\u001b\[1;31m/g, "</font><font color='#ff6666'>");
  out = out.replace(/\u001b\[1;32m/g, "</font><font color='#66ff66'>");
  out = out.replace(/\u001b\[1;33m/g, "</font><font color='#ffff66'>");
  out = out.replace(/\u001b\[1;34m/g, "</font><font color='#6699ff'>");
  out = out.replace(/\u001b\[1;35m/g, "</font><font color='#ff66ff'>");
  out = out.replace(/\u001b\[1;36m/g, "</font><font color='#33ffff'>");
  out = out.replace(/\u001b\[1;37m/g, "</font><font color='white'>");
  out = out.replace(/\u001b\[1;38m/g, "</font><font color='grey'>");
  return "<font color='white'>"+out+"</font>";
}

function displayTest(test)
{
  var detailsId = nextId();
  var childsId = nextId();

  document.write("<div class='test'>");
  if (test.succeed)
    document.write("<font color='green'>");
  else
    document.write("<font color='red'>");
  document.write("<a onclick='changeDisplayState(\""+childsId+"\")' style='cursor:pointer;'>"+test.name+"</a></font> <a onclick='changeDisplayState(\""+
                 detailsId+"\")' style='cursor:pointer;'>details</a><br/>");
  document.write("<div id='"+detailsId+"' style='display:none' ><table>");
  var prop;
  for (prop in test.details)
  {
    var x;

    if (prop == "stderr" || prop == 'stdout')
      x = "<td style='background-color:black'><pre>"+console2html(test.details[prop])+"</pre></td>";
    else
      x = "<td><pre>"+test.details[prop]+"</pre></td>";
    document.write("<tr><td>"+prop+"</td>"+x+"</tr>");
  }
  document.write("</table></div>");
  if (test.tests != null)
  {
    var i = 0;

    document.write("<div class='test-suite' id='"+childsId+"' class='childdiv'");
    if (test.succeed)
      document.write(" style='display:none'");
    document.write(">");
    for (i in test.tests) {
      displayTest(test.tests[i]);
    }
    document.write("</div>");
  }
  document.write('</div>');
}

function changeDisplayState(id)
{
  var div=document.getElementById(id);
  if (div.style.display == 'none' ||
      div.style.display == '') {
    div.style.display = 'block';
  } else {
    div.style.display = 'none';
  }
}

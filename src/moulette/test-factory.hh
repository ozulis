#ifndef MOULETTE_TEST_FACTORY_HH
# define MOULETTE_TEST_FACTORY_HH

# include <boost/filesystem/path.hpp>

namespace moulette
{
  class Testable;

  class TestFactory
  {
  public:
    static Testable * loadTest(const boost::filesystem::path & path);
    static Testable * create(const std::string & type);
  };
}

#endif /* !MOULETTE_TEST_FACTORY_HH */

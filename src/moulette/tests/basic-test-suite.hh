#ifndef MOULETTE_BASIC_TEST_SUITE_HH
# define MOULETTE_BASIC_TEST_SUITE_HH

# include <vector>

# include <moulette/test-suite.hh>
# include <moulette/test-result.hh>

namespace moulette
{
  class BasicTestSuite : public TestSuite
  {
  public:
    BasicTestSuite();
    ~BasicTestSuite();

    virtual void set(const std::string & key, const std::string & value);
    virtual const TestResult & result() const;
    virtual const std::string & type() const;
    void addTest(Testable * test);
    void addTest(const std::string & path);
    void addHereTest(const std::string & content);

  protected:
    virtual void test();

    std::vector<Testable *> tests_;
    TestResult result_;
  };
}

template class std::vector<moulette::Testable *>;

#endif /* !MOULETTE_BASIC_TEST_SUITE_HH */

#include <iostream>

#include <moulette/test-result.hh>
#include "dummy-test.hh"

namespace moulette
{
  void
  DummyTest::test()
  {
  }

  void
  DummyTest::set(const std::string & key, const std::string & value)
  {
    Test::set(key, value);
  }

  const TestResult &
  DummyTest::result() const
  {
    static TestResult result_;

    result_.succeed = true;
    return result_;
  }

  const std::string &
  DummyTest::type() const
  {
    static std::string type_("DummyTest");

    return type_;
  }
}

#include <sstream>

#include <ozulis/core/io-utils.hh>
#include "simple-process.hh"

namespace moulette
{
  SimpleProcess::SimpleProcess()
    : Test(),
      exitCode_(0),
      expectedExitCode_(0),
      process_()
  {
  }

  void
  SimpleProcess::test()
  {
    std::istream & out = process_.pipeStdout();
    std::istream & err = process_.pipeStderr();

    assert(&out);

    process_.run();
    exitCode_ = process_.wait();
    result_.succeed = (exitCode_ == expectedExitCode_);
    result_.details["exit-code"] = exitCode_;
    result_.details["expected-exit-code"] = expectedExitCode_;
    result_.details["command-line"] = process_.commandLine();

    std::string str;
    ozulis::core::readAll(out, str);
    result_.details["stdout"] = str;
    str.clear();
    ozulis::core::readAll(err, str);
    result_.details["stderr"] = str;
    process_.closeStreams();
  }

  void
  SimpleProcess::set(const std::string & key, const std::string & value)
  {
    if (key == "exit-code")
    {
      std::stringstream s(value, std::ios_base::in);
      s >> expectedExitCode_;
    }
    else if (key == "bin")
      process_.setBinary(value);
    else if (key == "arg")
      process_.addArg(value);
    else
      Test::set(key, value);
  }

  const TestResult &
  SimpleProcess::result() const
  {
    return result_;
  }

  const std::string &
  SimpleProcess::type() const
  {
    static std::string type_("SimpleProcess");

    return type_;
  }
}

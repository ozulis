#include <sstream>
#include <boost/foreach.hpp>

#include <moulette/tests/generated-compile-test.hh>

#include "expr-test-suite.hh"

namespace moulette
{
  typedef const char item_t[2][20];

  static const item_t unary_ops[] = {
    {"-", "neg"}
  };

  static const item_t unary_bitwise_ops[] = {
    {"!", "bang"},
    {"~", "not"}
  };

  static const item_t binary_ops[] = {
    {"+", "add"},
    {"-", "sub"},
    {"/", "div"},
    {"*", "mul"},
    {"%", "mod"},
    {"==", "eq"},
    {"!=", "ne"},
    {">", "gt"},
    {">=", "ge"},
    {"<", "lt"},
    {"<=", "le"}
  };

  static const item_t binary_bitwise_ops[] = {
    {"|", "or"},
    {"&", "and"},
    {"^", "xor"},
    {"&&", "andand"},
    {"||", "oror"},
    {"<<", "shl"},
    {">>", "lshr"},
    {">>>", "ashr"}
  };

  static const item_t int_types[] = {
    {"bool", "true"},
    {"int8", "13"},
    {"uint8", "13"},
    {"int16", "13"},
    {"uint16", "13"},
    {"int32", "13"},
    {"uint32", "13U"},
    {"int64", "13L"},
    {"uint64", "13UL"}
  };

  static const item_t float_types[] = {
    {"float", "0.2"},
    {"double", "0.42"}
  };

  static const item_t number_types[] = {
    {"bool", "true"},
    {"int8", "13"},
    {"uint8", "13"},
    {"int16", "13"},
    {"uint16", "13"},
    {"int32", "13"},
    {"uint32", "13U"},
    {"int64", "13L"},
    {"uint64", "13UL"},
    {"float", "0.2"},
    {"double", "0.42"}
  };

  ExprTestSuite::ExprTestSuite()
    : BasicTestSuite()
  {
    this->name_ = "expressions";
    Testable * tmp = 0;

    BasicTestSuite * bads = new BasicTestSuite;
    bads->set("name", "Bad expressions");
    tests_.push_back(bads);
    BasicTestSuite * goods = new BasicTestSuite;
    goods->set("name", "Good expressions");
    tests_.push_back(goods);

    BOOST_FOREACH (item_t & op, binary_ops)
      BOOST_FOREACH (item_t & type1, number_types)
      BOOST_FOREACH (item_t & type2, number_types)
      goods->addTest(createBinaryTest(op[0],
                                      type1[0], type1[1],
                                      type2[0], type2[1]));

    BOOST_FOREACH (item_t & op, binary_bitwise_ops)
      BOOST_FOREACH (item_t & type1, int_types)
      BOOST_FOREACH (item_t & type2, int_types)
      goods->addTest(createBinaryTest(op[0],
                                      type1[0], type1[1],
                                      type2[0], type2[1]));

    BOOST_FOREACH (item_t & op, binary_bitwise_ops)
      BOOST_FOREACH (item_t & type1, float_types)
      BOOST_FOREACH (item_t & type2, int_types)
    {
      tmp = createBinaryTest(op[0], type1[0], type1[1], type2[0], type2[1]);
      tmp->set("exit-code", "1");
      bads->addTest(tmp);
      tmp = createBinaryTest(op[0], type2[0], type2[1], type1[0], type1[1]);
      tmp->set("exit-code", "1");
      bads->addTest(tmp);
    }

    BOOST_FOREACH (item_t & op, unary_bitwise_ops)
    {
      BOOST_FOREACH (item_t & type, float_types)
      {
        tmp = createUnaryTest(op[0], type[0], type[1]);
        tmp->set("exit-code", "1");
        bads->addTest(tmp);
      }
      BOOST_FOREACH (item_t & type, int_types)
        goods->addTest(createUnaryTest(op[0], type[0], type[1]));
    }

    BOOST_FOREACH (item_t & op, unary_ops)
      BOOST_FOREACH (item_t & type, number_types)
      goods->addTest(createUnaryTest(op[0], type[0], type[1]));
  }

  GeneratedCompileTest *
  ExprTestSuite::createUnaryTest(const std::string & op,
                                 const std::string & type,
                                 const std::string & value)
  {
    std::stringstream ss;
    ss << op << " " << type;

    GeneratedCompileTest * test = new GeneratedCompileTest;
    test->set("name", ss.str());

    std::stringstream src;
    src << "int32 main()" << std::endl
        << "{" << std::endl
        << "  " << op << "cast(" << type << ", " << value << ");" << std::endl
        << "  return 0;" << std::endl
        << "}" << std::endl;

    test->generate("mgw", src.str());
    return test;
  }

  GeneratedCompileTest *
  ExprTestSuite::createBinaryTest(const std::string & op,
                                  const std::string & type1,
                                  const std::string & value1,
                                  const std::string & type2,
                                  const std::string & value2)
  {
    std::stringstream ss;
    ss << type1 << " " << op << " " << type2;

    GeneratedCompileTest * test = new GeneratedCompileTest;
    test->set("name", ss.str());

    std::stringstream src;
    src << "int32 main()" << std::endl
        << "{" << std::endl
        << "  " << "cast(" << type1 << ", " << value1 << ") "
        << op << " cast(" << type2 << ", " << value2 << ");" << std::endl
        << "  return 0;" << std::endl
        << "}" << std::endl;

    test->generate("mgw", src.str());
    return test;
  }

  const std::string &
  ExprTestSuite::type() const
  {
    static const std::string t("ExprTestSuite");
    return t;
  }
}

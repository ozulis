#ifndef MOULETTE_EXPR_TEST_SUITE_HH
# define MOULETTE_EXPR_TEST_SUITE_HH

# include <moulette/tests/basic-test-suite.hh>

namespace moulette
{
  class GeneratedCompileTest;

  class ExprTestSuite : public BasicTestSuite
  {
  public:
    ExprTestSuite();
    virtual const std::string & type() const;

  protected:
    static GeneratedCompileTest *
    createUnaryTest(const std::string & op,
                    const std::string & type,
                    const std::string & value);
    static GeneratedCompileTest *
    createBinaryTest(const std::string & op,
                     const std::string & type1,
                     const std::string & value1,
                     const std::string & type2,
                     const std::string & value2);
  };
}

#endif // MOULETTE_EXPR_TEST_SUITE_HH

#ifndef MOULETTE_SIMPLE_PROCESS_HH
# define MOULETTE_SIMPLE_PROCESS_HH

# include <ozulis/core/process.hh>

# include <moulette/test.hh>
# include <moulette/test-result.hh>

namespace moulette
{
  class SimpleProcess : public Test
  {
  public:
    SimpleProcess();

    virtual void set(const std::string & key, const std::string & value);
    virtual const TestResult & result() const;
    virtual const std::string & type() const;

  protected:
    virtual void test();

    // using 16 bits types to avoid confusion with char type with iostream
    // but theses values should not be bigger thant uint8_t.
    uint16_t exitCode_;
    uint16_t expectedExitCode_;
    ozulis::core::Process process_;
    TestResult result_;
  };
}

#endif /* !MOULETTE_SIMPLE_PROCESS_HH */

#include <sstream>
#include <fstream>

#include "generated-compile-test.hh"

namespace moulette
{
  GeneratedCompileTest::GeneratedCompileTest()
    : super_t()
  {
  }

  void
  GeneratedCompileTest::generate(const std::string & extension,
                                 const std::string & content)
  {
    std::stringstream ss;
    ss << "/tmp/" << rand() << "." << extension;
    std::ofstream of(ss.str().c_str());
    of << content;
    of.close();

    source_ = ss.str();
  }

  void
  GeneratedCompileTest::test()
  {
    super_t::test();
    boost::filesystem::remove(source_);
  }
}

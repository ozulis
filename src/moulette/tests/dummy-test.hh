#ifndef MOULETTE_DUMMY_TEST_HH
# define MOULETTE_DUMMY_TEST_HH

# include <moulette/test.hh>

namespace moulette
{
  class DummyTest : public Test
  {
  public:
    virtual void set(const std::string & key, const std::string & value);
    virtual const TestResult & result() const;
    virtual const std::string & type() const;

  protected:
    virtual void test();
  };
}

#endif /* !MOULETTE_DUMMY_TEST_HH */

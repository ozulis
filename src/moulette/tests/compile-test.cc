#include <fstream>

#include <ozulis/core/io-utils.hh>
#include "compile-test.hh"

namespace moulette
{
  CompileTest::CompileTest()
    : super_t(),
      source_()
  {
  }

  void
  CompileTest::test()
  {
    process_.setBinary(OZULIS_BINARY);
    process_.addArg(source_.string());

    super_t::test();

    // get the source content
    std::ifstream is(source_.string().c_str());
    std::string data;
    ozulis::core::readAll(is, data);
    is.close();

    // get the .ll content
    boost::filesystem::path llPath(source_);
    llPath.replace_extension("ll");
    is.open(llPath.string().c_str());
    std::string llvmAsm;
    ozulis::core::readAll(is, llvmAsm);
    is.close();

    // remove the .ll file
    boost::filesystem::remove(llPath);
    assert(!boost::filesystem::exists(llPath));

    // remove the .bc file
    boost::filesystem::path bcPath(source_);
    bcPath.replace_extension("bc");
    boost::filesystem::remove(bcPath);
    assert(!boost::filesystem::exists(bcPath));

    // remove the binary file
    boost::filesystem::path binPath(source_);
    binPath.replace_extension();
    boost::filesystem::remove(binPath);
    assert(!boost::filesystem::exists(binPath));

    result_.details["source"] = data;
    result_.details["source-path"] = source_.string();
    result_.details["assembly"] = llvmAsm;
    result_.details["assembly-path"] = llPath.string();
  }

  void
  CompileTest::set(const std::string & key, const std::string & value)
  {
    if (key == "source")
      setSource(value);
    else
      super_t::set(key, value);
  }

  void
  CompileTest::setSource(const std::string & source)
  {
    source_ = path.parent_path() / source;
  }

  const std::string &
  CompileTest::type() const
  {
    static std::string type_("CompileTest");

    return type_;
  }
}

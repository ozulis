#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <moulette/test-factory.hh>
#include <moulette/test-parser.hh>
#include "basic-test-suite.hh"

namespace moulette
{
  BasicTestSuite::BasicTestSuite()
    : TestSuite(),
      tests_(),
      result_()
  {
  }

  BasicTestSuite::~BasicTestSuite()
  {
    BOOST_FOREACH(Testable * t, tests_)
      delete t;
  }

  void
  BasicTestSuite::test()
  {
    uint64_t succeed = 0;
    uint64_t total = 0;

    BOOST_FOREACH (Testable * t, tests_)
    {
      t->run();
      if (t->isTestSuite())
      {
        succeed += boost::get<int64_t>(t->result().details.find("succeed")->second);
        total += boost::get<int64_t>(t->result().details.find("total")->second);
      }
      else
      {
        succeed += t->result().succeed;
        total++;
      }
    }
    result_.succeed = (succeed == total);
    result_.details["succeed"] = succeed;
    result_.details["total"] = total;
  }

  void
  BasicTestSuite::set(const std::string & key, const std::string & value)
  {
    if (key == "add-test")
      addTest(value);
    else if (key == "here-test")
      addHereTest(value);
    else
      TestSuite::set(key, value);
  }

  void
  BasicTestSuite::addTest(Testable * _test)
  {
    assert(_test);
    tests_.push_back(_test);
  }

  void
  BasicTestSuite::addTest(const std::string & _path)
  {
    boost::filesystem::path p = path.parent_path() / _path;
    Testable * t = TestFactory::loadTest(p);
    assert(t);
    tests_.push_back(t);
  }

  void
  BasicTestSuite::addHereTest(const std::string & content)
  {
    TestParser parser;
    Testable * t = parser.parseBuffer(content, path);
    assert(t);
    tests_.push_back(t);
  }

  const TestResult &
  BasicTestSuite::result() const
  {
    return result_;
  }

  const std::string &
  BasicTestSuite::type() const
  {
    static std::string type_("BasicTestSuite");

    return type_;
  }
}

#ifndef MOULETTE_COMPILE_TEST_HH
# define MOULETTE_COMPILE_TEST_HH

# include <boost/filesystem.hpp>
# include <moulette/tests/simple-process.hh>

namespace moulette
{
  class CompileTest : public SimpleProcess
  {
  public:
    typedef SimpleProcess super_t;

    CompileTest();

    virtual void set(const std::string & key, const std::string & value);
    virtual const std::string & type() const;

    void setSource(const std::string & path);

  protected:
    virtual void test();
    boost::filesystem::path source_;
  };
}

#endif /* !MOULETTE_COMPILE_TEST_HH */

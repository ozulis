#ifndef MOULETTE_GENERATED_COMPILE_TEST_HH
# define MOULETTE_GENERATED_COMPILE_TEST_HH

# include <moulette/tests/compile-test.hh>

namespace moulette
{
  class GeneratedCompileTest : public CompileTest
  {
  public:
    typedef CompileTest super_t;

    GeneratedCompileTest();

    void generate(const std::string & extension,
                  const std::string & content);

  protected:
    virtual void test();
  };
}

#endif /* !MOULETTE_GENERATED_COMPILE_TEST_HH */

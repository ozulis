#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <ozulis/core/assert.hh>
#include <moulette/test-factory.hh>
#include <moulette/testable.hh>
#include <moulette/listener.hh>
#include "moulette.hh"

template class std::vector<moulette::Testable *>;
template class ozulis::core::Singleton<moulette::Moulette>;

namespace moulette
{
  Moulette::Moulette()
    : ozulis::core::Singleton<Moulette>(),
      testStarting(),
      testFinished(),
      finished(),
      tests_()
  {
  }

  Moulette::~Moulette()
  {
    BOOST_FOREACH(Testable * test, tests_)
      delete test;
  }

  void
  Moulette::addTest(const boost::filesystem::path & path)
  {
    Testable * test = TestFactory::loadTest(path);
    if (test)
      addTest(test);
  }

  void
  Moulette::addTest(Testable * test)
  {
    assert(test);
    tests_.push_back(test);
  }

  void
  Moulette::run()
  {
    BOOST_FOREACH(Testable * test, tests_)
      test->run();
    finished();
  }

  void
  Moulette::registerListener(Listener & listener)
  {
    testStarting.connect(boost::bind(&Listener::testStarting, &listener, _1));
    testFinished.connect(boost::bind(&Listener::testFinished, &listener, _1));
    finished.connect(boost::bind(&Listener::finished, &listener));
  }
}

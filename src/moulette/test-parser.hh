#ifndef MOULETTE_TEST_PARSER_HH
# define MOULETTE_TEST_PARSER_HH

# include <boost/filesystem.hpp>

namespace moulette
{
  class Testable;

  class TestParser
  {
  public:
    TestParser();

    static Testable * parseFile(const boost::filesystem::path & path);
    static Testable * parseBuffer(const std::string & buffer,
                                  const boost::filesystem::path & path);

    boost::filesystem::path path;
    Testable * result;
  };
}

#endif /* !MOULETTE_TEST_PARSER_HH */

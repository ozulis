#ifndef MOULETTE_TEST_SUITE_HH
# define MOULETTE_TEST_SUITE_HH

# include "testable.hh"

namespace moulette
{
  class TestSuite : public Testable
  {
  public:
    virtual bool isTestSuite() const;
  };
}

#endif /* !MOULETTE_TEST_SUITE_HH */

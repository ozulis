#ifndef MOULETTE_TEST_HH
# define MOULETTE_TEST_HH

# include "testable.hh"

namespace moulette
{
  class Test : public Testable
  {
  public:
    virtual bool isTestSuite() const;
  };
}

#endif /* !MOULETTE_TEST_HH */

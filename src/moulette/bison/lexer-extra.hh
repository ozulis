#ifndef MOULETTE_LEXER_EXTRA_HH
# define MOULETTE_LEXER_EXTRA_HH

namespace moulette
{
  struct LexerExtra
  {
    LexerExtra() : delim(0), delim_len(0) {}

    char *delim;
    int delim_len;
  };
}

#endif /* !MOULETTE_LEXER_EXTRA_HH */

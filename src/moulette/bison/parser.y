%glr-parser
%pure-parser
%locations
%start file
%defines
%error-verbose
%name-prefix "moulette_"

%lex-param {yyscan_t yyscanner}
%parse-param {yyscan_t yyscanner}
%parse-param {moulette::TestParser & ctx}

%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <iostream>
#include <algorithm>

#include <ozulis/core/assert.hh>
#include <moulette/testable.hh>
#include <moulette/test-parser.hh>
#include <moulette/test-factory.hh>

#include "parser.hh"
#include "lexer-extra.hh"
#include "lexer.hh"

  static void yyerror(YYLTYPE                *yyloccp,
                      yyscan_t              /*yyscanner*/,
                      moulette::TestParser &  ctx,
                      const char             *str)
  {
    std::cerr << ctx.path << ":"
              << yyloccp->first_line << "." << yyloccp->first_column << "-"
              << yyloccp->last_line << "." << yyloccp->last_column
              << ": " << str << std::endl;
  }

%}

%union
{
  char * string;
}

%token <string> ID VALUE
%token TEST "test"

%%

file: test_type entries;

test_type: "test" '=' VALUE {
  ctx.result = moulette::TestFactory::create($3);
  assert(ctx.result);
  ctx.result->path = ctx.path;
  free($3);
};

entries: entries entry
| /* epsilon */;

entry: ID '=' VALUE {
  ctx.result->set($1, $3);
  free($1);
  free($3);
}

%%

%option reentrant noyywrap batch
%option bison-bridge bison-locations yylineno
%option warn ansi-definitions ansi-prototypes
%option prefix="moulette_"

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ozulis/core/assert.hh>
#include "parser.hh"
#include "lexer-extra.hh"

#define YY_USER_ACTION                                  \
   yylloc->first_line = yylloc->last_line;              \
   yylloc->last_line = yylineno;                        \
   yylloc->first_column = yylloc->last_column;          \
   yylloc->last_column += yyleng;

%}

%option extra-type="moulette::LexerExtra *"

%x key
%x value
%x assign
%x heredoc

ID                      [-a-zA-Z_0-9\.]+
WP                      [ \t]

%%

<INITIAL>test           BEGIN(assign); return TEST;
<key>{ID}               BEGIN(assign); yylval->string = strdup(yytext); return ID;
<key,INITIAL>{WP}+      /* eat */;
<key>\n                 /* eat */ yylloc->last_column = 0;

<assign>{WP}*           /* eat */
<assign>={WP}*          BEGIN(value); return '=';
<assign>"<<".*$         {
  BEGIN(heredoc);
  moulette::LexerExtra * extra = yyget_extra(yyscanner);
  extra->delim = strdup(yytext + 2);
  extra->delim_len = strlen(extra->delim);
  return '=';
 }

<heredoc>^.*$           {
  moulette::LexerExtra * extra = yyget_extra(yyscanner);
  if (strcmp(yytext + yyleng - extra->delim_len, extra->delim))
    yymore();
  else
  {
    BEGIN(key);
    yylval->string = strndup(yytext + 1, yyleng - extra->delim_len - 1);
    free(extra->delim);
    return VALUE;
  }
 }
<heredoc>\n             yymore(); yylloc->last_column = 0;

<value>.*$              BEGIN(key); yylval->string = strdup(yytext); return VALUE;

<key,INITIAL,assign>.   return 'z'; // error

%%

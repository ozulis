#ifndef MOULETTE_LISTENER_HH
# define MOULETTE_LISTENER_HH

namespace moulette
{
  class Testable;

  class Listener
  {
  public:
    virtual void testStarting(const Testable * test);
    virtual void testFinished(const Testable * test);
    virtual void finished();
  };
}

#endif /* !MOULETTE_LISTENER_HH */

#ifndef MOULETTE_MOULETTE_HH
# define MOULETTE_MOULETTE_HH

# include <boost/signals2.hpp>
# include <boost/filesystem/path.hpp>

# include <ozulis/core/singleton.hh>

namespace moulette
{
  class Testable;
  class Listener;

  class Moulette : public ozulis::core::Singleton<Moulette>
  {
  public:
    Moulette();
    ~Moulette();

    void addTest(const boost::filesystem::path & path);
    void addTest(Testable * test);

    void run();
    void registerListener(Listener & listener);

    boost::signals2::signal<void (const Testable *)> testStarting;
    boost::signals2::signal<void (const Testable *)> testFinished;
    boost::signals2::signal<void ()> finished;

  private:
    std::vector<Testable *> tests_;
  };
}

extern template class std::vector<moulette::Testable *>;
extern template class ozulis::core::Singleton<moulette::Moulette>;

#endif /* !MOULETTE_MOULETTE_HH */

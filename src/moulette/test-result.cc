#include "test-result.hh"

template class boost::variant<std::string, int64_t>;
template class std::map<std::string, moulette::TestResult::variant_t>;

namespace moulette
{
  TestResult::TestResult()
    : succeed(0)
  {
  }
}

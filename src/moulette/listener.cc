#include "listener.hh"

namespace moulette
{
  void
  Listener::testStarting(const Testable * /*test*/)
  {
  }

  void
  Listener::testFinished(const Testable * /*test*/)
  {
  }

  void
  Listener::finished()
  {
  }
}

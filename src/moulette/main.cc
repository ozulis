#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/foreach.hpp>

#include <config.hh>
#include <moulette/moulette.hh>
#include <moulette/listeners/console-listener.hh>
#include <moulette/listeners/xml-listener.hh>
#include <moulette/listeners/json-listener.hh>

namespace po = boost::program_options;

int main(int argc, char **argv)
{
  po::options_description desc("Help");
  desc.add_options()
    ("help,h", _("produce help message"))
    ("out-xml", po::value<std::string>(), _("xml output file"))
    ("out-json", po::value<std::string>(), _("json output file"))
    ("tests,t", po::value< std::vector<std::string> >(), _("test files"));

  po::positional_options_description pdesc;
  pdesc.add("tests", -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
            options(desc).positional(pdesc).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 0;
  }

  moulette::Moulette & moul = moulette::Moulette::instance();
  if (vm.count("tests"))
  {
    BOOST_FOREACH(const std::string & filename,
                  vm["tests"].as<std::vector<std::string> >())
      moul.addTest(filename);
  }

  moul.registerListener(*new moulette::ConsoleListener);
  if (vm.count("out-xml")) {
    std::ofstream * out = new std::ofstream(vm["out-xml"].as<std::string>().c_str());
    assert(out->good());
    moul.registerListener(*new moulette::XmlListener(*out));
  }

  if (vm.count("out-json")) {
    std::ofstream * out =
        new std::ofstream(vm["out-json"].as<std::string>().c_str());
    assert(out->good());
    moul.registerListener(*new moulette::JsonListener(*out));
  }

  moul.run();

  return 0;
}

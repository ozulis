#ifndef MOULETTE_TESTABLE_HH
# define MOULETTE_TESTABLE_HH

# include <string>
# include <boost/filesystem/path.hpp>

# include <moulette/test-result.hh>

namespace moulette
{
  class Testable
  {
  public:
    Testable();
    virtual ~Testable() {}

    void run();
    virtual bool isTestSuite() const = 0;
    virtual void set(const std::string & key, const std::string & value);
    virtual const TestResult & result() const = 0;
    virtual const std::string & name() const;
    virtual const std::string & type() const = 0;

    boost::filesystem::path path;

  protected:
    virtual void test() = 0;

    std::string name_;
  };
}

#endif /* !MOULETTE_TESTABLE_HH */

#include <iostream>
#include <sstream>

#include <moulette/testable.hh>
#include "json-listener.hh"

namespace moulette
{
  JsonListener::JsonListener(std::ostream & out)
    : Listener(),
      root_(),
      arrays_(),
      objects_(),
      out_(out)
  {
    arrays_.push_back(&root_);
  }

  void
  JsonListener::testStarting(const Testable * test)
  {
    JAULA::Value_Object * object = new JAULA::Value_Object();
    objects_.push_back(object);

    object->insertItem("name", JAULA::Value_String(test->name()));
    object->insertItem("type", JAULA::Value_String(test->type()));
    object->insertItem("path", JAULA::Value_String(test->path.string()));
    if (test->isTestSuite())
    {
      JAULA::Value_Array * array = new JAULA::Value_Array;
      arrays_.push_back(array);
    }
  }

  void
  JsonListener::testFinished(const Testable * test)
  {
    const TestResult & result(test->result());
    JAULA::Value_Object * object = objects_.back();

    object->insertItem("succeed", JAULA::Value_Boolean(result.succeed));

    JAULA::Value_Object * details = new JAULA::Value_Object;
    for (TestResult::details_t::const_iterator it = result.details.begin();
         it != result.details.end(); ++it)
    {
      std::stringstream ss;

      ss << it->second;
      details->insertItem(it->first, JAULA::Value_String(ss.str()));
    }
    object->insertItem("details", *details);
    delete details;

    objects_.pop_back();
    if (test->isTestSuite())
    {
      object->insertItem("tests", *arrays_.back());
      delete arrays_.back();
      arrays_.pop_back();
    }
    arrays_.back()->addItem(*object);
    delete object;
  }

  void
  JsonListener::finished()
  {
    root_.repr(out_);
    out_.flush();
  }
}

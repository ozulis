#include <iostream>
#include <sstream>
#include <rapidxml_print.hpp>

#include <moulette/testable.hh>
#include "xml-listener.hh"

template class rapidxml::xml_node<>;
template class rapidxml::xml_document<>;

namespace moulette
{
  XmlListener::XmlListener(std::ostream & out)
    : Listener(),
      doc_(),
      node_(&doc_),
      out_(out)
  {
  }

  void
  XmlListener::testStarting(const Testable * test)
  {
    rapidxml::xml_attribute<> * attr = 0;
    rapidxml::xml_node<> * node =
      doc_.allocate_node(rapidxml::node_element,
                         test->isTestSuite() ? "TestSuite" : "Test");
    node_->append_node(node);

    attr = doc_.allocate_attribute("name", test->name().c_str());
    node->append_attribute(attr);
    attr = doc_.allocate_attribute("type", test->type().c_str());
    node->append_attribute(attr);
    attr = doc_.allocate_attribute("path", test->path.string().c_str());
    node->append_attribute(attr);

    node_ = node;
  }

  void
  XmlListener::testFinished(const Testable * test)
  {
    const TestResult & result(test->result());
    rapidxml::xml_attribute<> * attr = 0;

    attr = doc_.allocate_attribute("succeed", result.succeed ? "true" : "false");
    node_->append_attribute(attr);

    rapidxml::xml_node<> * child =
      doc_.allocate_node(rapidxml::node_element, "Details");
    for (TestResult::details_t::const_iterator it = result.details.begin();
         it != result.details.end(); ++it)
    {
      std::stringstream ss;

      ss << it->second;
      attr = doc_.allocate_attribute(it->first.c_str(),
                                     doc_.allocate_string(ss.str().c_str()));
      child->append_attribute(attr);
    }
    node_->append_node(child);

    node_ = node_->parent();
  }

  void
  XmlListener::finished()
  {
    out_ << doc_;
    out_.flush();
  }
}

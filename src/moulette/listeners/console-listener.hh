#ifndef MOULETTE_CONSOLE_LISTENER_HH
# define MOULETTE_CONSOLE_LISTENER_HH

# include <moulette/listener.hh>

namespace moulette
{
  class ConsoleListener : public Listener
  {
  public:
    ConsoleListener();

    virtual void testStarting(const Testable * test);
    virtual void testFinished(const Testable * test);

  protected:
    void putSpaces();
    int depth_;
  };
}

#endif /* !MOULETTE_CONSOLE_LISTENER_HH */

#include <iostream>

#include <moulette/testable.hh>
#include "console-listener.hh"

namespace moulette
{
  ConsoleListener::ConsoleListener()
    : Listener(),
      depth_(0)
  {
  }

  void
  ConsoleListener::testStarting(const Testable * test)
  {
    assert(test);
    putSpaces();
    if (test->isTestSuite())
    {
      std::cout << "\e[1;34m*\e[m " << test->name() << " test suite:" << std::endl;
      depth_++;
    }
    else
      std::cout << "\e[0;34m*\e[m " << test->name() << " ";
  }

  void
  ConsoleListener::testFinished(const Testable * test)
  {
    const TestResult & result = test->result();

    if (!test->isTestSuite())
      std::cout << (result.succeed ? "\e[1;32mOK\e[m" :
                    "\e[1;31mKO\e[m")
                << std::endl;
    else
    {
      int64_t succeed = boost::get<int64_t>(result.details.find("succeed")->second);
      int64_t total = boost::get<int64_t>(result.details.find("total")->second);

      depth_--;
      putSpaces();
      std::cout << "\e[1;34m-\e[m " << test->name() << ": \e[1;31m"
                << total - succeed << "\e[m failed, \e[1;32m" << succeed
                << "\e[m succeed." << std::endl;
    }
  }

  void
  ConsoleListener::putSpaces()
  {
    for (int i = 0; i < depth_; i++)
      std::cout << " ";
  }
}

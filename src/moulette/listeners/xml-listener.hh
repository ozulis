#ifndef MOULETTE_XML_LISTENER_HH
# define MOULETTE_XML_LISTENER_HH

# include <ostream>
# include <rapidxml.hpp>

# include <moulette/listener.hh>

namespace moulette
{
  class XmlListener : public Listener
  {
  public:
    XmlListener(std::ostream & out);

    virtual void testStarting(const Testable * test);
    virtual void testFinished(const Testable * test);
    virtual void finished();

  protected:
    rapidxml::xml_document<> doc_;
    rapidxml::xml_node<> * node_;
    std::ostream & out_;
  };
}

extern template class rapidxml::xml_node<>;
extern template class rapidxml::xml_document<>;

#endif /* !MOULETTE_XML_LISTENER_HH */

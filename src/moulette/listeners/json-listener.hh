#ifndef MOULETTE_JSON_LISTENER_HH
# define MOULETTE_JSON_LISTENER_HH

# include <ostream>
# include <vector>

# include <jaula.h>

# include <moulette/listener.hh>

namespace moulette
{
  class JsonListener : public Listener
  {
  public:
    JsonListener(std::ostream & out);

    virtual void testStarting(const Testable * test);
    virtual void testFinished(const Testable * test);
    virtual void finished();

  protected:
    JAULA::Value_Array root_;
    std::vector<JAULA::Value_Array *> arrays_;
    std::vector<JAULA::Value_Object *> objects_;
    std::ostream & out_;
  };
}

#endif /* !MOULETTE_json_LISTENER_HH */

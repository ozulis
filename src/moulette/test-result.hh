#ifndef MOULETTE_TEST_RESULT_HH
# define MOULETTE_TEST_RESULT_HH

# include <map>
# include <string>
# include <boost/variant.hpp>

namespace moulette
{
  class TestResult
  {
  public:
    TestResult();

    typedef boost::variant<std::string, int64_t> variant_t;
    typedef std::map<std::string, variant_t>     details_t;

    bool      succeed;
    details_t details;
  };
}

extern template class boost::variant<std::string, int64_t>;
extern template class std::map<std::string, moulette::TestResult::variant_t>;

#endif // MOULETTE_TEST_RESULT_HH

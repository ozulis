#include <config.hh>
#include <ozulis/core/assert.hh>
#include <moulette/bison/parser.hh>
#include <moulette/bison/lexer-extra.hh>
#include <moulette/bison/lexer.hh>
#include "test-parser.hh"

int moulette_parse(yyscan_t yyscanner, moulette::TestParser & ctx);

namespace moulette
{
  TestParser::TestParser()
    : path(),
      result(0)
  {
  }

  Testable *
  TestParser::parseBuffer(const std::string & buffer,
                          const boost::filesystem::path & path)
  {
    TestParser parser;
    yyscan_t   scanner;
    LexerExtra extra;

    parser.path = path;
    moulette_lex_init_extra(&extra, &scanner);
    moulette__scan_string(buffer.c_str(), scanner);
    assert_msg(!moulette_parse(scanner, parser), _("parse error"));
    moulette_lex_destroy(scanner);
    return parser.result;
  }

  Testable *
  TestParser::parseFile(const boost::filesystem::path & path)
  {
    if (!boost::filesystem::is_regular_file(path))
      return 0;

    TestParser parser;
    parser.path = path;

    yyscan_t scanner;
    LexerExtra extra;
    moulette_lex_init_extra(&extra, &scanner);

    FILE * stream = fopen(path.string().c_str(), "r");
    assert(stream);
    moulette_set_in(stream, scanner);
    assert_msg(!moulette_parse(scanner, parser), _("parse error"));
    moulette_lex_destroy(scanner);
    return parser.result;
  }
}

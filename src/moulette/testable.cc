#include <config.hh>
#include <ozulis/core/assert.hh>
#include <moulette/moulette.hh>
#include "testable.hh"

namespace moulette
{
  Testable::Testable()
    : path(),
      name_("Unamed test")
  {
  }

  const std::string &
  Testable::name() const
  {
    return name_;
  }

  void
  Testable::set(const std::string & key, const std::string & value)
  {
    if (key == "name")
      name_ = value;
    else
      assert_msg(false, _("unknown key `%s' (with value: `%s')."),
                 key.c_str(), value.c_str());
  }

  void
  Testable::run()
  {
    Moulette & moul = Moulette::instance();
    moul.testStarting(this);
    test();
    moul.testFinished(this);
  }
}

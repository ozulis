#include <fstream>
#include <iostream>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <moulette/test-parser.hh>
#include <moulette/tests/basic-test-suite.hh>
#include <moulette/tests/compile-test.hh>
#include <moulette/tests/dummy-test.hh>
#include <moulette/tests/expr-test-suite.hh>
#include <moulette/tests/simple-process.hh>
#include "test-factory.hh"

namespace moulette
{
  Testable *
  TestFactory::loadTest(const boost::filesystem::path & path)
  {
    assert(boost::filesystem::is_regular_file(path));

    return TestParser::parseFile(path);
  }

  Testable *
  TestFactory::create(const std::string & type)
  {
    if (type == "dummy")
      return new DummyTest;
    if (type == "simple-process")
      return new SimpleProcess;
    if (type == "test-suite")
      return new BasicTestSuite;
    if (type == "compile-test")
      return new CompileTest;
    if (type == "expr-test-suite")
      return new ExprTestSuite;
    std::cerr << "test not found: " << type << std::endl;
    assert(false);
  }
}

#define TARGET_DATA_HELPER(Type)                \
  inline const TargetData::TypeAlignment *      \
  TargetData::find##Type(uint8_t size) const    \
  {                                             \
    return find(TypeAlignment::Type, size);     \
  }

namespace ozulis
{
  TARGET_DATA_HELPER(Integer)
  TARGET_DATA_HELPER(Float)
  TARGET_DATA_HELPER(Vector)
  TARGET_DATA_HELPER(Agregate)
}

#include "parse-task.hh"
#include <ozulis/compiler.hh>

namespace ozulis
{
  ParseTask::~ParseTask()
  {
  }

  void
  ParseTask::accept(Compiler & compiler)
  {
    compiler.work(*this);
  }

  std::vector<Task *> *
  ParseTask::dependancies()
  {
    return 0;
  }
}

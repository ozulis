#include <ozulis/core/multi-method.hh>
#include <ozulis/ast/ast-cast.hh>
#include "cast-tables.hh"

#define DUMMY_CAST(Type1, Type2)                \
  static CastExp *                              \
  cast##Type1##Type2(Type * /*type1*/,          \
                     Type * /*type2*/)          \
  {                                             \
    return 0;                                   \
  }

#define TRIVIAL_CAST(Type1, Type2)                      \
  static CastExp *                                      \
  cast##Type1##Type2(Type * type1,                      \
                     Type * type2)                      \
  {                                                     \
    CastExp * castExp = new CastExp();                  \
                                                        \
    if (type1->nodeType == Type2##Type::nodeTypeId())   \
      castExp->type = type1;                            \
    else                                                \
      castExp->type = type2;                            \
    return castExp;                                     \
  }

#define ADD_SYMETRIC_ENTRY(Type1, Type2)       \
  table.addEntry(cast##Type1##Type2,           \
                 Type1##Type::nodeTypeId(),    \
                 Type2##Type::nodeTypeId());   \
  table.addEntry(cast##Type1##Type2,           \
                 Type2##Type::nodeTypeId(),    \
                 Type1##Type::nodeTypeId());

namespace ozulis
{
  namespace ast
  {
    typedef CastExp * (*castFunc_t)(Type * type1, Type * type2);
    typedef core::DispatchTable<castFunc_t, id_t, id_t> castTable_t;

    static castTable_t table;

    DUMMY_CAST(Void, Void)
    DUMMY_CAST(Bool, Bool)
    DUMMY_CAST(Float, Float)
    DUMMY_CAST(Double, Double)

    TRIVIAL_CAST(Bool, Integer)
    TRIVIAL_CAST(Bool, Float)
    TRIVIAL_CAST(Bool, Double)
    TRIVIAL_CAST(Integer, Float)
    TRIVIAL_CAST(Integer, Double)
    TRIVIAL_CAST(Integer, Pointer)
    TRIVIAL_CAST(Float, Double)

    static CastExp *
    castIntegerInteger(Type * type1,
                       Type * type2)
    {
      CastExp *     castExp = 0;
      IntegerType * itype1 = ast_cast<IntegerType *> (type1);
      IntegerType * itype2 = ast_cast<IntegerType *> (type2);

      if (itype1->size == itype2->size && itype1->isSigned == itype2->isSigned)
        return 0;
      castExp = new CastExp();
      if (itype1->size > itype2->size)
        castExp->type = itype1;
      else
        castExp->type = itype2;
      return castExp;
    }

    static CastExp *
    castPointerPointer(Type * type1, Type * type2)
    {
      PointerType * ptype1 = ast_cast<PointerType *>(type1);
      PointerType * ptype2 = ast_cast<PointerType *>(type2);

      printf("%s, %s\n", ptype1->type->nodeName().c_str(), ptype2->type->nodeName().c_str());

      assert_msg(isSameType(ptype1->type, ptype2->type),
                 "error: cannot cast different pointers type, %s and %s.",
                 type1->nodeName().c_str(), type2->nodeName().c_str());
      return 0;
    }

    static CastExp *
    castIntegerArray(Type * type1, Type * type2)
    {
      IntegerType * itype;
      ArrayType * atype;

      if (type1->nodeType == IntegerType::nodeTypeId())
      {
        itype = ast_cast<IntegerType *>(type1);
        atype = ast_cast<ArrayType *>(type2);
      }
      else
      {
        itype = ast_cast<IntegerType *>(type2);
        atype = ast_cast<ArrayType *>(type1);
      }

      PointerType * ptype = new PointerType;
      ptype->type = atype->type;
      CastExp * cast = new CastExp;
      cast->type = ptype;

      return cast;
    }

    static CastExp *
    castPointerArray(Type * type1, Type * type2)
    {
      UnaryType * utype1;
      UnaryType * utype2;

      utype1 = reinterpret_cast<UnaryType *>(type1);
      utype2 = reinterpret_cast<UnaryType *>(type2);

      assert(isSameType(utype1->type, utype2->type));

      CastExp * cast = new CastExp;
      if (type1->nodeType == PointerType::nodeTypeId())
        cast->type = type1;
      else
        cast->type = type2;
      return cast;
    }

    static void
    init()
    {
      ADD_SYMETRIC_ENTRY(Void, Void);
      ADD_SYMETRIC_ENTRY(Bool, Bool);
      ADD_SYMETRIC_ENTRY(Bool, Integer);
      ADD_SYMETRIC_ENTRY(Bool, Float);
      ADD_SYMETRIC_ENTRY(Bool, Double);
      ADD_SYMETRIC_ENTRY(Integer, Integer);
      ADD_SYMETRIC_ENTRY(Integer, Float);
      ADD_SYMETRIC_ENTRY(Integer, Double);
      ADD_SYMETRIC_ENTRY(Integer, Pointer);
      ADD_SYMETRIC_ENTRY(Integer, Array);
      ADD_SYMETRIC_ENTRY(Float, Float);
      ADD_SYMETRIC_ENTRY(Float, Double);
      ADD_SYMETRIC_ENTRY(Double, Double);
      ADD_SYMETRIC_ENTRY(Pointer, Pointer);
      ADD_SYMETRIC_ENTRY(Pointer, Array);
    }

    CastExp *
    castToBestType(Type * type1, Type * type2)
    {
      static bool initTable = true;

      if (initTable) {
        init();
        initTable = false;
      }

      type1 = unreferencedType(type1);
      type2 = unreferencedType(type2);

      assert(type1);
      assert(type2);

      return table.dispatch(type1->nodeType, type2->nodeType)(type1, type2);
    }

    bool
    isSameType(Type * type1, Type * type2)
    {
      return !castToBestType(type1, type2);
    }

    static inline
    CastExp *
    makeCast(Type * type, Exp * exp)
    {
      CastExp * castExp = new CastExp;
      castExp->type = type;
      castExp->exp = exp;
      return castExp;
    }

    Exp *
    castToType(Type * type, Exp * exp)
    {
      if (type->nodeType != exp->type->nodeType)
        return makeCast(type, exp);

      if (type->nodeType == IntegerType::nodeTypeId())
      {
        IntegerType * typeTo = ast_cast<IntegerType *> (type);
        IntegerType * typeFrom = ast_cast<IntegerType *> (exp->type);

        if (typeTo->size != typeFrom->size ||
            typeTo->isSigned != typeFrom->isSigned)
          return makeCast(type, exp);
      }

      return exp;
    }

#define UNREFERENCED_TYPE(Const)                                \
    Const Type *                                                \
    unreferencedType(Const Type * type)                         \
    {                                                           \
      assert(type);                                             \
      if (type->nodeType == ReferenceType::nodeTypeId())        \
        return ast_cast<Const ReferenceType *> (type)->type;    \
      return type;                                              \
    }

    UNREFERENCED_TYPE()
    UNREFERENCED_TYPE(const)
  }
}

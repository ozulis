#include <ozulis/core/assert.hh>

namespace ozulis
{
  namespace ast
  {
#define AST_CAST(Const)                         \
    template <typename T>                       \
    T ast_cast(Const Node * node)               \
    {                                           \
      assert(node);                             \
      assert(dynamic_cast<T> (node));           \
      return reinterpret_cast<T>(node);         \
    }

    AST_CAST()
    AST_CAST(const)
  }
}

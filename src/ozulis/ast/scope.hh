#ifndef SCOPE_HH
# define SCOPE_HH

# include <map>

# include "ast.hh"

namespace ozulis
{
  namespace ast
  {
    class Scope
    {
    public:
      Scope();
      ~Scope();

      void addSymbol(SymbolPtr symbol);
      SymbolPtr findSymbol(const std::string & name) const;

      void addType(const std::string & name, TypePtr type);
      TypePtr findType(const std::string & name) const;

      std::string nextId();
      std::string nextStringId();

      std::string prefix;
      Scope *     parent;

    protected:
      typedef std::map<std::string /* name */, SymbolPtr> symbols_t;
      typedef std::map<std::string /* name */, TypePtr> types_t;
      symbols_t   symbols_;
      types_t     types_;
      int32_t     nextId_;
    };
  }
}

extern template class std::map<std::string, ozulis::ast::SymbolPtr>;
extern template class std::map<std::string, ozulis::ast::TypePtr>;

#endif /* !SCOPE_HH */

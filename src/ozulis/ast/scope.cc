#include <sstream>
#include <boost/format.hpp>

#include <ozulis/compiler.hh>
#include <ozulis/core/assert.hh>
#include "scope.hh"

template class std::map<std::string, ozulis::ast::SymbolPtr>;
template class std::map<std::string, ozulis::ast::TypePtr>;

namespace ozulis
{
  namespace ast
  {
    Scope::Scope()
      : prefix(),
        parent(0),
        symbols_(),
        types_(),
        nextId_(0)
    {
    }

    Scope::~Scope()
    {
    }

    SymbolPtr
    Scope::findSymbol(const std::string & name) const
    {
      symbols_t::const_iterator it = symbols_.find(name);
      if (it == symbols_.end())
        return parent ? parent->findSymbol(name) : SymbolPtr(0);
      return it->second;
    }

    void
    Scope::addSymbol(SymbolPtr symbol)
    {
      assert(symbol);
      assert(!symbol->name.empty());
      assert(symbol->type);
      assert(symbol->address);

      /* Check that the symbol is undefined */
      if (symbols_.find(symbol->name) != symbols_.end())
        Compiler::instance().error((boost::format(
            _("multiple definition of the variable %1%")) %
                                    symbol->name).str());

      symbols_[symbol->name] = symbol;

      /* make the address */
      if (symbol->address->nodeType == MemoryAddress::nodeTypeId())
        reinterpret_cast<MemoryAddress *>(symbol->address.ptr())->address =
          prefix + symbol->name;
      if (symbol->address->nodeType == RegisterAddress::nodeTypeId())
        reinterpret_cast<RegisterAddress *>(symbol->address.ptr())->address =
          prefix + symbol->name;
    }

    TypePtr
    Scope::findType(const std::string & name) const
    {
      types_t::const_iterator it = types_.find(name);
      if (it == types_.end())
        return parent ? parent->findType(name) : TypePtr();
      return it->second;
    }

    void
    Scope::addType(const std::string & name, TypePtr type)
    {
      assert(type);
      assert(!name.empty());

      /* Check that the type is undefined */
      if (types_.find(name) != types_.end())
        Compiler::instance().error((boost::format(
            _("multiple definition of the type %1%")) % name).str());

      types_[name] = type;
    }

    std::string
    Scope::nextId()
    {
      std::stringstream ss;
      ss << "$scope$" << nextId_++;
      return ss.str();
    }

    std::string
    Scope::nextStringId()
    {
      return nextId();
    }
  }
}

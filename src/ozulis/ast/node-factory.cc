#include <ozulis/core/assert.hh>

#include <ozulis/ast/ast.hh>
#include <ozulis/visitors/sizeof.hh>
#include "node-factory.hh"

namespace ozulis
{
  namespace ast
  {
#define CREATE_SIMPLE_TYPE(Type)                \
    Type *                                      \
    NodeFactory::create##Type()                 \
    {                                           \
      static Type * type = new Type;            \
                                                \
      return type;                              \
    }

    CREATE_SIMPLE_TYPE(VoidType)
    CREATE_SIMPLE_TYPE(BoolType)
    CREATE_SIMPLE_TYPE(FloatType)
    CREATE_SIMPLE_TYPE(DoubleType)

    CREATE_SIMPLE_TYPE(RegisterAddress)
    CREATE_SIMPLE_TYPE(MemoryAddress)

    IntegerType *
    NodeFactory::createIntegerType(bool isSigned, uint8_t size)
    {
      static std::map<int /*size*/, IntegerTypePtr> types[2];

      isSigned = !!isSigned;
      IntegerTypePtr & type = types[isSigned][size];
      if (!type)
      {
        type = new IntegerType;
        type->isSigned = isSigned;
        type->size = size;
      }
      return type;
    }

    PointerType *
    NodeFactory::createConstStringType()
    {
      static PointerTypePtr type;

      if (!type)
      {
        type = new PointerType;
        type->type = createIntegerType(false, 8);
      }
      return type;
    }

    IntegerType *
    NodeFactory::createUIntForPtr()
    {
      static IntegerTypePtr type;

      if (!type)
      {
        type = new IntegerType;
        type->isSigned = false;
        type->size = visitors::Sizeof::pointerSize();
      }
      return type;
    }

    Exp *
    NodeFactory::createSizeofValue(Type * type)
    {
      visitors::Sizeof sv;
      visitors::Sizeof::visit(*type, sv);

      NumberExp * exp = new NumberExp;
      exp->type = createUIntForPtr();
      exp->number = sv.size;
      return exp;
    }

    CastExp *
    NodeFactory::createCastPtrToUInt(Exp * exp)
    {
      CastExp * cast = new CastExp;
      cast->type = createUIntForPtr();
      cast->exp = exp;

      return cast;
    }

    CastExp *
    NodeFactory::createCastUIntToPtr(Exp * exp, Type * type)
    {
      CastExp * cast = new CastExp;
      cast->type = type;
      cast->exp = exp;

      return cast;
    }
  }
}

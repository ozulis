#ifndef AST_AST_CAST_HH
# define AST_AST_CAST_HH

namespace ozulis
{
  namespace ast
  {
    class Node;

    template <typename T>
    T ast_cast(Node *);

    template <typename T>
    T ast_cast(const Node *);
  }
}

# include "ast-cast.hxx"

#endif /* !AST_AST_CAST_HH */

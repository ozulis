#ifndef CORE_NODE_FACTORY_HH
# define CORE_NODE_FACTORY_HH

# include <ozulis/ast/ast.hh>

namespace ozulis
{
  namespace ast
  {
    class NodeFactory
    {
    public:
      static VoidType * createVoidType();
      static BoolType * createBoolType();
      static FloatType * createFloatType();
      static DoubleType * createDoubleType();
      static IntegerType * createIntegerType(bool isSigned, uint8_t size);
      static PointerType * createConstStringType();

      static RegisterAddress * createRegisterAddress();
      static MemoryAddress * createMemoryAddress();

      static IntegerType * createUIntForPtr();
      static Exp * createSizeofValue(Type * type);
      static CastExp * createCastPtrToUInt(Exp * exp);
      static CastExp * createCastUIntToPtr(Exp * exp, Type * type);
    };
  }
}

#endif /* !CORE_NODE_FACTORY_HH */

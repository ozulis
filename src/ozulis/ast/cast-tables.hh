#ifndef AST_CAST_TABLES_HH
# define AST_CAST_TABLES_HH

# include <ozulis/ast/ast.hh>

namespace ozulis
{
  namespace ast
  {
    CastExp * castToBestType(Type * type1, Type * type2);
    CastExp * castToIntegerType(Type * type1, Type * type2);
    Exp * castToType(Type * type, Exp * exp);
    bool isSameType(Type * type1, Type * type2);
    Type * unreferencedType(Type * type);
    const Type * unreferencedType(const Type * type);
  }
}

#endif /* !CAST_TABLES_HH */

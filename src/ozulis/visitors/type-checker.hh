#ifndef VISITORS_TYPE_CHECKER_VISITOR_HH
# define VISITORS_TYPE_CHECKER_VISITOR_HH

# include <utility>
# include <map>

# include <ozulis/visitors/visitor.hh>

namespace ozulis
{
  namespace visitors
  {
    /// @defgroup Visitors
    /**
     * @brief Check ast's type is semanticaly correct
     * @ingroup Visitors
     */
    class TypeChecker : public Visitor<TypeChecker>
    {
    public:
      TypeChecker();
      virtual ~TypeChecker();

      /** @brief type check the node, and may replace it */
      template <typename T>
      T check(T node);

      /** @brief initialise the visitor's table */
      static void initBase();

      /** @brief add a cast to a member if it is needed */
      static void homogenizeTypes(ast::BinaryExp & node);
      void pointerArith(ast::Exp * pointer, ast::Exp * offset);

      ast::FunctionPtr currentFunction;
      ast::Scope *     scope;
      ast::NodePtr     replacement;
    };
  }
}

#endif /* !VISITORS_TYPE_CHECKER_VISITOR_HH */

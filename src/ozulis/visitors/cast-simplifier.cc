#include <iostream>

#include <ozulis/ast/cast-tables.hh>
#include "cast-simplifier.hh"

namespace ozulis
{
  namespace visitors
  {
    CastSimplifier::CastSimplifier()
      : core::DispatchTable<ast::Exp * (*)(ast::CastExp & cast),
                            core::id_t, core::id_t> (),
        core::Singleton<CastSimplifier> ()
    {
    }

#define DUMMY_CAST(Type)                                                \
    static ast::Exp * simplify##Type##To##Type(ast::CastExp & cast)     \
    {                                                                   \
      return cast.exp;                                                  \
    }

    DUMMY_CAST(Bool)
    DUMMY_CAST(Float)
    DUMMY_CAST(Double)

#define SIMPLE_CAST(Type1, Type2)                                       \
    static ast::Exp * simplify##Type1##To##Type2(ast::CastExp & cast)   \
    {                                                                   \
      cast.nodeType = ast::Cast##Type1##To##Type2##Exp::nodeTypeId();   \
      return &cast;                                                     \
    }

    SIMPLE_CAST(Bool, Integer)
    SIMPLE_CAST(Bool, Float)
    SIMPLE_CAST(Bool, Double)
    SIMPLE_CAST(Integer, Bool)
    SIMPLE_CAST(Integer, Float)
    SIMPLE_CAST(Integer, Double)
    SIMPLE_CAST(Float, Integer)
    SIMPLE_CAST(Float, Double)
    SIMPLE_CAST(Double, Integer)
    SIMPLE_CAST(Double, Float)
    SIMPLE_CAST(Integer, Pointer)
    SIMPLE_CAST(Pointer, Integer)

    static ast::Exp * simplifyIntegerToInteger(ast::CastExp & cast)
    {
      ast::IntegerType * dtype = AST_PTR_CAST(IntegerType, cast.type);
      ast::IntegerType * ftype = AST_PTR_CAST(IntegerType, cast.exp->type);

      if (dtype->size == ftype->size)
        return cast.exp;
      cast.nodeType = ast::CastIntegerToIntegerExp::nodeTypeId();
      return &cast;
    }

    /// @todo handle references
    static ast::Exp * simplifyReferenceToInteger(ast::CastExp & cast)
    {
      /// @todo more checks
      return cast.exp;
    }

    static ast::Exp * simplifyPointerToPointer(ast::CastExp & cast)
    {
      ast::PointerType * dtype = AST_PTR_CAST(PointerType, cast.type);
      ast::PointerType * ftype = AST_PTR_CAST(PointerType, cast.exp->type);

      if (ast::isSameType(dtype->type, ftype->type))
        return cast.exp;
      cast.nodeType = ast::CastPointerToPointerExp::nodeTypeId();
      return &cast;
    }

    ast::Exp *
    CastSimplifier::simplify(ast::CastExp & cast)
    {
      // don't simplify an already simplified CastExp.
      if (cast.nodeType != ast::CastExp::nodeTypeId())
        return &cast;

#if 0
      std::cout << "cast: " << cast.exp->type->nodeName()
                << " to " << cast.type->nodeName() << std::endl;
#endif
      return instance().dispatch
        (cast.exp->type->nodeType, cast.type->nodeType)(cast);
    }

    void
    CastSimplifier::init()
    {
#define ADD_ENTRY(Type1, Type2)                                         \
      addEntry(simplify##Type1##To##Type2,                              \
               ast::Type1##Type::nodeTypeId(),                          \
               ast::Type2##Type::nodeTypeId())

      ADD_ENTRY(Bool, Bool);
      ADD_ENTRY(Float, Float);
      ADD_ENTRY(Double, Double);

      ADD_ENTRY(Bool, Integer);
      ADD_ENTRY(Bool, Float);
      ADD_ENTRY(Bool, Double);
      ADD_ENTRY(Integer, Bool);
      ADD_ENTRY(Integer, Integer);
      ADD_ENTRY(Integer, Float);
      ADD_ENTRY(Integer, Double);
      ADD_ENTRY(Float, Integer);
      ADD_ENTRY(Float, Double);
      ADD_ENTRY(Double, Integer);
      ADD_ENTRY(Double, Float);
      ADD_ENTRY(Integer, Pointer);
      ADD_ENTRY(Pointer, Integer);
      ADD_ENTRY(Pointer, Pointer);

      ADD_ENTRY(Reference, Integer);
    }
  }
}

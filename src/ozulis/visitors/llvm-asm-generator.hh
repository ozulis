#ifndef VISITORS_LLVM_ASM_GENERATOR_HH
# define VISITORS_LLVM_ASM_GENERATOR_HH

# include <ostream>

# include <ozulis/ast/ast.hh>
# include <ozulis/visitors/visitor.hh>

namespace ozulis
{
  namespace visitors
  {
    /// @defgroup Visitors
    /**
     * @brief Generates LLVM assembly
     * @ingroup Visitors
     */
    class LLVMAsmGenerator : public ConstVisitor<LLVMAsmGenerator>
    {
    public:
      LLVMAsmGenerator(std::ostream & out);
      virtual ~LLVMAsmGenerator();

      static void initBase();

      void visitUDivExp(const ast::DivExp & node);
      void visitSDivExp(const ast::DivExp & node);
      void visitFDivExp(const ast::DivExp & node);
      void visitUModExp(const ast::ModExp & node);
      void visitSModExp(const ast::ModExp & node);
      void visitFModExp(const ast::ModExp & node);

      void writeFunctionHeader(const ast::FunctionDecl & node);

      std::ostream & out;
    };
  }
}

#endif /* !VISITORS_LLVM_ASM_GENERATOR_HH */

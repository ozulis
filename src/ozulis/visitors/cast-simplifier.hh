#ifndef CORE_CAST_SIMPLIFIER_HH
# define CORE_CAST_SIMPLIFIER_HH

# include <ozulis/core/singleton.hh>
# include <ozulis/core/multi-method.hh>
# include <ozulis/ast/ast.hh>

namespace ozulis
{
  namespace visitors
  {
    class CastSimplifier :
      public core::DispatchTable<ast::Exp * (*)(ast::CastExp & cast),
                                 core::id_t, core::id_t>,
      public core::Singleton<CastSimplifier>
    {
    public:
      CastSimplifier();

      /** @brief used to initialize the dispatch table.
       * You should not call it. Singleton will do it. */
      void init();
      /** @brief will simplify a cast */
      static ast::Exp * simplify(ast::CastExp & cast);
    };
  }
}

#endif /* !CORE_CAST_SIMPLIFIER_HH */

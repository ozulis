#include <sstream>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/ast/ast.hh>
#include <ozulis/ast/node-factory.hh>
#include <ozulis/visitors/browser.hh>
#include "scope-builder.hh"

namespace ozulis
{
  namespace visitors
  {
    std::string
    ScopeBuilder::nextId()
    {
      std::stringstream ss;
      ss << "global_$" << ++nextId_;
      return ss.str();
    }

    static void visitFile(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::File &> (node_);
      node.scope = new ast::Scope();
      node.scope->parent = 0;
      node.scope->prefix = "@";
      ctx.file = &node;
      ctx.parent = node.scope;
      browseFile<ScopeBuilder>(node, ctx);
      ctx.parent = 0;
    }

    static void visitFunctionDecl(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::FunctionDecl &> (node_);
      assert(ctx.parent);
      assert(node.args);

      ast::FunctionTypePtr ftype = new ast::FunctionType;
      ftype->type = node.returnType;
      ftype->argsType = new std::vector<ast::TypePtr> ();
      BOOST_FOREACH (ast::VarDecl * varDecl, *node.args)
        ftype->argsType->push_back(varDecl->type);

      ast::SymbolPtr symbol = new ast::Symbol;
      symbol->name = node.name;
      symbol->type = ftype;
      symbol->address = new ast::MemoryAddress;
      ctx.parent->addSymbol(symbol);
    }

    void visitFunction(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::Function &> (node_);

      assert(ctx.parent);
      assert(node.block);
      assert(node.args);

      ast::FunctionTypePtr ftype = new ast::FunctionType;
      ftype->type = node.returnType;
      ftype->argsType = new std::vector<ast::TypePtr> ();
      BOOST_FOREACH (ast::VarDecl * varDecl, *node.args)
        ftype->argsType->push_back(varDecl->type);

      ast::SymbolPtr symbol = new ast::Symbol;
      symbol->name = node.name;
      symbol->type = ftype;
      symbol->address = new ast::MemoryAddress;
      /// @todo address suffix
      ctx.parent->addSymbol(symbol);

      node.scope = new ast::Scope;
      node.scope->parent = ctx.parent;
      node.scope->prefix = "%";

      BOOST_FOREACH (ast::VarDeclPtr & varDecl, *node.args)
      {
        assert(varDecl);

        symbol = new ast::Symbol;
        symbol->name = "." + varDecl->name;
        symbol->address = new ast::RegisterAddress;
        symbol->type = varDecl->type;
        node.scope->addSymbol(symbol);

        ast::SymbolExpPtr value = new ast::SymbolExp;
        value->symbol = symbol;

        ast::ValuedVarDeclPtr valuedVarDecl = new ast::ValuedVarDecl;
        valuedVarDecl->name = varDecl->name;
        valuedVarDecl->type = varDecl->type;
        valuedVarDecl->value = value;

        node.block->varDecls->insert(node.block->varDecls->begin(),
                                     valuedVarDecl);
      }

      ctx.parent = node.scope;
      ctx.blockIds.clear();
      ctx.blockIds.push_back(0);
      ScopeBuilder::visit(*node.block, ctx);
      ctx.parent = node.scope->parent;
    }

    void visitBlock(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::Block &> (node_);
      assert(ctx.parent);
      node.scope = new ast::Scope();
      node.scope->parent = ctx.parent;

      std::stringstream stream;
      stream << ctx.parent->prefix << ".B" << ctx.blockIds.back()++ << ".";
      node.scope->prefix = stream.str();
      ctx.parent = node.scope;

      assert(node.varDecls);
      BOOST_FOREACH (ast::VarDeclPtr & varDecl, (*node.varDecls))
      {
        ast::SymbolPtr symbol = new ast::Symbol;

        assert(varDecl);
        symbol->name = varDecl->name;
        ast::ReferenceTypePtr rtype = new ast::ReferenceType;
        rtype->type = varDecl->type;
        symbol->type = rtype;
        ast::MemoryAddressPtr addr = new ast::MemoryAddress;
        symbol->address = addr;
        node.scope->addSymbol(symbol);
      }

      ctx.blockIds.push_back(0);
      browseBlock<ScopeBuilder>(node, ctx);
      ctx.blockIds.pop_back();
      ctx.parent = node.scope->parent;
    }

    /// @todo check this one again later
    static void visitStringExp(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::StringExp &> (node_);
      assert(ctx.file);

      ast::ArrayTypePtr array = new ast::ArrayType;
      array->isConst = true;
      array->size = node.string.length() + 1;
      array->type = ast::NodeFactory::createIntegerType(false, 8);

      ast::ReferenceTypePtr rtype = new ast::ReferenceType;
      rtype->type = array;

      ast::ValuedVarDeclPtr varDecl = new ast::ValuedVarDecl;
      varDecl->name = ctx.file->scope->nextStringId();
      varDecl->type = rtype;
      varDecl->value = &node;
      node.type = varDecl->type;

      ast::SymbolPtr symbol = new ast::Symbol;
      symbol->name = varDecl->name;
      symbol->type = varDecl->type;
      symbol->address = ast::NodeFactory::createMemoryAddress();

      ast::SymbolExpPtr sexp = new ast::SymbolExp;
      sexp->symbol = symbol;
      sexp->type = symbol->type;

      ast::AtExpPtr at = new ast::AtExp;
      at->exp = sexp;

      ast::CastExpPtr cast = new ast::CastExp;
      cast->exp = at;
      cast->type = ast::NodeFactory::createConstStringType();

      assert(&node == *ctx.parentRef);
      *ctx.parentRef = cast;

      ctx.file->scope->addSymbol(symbol);
      ctx.file->varDecls->push_back(varDecl);
    }

    static void visitAlias(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::Alias &> (node_);
      assert(ctx.parent);

      ctx.parent->addType(node.name, node.type);
    }

    static void visitTypedef(ast::Node & node_, ScopeBuilder & ctx)
    {
      auto & node = reinterpret_cast<ast::Typedef &> (node_);
      assert(ctx.parent);

      ctx.parent->addType(node.name, node.type);
    }

    ScopeBuilder::ScopeBuilder()
      : Visitor<ScopeBuilder>(),
        file(0),
        parent(0),
        nextId_(0)
    {
    }

    void
    ScopeBuilder::initBase()
    {
#define REGISTER_METHOD(Class)                                  \
      registerMethod(ast::Class::nodeTypeId(), visit##Class)

      REGISTER_METHOD(File);
      REGISTER_METHOD(Function);
      REGISTER_METHOD(FunctionDecl);
      REGISTER_METHOD(Block);
      REGISTER_METHOD(StringExp);
      REGISTER_METHOD(Alias);
      REGISTER_METHOD(Typedef);

      completeWith<Browser<ScopeBuilder> >();
    }
  }
}

#ifndef VISITORS_SIZEOF_HH
# define VISITORS_SIZEOF_HH

# include <ozulis/ast/ast.hh>
# include <ozulis/visitors/visitor.hh>

namespace ozulis
{
  namespace visitors
  {
    class Sizeof : public ConstVisitor<Sizeof>
    {
    public:
      Sizeof();
      virtual ~Sizeof();

      static void initBase();
      static uint8_t pointerSize();

      uint32_t size;
    };
  }
}

#endif /* !VISITORS_SIZEOF_HH */

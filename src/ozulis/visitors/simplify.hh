#ifndef VISITORS_SIMPLIFY_HH
# define VISITORS_SIMPLIFY_HH

# include <vector>

# include <ozulis/ast/ast.hh>
# include <ozulis/visitors/visitor.hh>

namespace ozulis
{
  namespace visitors
  {
    /**
     * @brief transforms an AST to 3 address instructions.
     */
    class Simplify : public Visitor<Simplify>
    {
    public:
      Simplify();
      virtual ~Simplify();

      /** @brief initialize the table of the visitor */
      static void initBase();

      /** @brief when you simplify lvalue, you want the pointer and not the
       * value to be able to do a StoreVar latter */
      ast::ExpPtr simplifyLValue(ast::ExpPtr exp);
      /** @brief replaces node by it's simplified expression */
      ast::ExpPtr simplify(ast::ExpPtr node);
      /** @brief create a temporary in a register which is the result of node */
      void makeTmpResult(ast::Exp & node);

      std::string currentId() const;
      std::string nextId();

      /// @brief the new list of 3 address instructions
      std::vector<ast::NodePtr> simplifications;
      /// @brief the child which replace the previously visited node
      ast::ExpPtr               replacement;
      /// @brief the current scope, to retype check some generated expressions
      ast::Scope *              scope;

    private:
      /// @brief the next id for register count
      int                       nextId_;
    };
  }
}

#endif /* !VISITORS_SIMPLIFY_HH */

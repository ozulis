#ifndef VISITOR_VISITOR_HH
# define VISITOR_VISITOR_HH

# include <ozulis/core/id-visitor.hh>
# include <ozulis/ast/ast.hh>

namespace ozulis
{
  namespace visitors
  {
    class GetNodeId
    {
    public:
      static inline id_t id(const ast::Node & node)
      {
        return node.nodeType;
      }

      static inline id_t parent(const ast::Node & node)
      {
        return core::IdBase<ast::Node>::parent(node.nodeType);
      }

      static inline id_t limit()
      {
        return core::IdBase<ast::Node>::count();
      }
    };

    template <class T, class Node, typename ...Args>
    class VisitorBase : public core::IdVisitor<T, Node, GetNodeId, T &, Args...>
    {
    public:
      /** Used by the browse visitor to save the reference to the node pointer */
      ast::Node const ** parentRef;
    };

    template <class T, typename ...Args>
    class Visitor : public VisitorBase<T, ast::Node, Args...>
    {
    public:
      typedef Visitor<T, Args...> Visitor_t;
    };

    template <class T, typename ...Args>
    class ConstVisitor : public VisitorBase<T, const ast::Node, Args...>
    {
    public:
      typedef ConstVisitor<T, Args...> ConstVisitor_t;
    };
  }
}

#endif /* !VISITORS_VISITOR_HH */

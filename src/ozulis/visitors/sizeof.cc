#include <ozulis/compiler.hh>
#include <ozulis/target-data.hh>
#include <ozulis/visitors/browser.hh>
#include <ozulis/visitors/sizeof.hh>

namespace ozulis
{
  namespace visitors
  {
    Sizeof::Sizeof()
      : ConstVisitor_t(),
        size(0)
    {
    }

    Sizeof::~Sizeof()
    {
    }

    uint8_t
    Sizeof::pointerSize()
    {
      return Compiler::instance().targetData().findPointer()->size;
    }

#define CONST_SIZE_TYPE(Type_, Size)                                    \
    static void visit##Type_##Type(const ast::Node & /*node_*/, Sizeof & ctx)  \
    {                                                                   \
      ctx.size += Size;                                                 \
    }

    CONST_SIZE_TYPE(Pointer, Sizeof::pointerSize())
    CONST_SIZE_TYPE(Reference, Sizeof::pointerSize())
    CONST_SIZE_TYPE(Void, 1)
    CONST_SIZE_TYPE(Bool, 1)
    CONST_SIZE_TYPE(Float, sizeof (float))
    CONST_SIZE_TYPE(Double, sizeof (double))

    static void visitIntegerType(const ast::Node & node_, Sizeof & ctx)
    {
      const ast::IntegerType & node =
        reinterpret_cast<const ast::IntegerType &> (node_);
      ctx.size += (node.size >> 3) + !!(node.size & 0x7);
    }

    void
    Sizeof::initBase()
    {
#define REGISTER_METHOD(Class)                                  \
      registerMethod(ast::Class::nodeTypeId(), visit##Class)

      REGISTER_METHOD(PointerType);
      REGISTER_METHOD(ReferenceType);
      REGISTER_METHOD(VoidType);
      REGISTER_METHOD(BoolType);
      REGISTER_METHOD(IntegerType);
      REGISTER_METHOD(FloatType);
      REGISTER_METHOD(DoubleType);

      completeWith<ConstBrowser<Sizeof> >();
    }
  }
}

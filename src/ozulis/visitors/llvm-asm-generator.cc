#include <iomanip>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/ast/ast-cast.hh>
#include <ozulis/ast/cast-tables.hh>
#include <ozulis/visitors/browser.hh>

#include "llvm-asm-generator.hh"

#if 0 // 1 to get a backtrace
# define static
#endif

namespace ozulis
{
  namespace visitors
  {
    LLVMAsmGenerator::LLVMAsmGenerator(std::ostream & _out)
      : ConstVisitor<LLVMAsmGenerator>(),
        out(_out)
    {
      assert_msg(out.good(), "The output for llvm assembly is not valid.");
      out << std::setprecision(1000);
    }

    LLVMAsmGenerator::~LLVMAsmGenerator()
    {
    }

    static void visitFile(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      const auto & node = reinterpret_cast<const ast::File &> (node_);

      BOOST_FOREACH(ast::VarDecl * var, *node.varDecls)
      {
        ctx.out << "@" << var->name << " = internal constant ";
        LLVMAsmGenerator::visit(*unreferencedType(var->type), ctx);
        if (var->nodeType == ast::ValuedVarDecl::nodeTypeId())
        {
          ctx.out << " ";
          LLVMAsmGenerator::visit(
            *reinterpret_cast<ast::ValuedVarDecl *> (var)->value, ctx);
        }
        ctx.out << ";" << std::endl;
        //assert_msg(false, "finish array first");
      }

      BOOST_FOREACH(ast::Node * decl, *node.decls)
        LLVMAsmGenerator::visit(*decl, ctx);
    }

    void
    LLVMAsmGenerator::writeFunctionHeader(const ast::FunctionDecl & node)
    {
      LLVMAsmGenerator::visit(*node.returnType, *this);
      out << " @" << node.name << "(";

      for (std::vector<ast::VarDeclPtr>::iterator it = node.args->begin();
           it != node.args->end(); it++)
      {
        if (it != node.args->begin())
          out << ", ";
        LLVMAsmGenerator::visit(*(*it)->type, *this);
        out << " %." << (*it)->name;
      }

      out << ")" << std::endl;
    }

    static void visitFunctionDecl(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::FunctionDecl &> (node_);
      ctx.out << "declare ";
      ctx.writeFunctionHeader(node);
    }

    static void visitFunction(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::Function &> (node_);
      ctx.out << "define ";
      ctx.writeFunctionHeader(node);
      ctx.out << "{" << std::endl;

      LLVMAsmGenerator::visit(*node.block, ctx);
      ctx.out << "}" << std::endl;
    }

    static void visitVarDecl(const ast::Node & /*node_*/,
                             LLVMAsmGenerator & /*ctx*/)
    {
      assert_msg(false, _("Should not come here, use Alloca."));
    }

    static void visitAlloca(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::Alloca &> (node_);

      ctx.out << "  ";
      LLVMAsmGenerator::visit(*node.symbol->address, ctx);
      ctx.out << " = alloca ";
      LLVMAsmGenerator::visit(*unreferencedType(node.symbol->type), ctx);
      ctx.out << std::endl;
    }

    static void visitLoadVar(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::LoadVar &> (node_);
      assert(node.from->type);

      ctx.out << "  " << node.to << " = load ";
      LLVMAsmGenerator::visit(*node.from->type, ctx);
      ctx.out << " ";
      LLVMAsmGenerator::visit(*node.from, ctx);
      ctx.out << std::endl;
    }

    static void visitStoreVar(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::StoreVar &> (node_);
      assert(node.value);
      assert(node.value->type);

      ctx.out << "  store ";
      LLVMAsmGenerator::visit(*node.value->type, ctx);
      LLVMAsmGenerator::visit(*node.value, ctx);
      ctx.out << ", ";
      LLVMAsmGenerator::visit(*node.to, ctx);
      ctx.out << std::endl;
    }

    static void visitAssignExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::AssignExp &> (node_);
      ctx.out << "  ";
      LLVMAsmGenerator::visit(*node.dest, ctx);
      ctx.out << " = ";
      if (node.value->nodeType == ast::NumberExp::nodeTypeId() ||
          node.value->nodeType == ast::SymbolExp::nodeTypeId())
        assert_msg(false, _("should not get here"));
      else
        LLVMAsmGenerator::visit(*node.value, ctx);
      ctx.out << std::endl;
    }

    static void visitPointerArithExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node =
        reinterpret_cast<const ast::PointerArithExp &> (node_);

      ctx.out << "getelementptr ";
      LLVMAsmGenerator::visit(*node.pointer->type, ctx);
      LLVMAsmGenerator::visit(*node.pointer, ctx);
      ctx.out << ", ";
      LLVMAsmGenerator::visit(*node.offset->type, ctx);
      LLVMAsmGenerator::visit(*node.offset, ctx);
    }

#define GEN_BINARY_EXP_EXT(Name, Type, Instr)                           \
    static void Name(const ast::Node & node_, LLVMAsmGenerator & ctx)   \
    {                                                                   \
      auto & node = reinterpret_cast<const ast::Type &> (node_);        \
      ctx.out << Instr" ";                                              \
      LLVMAsmGenerator::visit(*unreferencedType(node.type), ctx);       \
      LLVMAsmGenerator::visit(*node.left, ctx);                         \
      ctx.out << ", ";                                                  \
      LLVMAsmGenerator::visit(*node.right, ctx);                        \
    }

#define GEN_BINARY_EXP(Type, Instr) GEN_BINARY_EXP_EXT(visit##Type, Type, Instr)

    GEN_BINARY_EXP(AddExp, "add")
    GEN_BINARY_EXP(SubExp, "sub")
    GEN_BINARY_EXP(MulExp, "mul")
    GEN_BINARY_EXP_EXT(visitUDivExp, DivExp, "udiv")
    GEN_BINARY_EXP_EXT(visitSDivExp, DivExp, "sdiv")
    GEN_BINARY_EXP_EXT(visitFDivExp, DivExp, "fdiv")
    GEN_BINARY_EXP_EXT(visitUModExp, ModExp, "urem")
    GEN_BINARY_EXP_EXT(visitSModExp, ModExp, "srem")
    GEN_BINARY_EXP_EXT(visitFModExp, ModExp, "frem")

    GEN_BINARY_EXP(AndExp, "and")
    GEN_BINARY_EXP(OrExp, "or")
    GEN_BINARY_EXP(XorExp, "xor")

    GEN_BINARY_EXP(ShlExp, "shl")
    GEN_BINARY_EXP(AShrExp, "ashr")
    GEN_BINARY_EXP(LShrExp, "lshr")

    /// @todo check this is correct.
#define GEN_CMP_EXP(Type_, Instr)                                       \
    static void visit##Type_(const ast::Node & node_, LLVMAsmGenerator & ctx) \
    {                                                                   \
      const ast::Type_ & node = reinterpret_cast<const ast::Type_ &> (node_); \
      const ast::Type * type = unreferencedType(node.left->type);       \
                                                                        \
      if (type->nodeType == ast::FloatType::nodeTypeId() ||             \
          type->nodeType == ast::DoubleType::nodeTypeId())              \
        ctx.out << "fcmp o";                                            \
      else                                                              \
      {                                                                 \
        ctx.out << "icmp ";                                             \
        if (node.nodeType == ast::LtExp::nodeTypeId() ||                \
            node.nodeType == ast::LtEqExp::nodeTypeId() ||              \
            node.nodeType == ast::GtExp::nodeTypeId() ||                \
            node.nodeType == ast::GtEqExp::nodeTypeId())                \
        {                                                               \
          if (type->nodeType == ast::IntegerType::nodeTypeId())         \
          {                                                             \
            const ast::IntegerType * itype =                            \
              reinterpret_cast<const ast::IntegerType *>(type);         \
            ctx.out << (itype->isSigned ? "s" : "u");                   \
          }                                                             \
          else if (type->nodeType == ast::BoolType::nodeTypeId())       \
            ctx.out << "u";                                             \
        }                                                               \
      }                                                                 \
      ctx.out << Instr" ";                                              \
      LLVMAsmGenerator::visit(*type, ctx);                              \
      LLVMAsmGenerator::visit(*node.left, ctx);                         \
      ctx.out << ", ";                                                  \
      LLVMAsmGenerator::visit(*node.right, ctx);                        \
    }

    GEN_CMP_EXP(EqExp, "eq")
    GEN_CMP_EXP(NeqExp, "ne")
    GEN_CMP_EXP(LtExp, "lt")
    GEN_CMP_EXP(LtEqExp, "le")
    GEN_CMP_EXP(GtExp, "gt")
    GEN_CMP_EXP(GtEqExp, "ge")

#define GEN_BINARY_BOOL_EXP(Type, Op)                                   \
    static void visit##Type(const ast::Node & node_, LLVMAsmGenerator & ctx) \
    {                                                                   \
      auto & node = reinterpret_cast<const ast::Type &> (node_);        \
      ctx.out << Op " i1 ";                                             \
      LLVMAsmGenerator::visit(*node.left, ctx);                         \
      ctx.out << ", ";                                                  \
      LLVMAsmGenerator::visit(*node.right, ctx);                        \
    }

    GEN_BINARY_BOOL_EXP(AndAndExp, "and ")
    GEN_BINARY_BOOL_EXP(OrOrExp, "or ")

#define GEN_DIV_MOD_EXP(Name)                                           \
    static void visit##Name##Exp(const ast::Node & node_, LLVMAsmGenerator & ctx) \
    {                                                                   \
      const ast::Name##Exp & node = reinterpret_cast<const ast::Name##Exp &> (node_); \
      if (node.type->nodeType == ast::BoolType::nodeTypeId())           \
        visitU##Name##Exp(node, ctx);                                   \
      else if (node.type->nodeType == ast::FloatType::nodeTypeId() ||   \
               node.type->nodeType == ast::DoubleType::nodeTypeId())    \
        visitF##Name##Exp(node, ctx);                                   \
      else if (node.type->nodeType == ast::IntegerType::nodeTypeId())   \
      {                                                                 \
        const ast::IntegerType * type =                                 \
          reinterpret_cast<const ast::IntegerType *>(node.type.ptr());  \
        type->isSigned ?                                                \
          visitS##Name##Exp(node, ctx) : visitU##Name##Exp(node, ctx);  \
      }                                                                 \
      else                                                              \
        assert(false);                                                  \
    }

    GEN_DIV_MOD_EXP(Div)
    GEN_DIV_MOD_EXP(Mod)


    static void visitNegExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::NegExp &> (node_);
      ctx.out << "sub ";
      LLVMAsmGenerator::visit(*node.type, ctx);
      if (AST_IS_FLOATING_POINT_TYPE(node.type))
        ctx.out << " 0.0, ";
      else
        ctx.out << " 0, ";
      LLVMAsmGenerator::visit(*node.exp, ctx);
    }

    static void visitBangExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::BangExp &> (node_);
      ctx.out << "xor i1 1, ";
      LLVMAsmGenerator::visit(*node.exp, ctx);
    }

    static void visitNotExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::NotExp &> (node_);
      ctx.out << "xor ";
      LLVMAsmGenerator::visit(*node.type, ctx);
      ctx.out << " -1, ";
      LLVMAsmGenerator::visit(*node.exp, ctx);
    }

    static void visitExp(const ast::Node & /*node_*/, LLVMAsmGenerator & /*ctx*/)
    {
      assert_msg(false, "unimplemented method");
    }

    static void visitVoidExp(const ast::Node & /*node_*/, LLVMAsmGenerator & /*ctx*/)
    {
    }

    static void visitSymbolExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node =
        reinterpret_cast<const ast::SymbolExp &> (node_);
      LLVMAsmGenerator::visit(*node.symbol->address, ctx);
    }

    static void visitMemoryAddress(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node =
        reinterpret_cast<const ast::MemoryAddress &> (node_);
      assert_msg(!node.address.empty(), _("address should never be empty"));
      ctx.out << node.address;
    }

    static void visitRegisterAddress(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node =
        reinterpret_cast<const ast::RegisterAddress &> (node_);
      assert_msg(!node.address.empty(), _("address should never be empty"));
      ctx.out << node.address;
    }

    static void visitAtExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::AtExp &> (node_);
      LLVMAsmGenerator::visit(*node.exp, ctx);
    }

    static void visitCallExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::CallExp &> (node_);
      ctx.out << "  call ";
      LLVMAsmGenerator::visit(*node.type, ctx);
      LLVMAsmGenerator::visit(*node.function, ctx);
      ctx.out << "(";

      for (ast::CallExp::args_t::iterator it = node.args->begin();
           it != node.args->end(); it++)
      {
        if (it != node.args->begin())
          ctx.out << ", ";
        LLVMAsmGenerator::visit(*unreferencedType((*it)->type), ctx);
        LLVMAsmGenerator::visit(*(*it), ctx);
      }

      ctx.out << ")";
      if (node.type->nodeType == ast::VoidType::nodeTypeId())
        ctx.out << std::endl;
    }

    static void visitNumberExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::NumberExp &> (node_);
      if (node.type->nodeType == ast::FloatType::nodeTypeId())
        /// @todo find the right way to print floating point numbers
        ctx.out << std::setprecision(1000) << (float)node.number;
      else
        ctx.out << std::setprecision(1000) << node.number;
    }

    static void visitStringExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::StringExp &> (node_);
      ctx.out << "c\"" << node.string << "\\00\"";
    }

    static void visitLabel(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::Label &> (node_);
      ctx.out << "  br label %" << node.name << " ; dummy branch to start a block" << std::endl
              << node.name << ":" << std::endl;
    }

    static void visitGoto(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::Goto &> (node_);
      ctx.out << "  br label %" << node.label << std::endl;
    }

    static void visitReturn(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::Return &> (node_);
      assert(node.exp);

      ctx.out << "  ret ";
      LLVMAsmGenerator::visit(*node.exp->type, ctx);
      LLVMAsmGenerator::visit(*node.exp, ctx);
      ctx.out << std::endl;
    }

    static void visitConditionalBranch(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::ConditionalBranch &> (node_);
      ctx.out << "  br i1 ";
      LLVMAsmGenerator::visit(*node.cond, ctx);
      ctx.out << ", label %" << node.trueLabel->name << ", label %"
              << node.falseLabel->name << std::endl;
    }

    static void visitType(const ast::Node & /*node_*/, LLVMAsmGenerator & /*ctx*/)
    {
      //assert_msg(false, "we should never reach this code");
    }

    static void visitNumberType(const ast::Node & /*node_*/, LLVMAsmGenerator & /*ctx*/)
    {
      assert_msg(false, "we should never reach this code");
    }

    static void visitBoolType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::BoolType &> (node_);
      ctx.out << "i1 ";
    }

    static void visitVoidType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::VoidType &> (node_);
      ctx.out << "void ";
    }

    static void visitIntegerType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::IntegerType &> (node_);
      ctx.out << "i" << node.size << " ";
    }

    static void visitFloatType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::FloatType &> (node_);
      ctx.out << "float ";
    }

    static void visitDoubleType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::DoubleType &> (node_);
      ctx.out << "double ";
    }

    static void visitPointerType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::PointerType &> (node_);
      LLVMAsmGenerator::visit(*node.type, ctx);
      ctx.out << "*";
    }

    static void visitArrayType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::ArrayType &> (node_);
      ctx.out << " [" << node.size << " x ";
      LLVMAsmGenerator::visit(*node.type, ctx);
      ctx.out << "] ";
    }

    static void visitReferenceType(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::ReferenceType &> (node_);
      LLVMAsmGenerator::visit(*node.type, ctx);
      ctx.out << "*";
    }

    static void visitCastExp(const ast::Node & /*node_*/,
                             LLVMAsmGenerator & /*ctx*/)
    {
      assert_msg(false, "the cast should be resolved");
    }

#define VISIT_CAST(Type1, Type2, CastInstr)                             \
    static void                                                         \
    visitCast##Type1##To##Type2##Exp(const ast::Node & node_,           \
                                     LLVMAsmGenerator & ctx)            \
    {                                                                   \
      const ast::Cast##Type1##To##Type2##Exp & node =                   \
        reinterpret_cast<const ast::Cast##Type1##To##Type2##Exp &>      \
        (node_);                                                        \
      ctx.out << CastInstr" ";                                          \
      LLVMAsmGenerator::visit(*node.exp->type, ctx);                    \
      ctx.out << " ";                                                   \
      LLVMAsmGenerator::visit(*node.exp, ctx);                          \
      ctx.out << " to ";                                                \
      LLVMAsmGenerator::visit(*node.type, ctx);                         \
      return;                                                           \
    }

    VISIT_CAST(Bool, Float, "uitofp")
    VISIT_CAST(Bool, Double, "uitofp")
    VISIT_CAST(Double, Float, "fptrunc")
    VISIT_CAST(Float, Double, "fpext")
    VISIT_CAST(Integer, Pointer, "inttoptr")
    VISIT_CAST(Pointer, Integer, "ptrtoint")

    static void
    visitCastArrayToPointerExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::CastExp &> (node_);
      ctx.out << "getelementptr ";
      LLVMAsmGenerator::visit(*node.exp->type, ctx);
      ctx.out << "*";
      LLVMAsmGenerator::visit(*node.exp, ctx);
      ctx.out << ", i64 0, ";
      const ast::ArrayType * atype =
        ast::ast_cast<const ast::ArrayType *> (node.exp->type);
      LLVMAsmGenerator::visit(*atype->type, ctx);
      ctx.out << "0";
    }

    static void
    visitCastBoolToIntegerExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::CastExp &> (node_);
      const ast::IntegerType * to =
        reinterpret_cast<const ast::IntegerType *>(node.type.ptr());

      if (to->size == 1)
      {
        ctx.out << "add i1 0, ";
        LLVMAsmGenerator::visit(*node.exp, ctx);
      }
      else
      {
        ctx.out << "zext i1 ";
        LLVMAsmGenerator::visit(*node.exp, ctx);
        ctx.out << " to ";
        LLVMAsmGenerator::visit(*to, ctx);
      }
    }

    static void
    visitCastIntegerToIntegerExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::CastExp &> (node_);
      const ast::IntegerType * from = ast::ast_cast<const ast::IntegerType *>(node.exp->type);
      const ast::IntegerType * to   = ast::ast_cast<const ast::IntegerType *>(node.type);

      if (from->size == to->size)
      {
        ctx.out << "add i" << to->size << " 0, ";
        LLVMAsmGenerator::visit(*node.exp, ctx);
        return;
      }

      if (from->size > to->size)
        ctx.out << "trunc";
      else if (from->isSigned)
        ctx.out << "sext";
      else
        ctx.out << "zext";
      ctx.out << " i" << from->size << " ";
      LLVMAsmGenerator::visit(*node.exp, ctx);
      ctx.out << " to i" << to->size;
    }

#define INTEGER_TO_FLOAT(Float, Str)                                    \
    static void                                                         \
    visitCastIntegerTo##Float##Exp(const ast::Node & node_,             \
                                   LLVMAsmGenerator & ctx)              \
    {                                                                   \
      auto & node =                                                     \
        reinterpret_cast<const ast::CastExp &> (node_);                 \
      const ast::IntegerType * from =                                   \
        ast::ast_cast<const ast::IntegerType *>(node.exp->type);        \
                                                                        \
      ctx.out << (from->isSigned ? "sitofp " : "uitofp ");              \
      LLVMAsmGenerator::visit(*from, ctx);                              \
      LLVMAsmGenerator::visit(*node.exp, ctx);                          \
      ctx.out << " to " Str;                                            \
    }

    INTEGER_TO_FLOAT(Float, "float")
    INTEGER_TO_FLOAT(Double, "double")

#define FLOAT_TO_INTEGER(Float, Str)                                    \
    static void                                                         \
    visitCast##Float##ToIntegerExp(const ast::Node & node_,             \
                                   LLVMAsmGenerator & ctx)              \
    {                                                                   \
      auto & node =                                                     \
        reinterpret_cast<const ast::CastExp &> (node_);                 \
      const ast::IntegerType * to =                                     \
        ast::ast_cast<const ast::IntegerType *>(node.type);             \
                                                                        \
      ctx.out << (to->isSigned ? "fptosi " : "fptoui ") << Str" ";      \
      LLVMAsmGenerator::visit(*node.exp, ctx);                          \
      ctx.out << " to " Str;                                            \
      LLVMAsmGenerator::visit(*to, ctx);                                \
    }

    FLOAT_TO_INTEGER(Float, "float")
    FLOAT_TO_INTEGER(Double, "double")

    static void
    visitCastIntegerToBoolExp(const ast::Node & node_, LLVMAsmGenerator & ctx)
    {
      auto & node = reinterpret_cast<const ast::CastExp &> (node_);
      ctx.out << "icmp ne ";
      LLVMAsmGenerator::visit(*node.exp->type, ctx);
      ctx.out << "0, ";
      LLVMAsmGenerator::visit(*node.exp, ctx);
    }

#if 0
#define FLOAT_TO_BOOL(Type, Str)                                        \
    static void                                                         \
    visitCast##Type##ToBoolExp(const ast::Node & /*node*/,              \
                               LLVMAsmGenerator & /*ctx*/)              \
    {                                                                   \
      /** @todo throw a warning */                                      \
      assert_msg(false, "casting "Str" to bool is a non sense.");       \
    }

    FLOAT_TO_BOOL(Float, "float")
    FLOAT_TO_BOOL(Double, "double")
#endif

    void
    LLVMAsmGenerator::initBase()
    {
#define REGISTER_METHOD(Class)                                  \
      registerMethod(ast::Class::nodeTypeId(), visit##Class)

      REGISTER_METHOD(File);

      REGISTER_METHOD(FunctionDecl);
      REGISTER_METHOD(Function);

      REGISTER_METHOD(VarDecl);

      REGISTER_METHOD(Alloca);
      REGISTER_METHOD(LoadVar);
      REGISTER_METHOD(StoreVar);

      REGISTER_METHOD(Exp);
      REGISTER_METHOD(VoidExp);
      REGISTER_METHOD(SymbolExp);
      REGISTER_METHOD(AtExp);
      REGISTER_METHOD(NumberExp);
      REGISTER_METHOD(StringExp);
      REGISTER_METHOD(CallExp);

      REGISTER_METHOD(AssignExp);
      REGISTER_METHOD(PointerArithExp);
      REGISTER_METHOD(AddExp);
      REGISTER_METHOD(SubExp);
      REGISTER_METHOD(MulExp);
      REGISTER_METHOD(DivExp);
      REGISTER_METHOD(ModExp);

      REGISTER_METHOD(AndExp);
      REGISTER_METHOD(OrExp);
      REGISTER_METHOD(XorExp);

      REGISTER_METHOD(ShlExp);
      REGISTER_METHOD(AShrExp);
      REGISTER_METHOD(LShrExp);

      REGISTER_METHOD(AndAndExp);
      REGISTER_METHOD(OrOrExp);

      REGISTER_METHOD(NotExp);
      REGISTER_METHOD(BangExp);
      REGISTER_METHOD(NegExp);

      REGISTER_METHOD(EqExp);
      REGISTER_METHOD(NeqExp);
      REGISTER_METHOD(LtExp);
      REGISTER_METHOD(LtEqExp);
      REGISTER_METHOD(GtExp);
      REGISTER_METHOD(GtEqExp);

      REGISTER_METHOD(Label);
      REGISTER_METHOD(Goto);
      REGISTER_METHOD(Return);
      REGISTER_METHOD(ConditionalBranch);

      REGISTER_METHOD(Type);
      REGISTER_METHOD(VoidType);
      REGISTER_METHOD(BoolType);
      REGISTER_METHOD(NumberType);
      REGISTER_METHOD(IntegerType);
      REGISTER_METHOD(FloatType);
      REGISTER_METHOD(DoubleType);
      REGISTER_METHOD(PointerType);
      REGISTER_METHOD(ReferenceType);
      REGISTER_METHOD(ArrayType);

      REGISTER_METHOD(CastExp);
      REGISTER_METHOD(MemoryAddress);
      REGISTER_METHOD(RegisterAddress);

#define REGISTER_CAST(Type1, Type2)                     \
      REGISTER_METHOD(Cast##Type1##To##Type2##Exp)

      REGISTER_CAST(Bool, Integer);
      REGISTER_CAST(Bool, Float);
      REGISTER_CAST(Bool, Double);
      REGISTER_CAST(Integer, Bool);
      REGISTER_CAST(Integer, Integer);
      REGISTER_CAST(Integer, Float);
      REGISTER_CAST(Integer, Double);
      //REGISTER_CAST(Float, Bool);
      REGISTER_CAST(Float, Integer);
      REGISTER_CAST(Float, Double);
      //REGISTER_CAST(Double, Bool);
      REGISTER_CAST(Double, Integer);
      REGISTER_CAST(Double, Float);

      REGISTER_CAST(Integer, Pointer);
      REGISTER_CAST(Pointer, Integer);
      //REGISTER_CAST(Array, Pointer);
      //REGISTER_CAST(, );

      completeWith<ConstBrowser<LLVMAsmGenerator> >();
    }
  }
}

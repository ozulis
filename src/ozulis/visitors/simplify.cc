#include <sstream>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/ast/cast-tables.hh>
#include <ozulis/ast/ast-cast.hh>
#include <ozulis/ast/node-factory.hh>
#include <ozulis/ast/scope.hh>
#include <ozulis/visitors/type-checker.hh>
#include <ozulis/visitors/simplify.hh>
#include <ozulis/visitors/browser.hh>
#include <ozulis/visitors/cast-simplifier.hh>

#if 0 // 1 to get a backtrace
# define static
#endif

namespace ozulis
{
  namespace visitors
  {
    /// @defgroup s
    /**
     * @class Simplify
     * @ingroup Visitors
     *
     * <b>Algorithm</b><br>
     * - on Block
     *  - for every instruction
     *   - split the instruction in 3 address instruction
     */

#define SIMPLIFY(Value) (Value) = ctx.simplify(Value)

    Simplify::Simplify()
      : Visitor<Simplify>(),
        simplifications(0),
        replacement(0),
        scope(0),
        nextId_(0)
    {
    }

    Simplify::~Simplify()
    {
    }

    ast::ExpPtr
    Simplify::simplify(ast::ExpPtr node)
    {
      assert(node);
      assert(node->type);

      visit(*node, *this);
      return replacement;
    }

    std::string
    Simplify::nextId()
    {
      std::stringstream ss;
      ss << "$" << ++nextId_;
      return ss.str();
    }

    std::string
    Simplify::currentId() const
    {
      std::stringstream ss;
      ss << "r" << nextId_;
      return ss.str();
    }

    void
    Simplify::makeTmpResult(ast::Exp & node)
    {
      if (node.type->nodeType == ast::VoidType::nodeTypeId())
      {
        replacement = 0;
        simplifications.push_back(&node);
        return;
      }

      /* keep registers and numbers */
      if (node.nodeType == ast::NumberExp::nodeTypeId())
      {
        replacement = &node;
        return;
      }

      if (node.nodeType == ast::SymbolExp::nodeTypeId())
      {
        auto & se = reinterpret_cast<ast::SymbolExp &> (node);
        if (se.symbol->address->nodeType == ast::RegisterAddress::nodeTypeId())
        {
          replacement = &node;
          return;
        }
      }

      ast::SymbolExpPtr sexp = new ast::SymbolExp();
      sexp->symbol = new ast::Symbol;
      sexp->symbol->name = nextId();
      sexp->symbol->type = node.type;
      sexp->type = node.type;
      ast::RegisterAddressPtr addr = new ast::RegisterAddress;
      addr->address = scope->prefix + sexp->symbol->name;
      sexp->symbol->address = addr;

      ast::AssignExpPtr assign = new ast::AssignExp();
      assign->value = &node;
      assign->dest = sexp;
      assign->type = node.type;
      simplifications.push_back(assign);

      replacement = sexp;
    }

    static void visitBlock(ast::Node & node_, Simplify & ctx)
    {
      auto & block = reinterpret_cast<ast::Block &> (node_);

      ctx.scope = block.scope;
      std::vector<ast::NodePtr> backup = ctx.simplifications;
      ctx.simplifications.clear();

      BOOST_FOREACH (ast::VarDecl * varDecl, *block.varDecls)
        Simplify::visit(*varDecl, ctx);
      // We don't need VarDecls anymore
      block.varDecls->clear();

      BOOST_FOREACH (ast::Node * node, (*block.statements))
      {
        assert(node);
        Simplify::visit(*node, ctx);
      }

      *block.statements = ctx.simplifications;
      ctx.simplifications = backup; /// @todo use a pointer
    }

    static void visitVoidExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::VoidExp &> (node_);
      ctx.replacement = &node;
    }

#define SIMPLIFY_UNARY_EXP(Type)                                        \
    /**                                                                 \
     * @internal                                                        \
     * - Simplify childs                                                \
     * - Create a new id.                                               \
     * - Create a new assign exp to id.                                 \
     * - Create a IdExp and tell replace the parent's child with it.    \
     */                                                                 \
    static void visit##Type(ast::Node & node_, Simplify & ctx)          \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      assert(node.exp);                                                 \
      SIMPLIFY(node.exp);                                               \
                                                                        \
      ctx.makeTmpResult(node);                                          \
    }

    SIMPLIFY_UNARY_EXP(NotExp)
    SIMPLIFY_UNARY_EXP(BangExp)
    SIMPLIFY_UNARY_EXP(NegExp)

#define SIMPLIFY_BINARY_EXP(Type)                                       \
    /**                                                                 \
     * @internal                                                        \
     * - Simplify childs                                                \
     * - Create a new id.                                               \
     * - Create a new assign exp to id.                                 \
     * - Create a IdExp and tell replace the parent's child with it.    \
     */                                                                 \
    static void visit##Type(ast::Node & node_, Simplify & ctx)          \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      SIMPLIFY(node.left);                                              \
      SIMPLIFY(node.right);                                             \
                                                                        \
      ctx.makeTmpResult(node);                                          \
    }

    SIMPLIFY_BINARY_EXP(EqExp)
    SIMPLIFY_BINARY_EXP(NeqExp)
    SIMPLIFY_BINARY_EXP(LtExp)
    SIMPLIFY_BINARY_EXP(LtEqExp)
    SIMPLIFY_BINARY_EXP(GtExp)
    SIMPLIFY_BINARY_EXP(GtEqExp)

    SIMPLIFY_BINARY_EXP(AddExp)
    SIMPLIFY_BINARY_EXP(SubExp)
    SIMPLIFY_BINARY_EXP(MulExp)
    SIMPLIFY_BINARY_EXP(DivExp)
    SIMPLIFY_BINARY_EXP(ModExp)

    SIMPLIFY_BINARY_EXP(AndExp)
    SIMPLIFY_BINARY_EXP(OrExp)
    SIMPLIFY_BINARY_EXP(XorExp)

    SIMPLIFY_BINARY_EXP(AndAndExp)
    SIMPLIFY_BINARY_EXP(OrOrExp)

    SIMPLIFY_BINARY_EXP(ShlExp)
    SIMPLIFY_BINARY_EXP(AShrExp)
    SIMPLIFY_BINARY_EXP(LShrExp)

    /// @todo redo it :)
    static void visitAssignExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::AssignExp &> (node_);
      assert(node.value);
      assert(node.type);
      assert(node.dest);

      ctx.simplifyLValue(node.dest);
      SIMPLIFY(node.value);

      ast::SymbolExpPtr dest = ast::ast_cast<ast::SymbolExp *> (node.dest.ptr());
      ast::StoreVarPtr sv = new ast::StoreVar();
      sv->to = dest->symbol;
      sv->value = ctx.replacement; // node.value
      ctx.simplifications.push_back(sv);
      return;
    }

    static void visitNumberExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::NumberExp &> (node_);
      ctx.replacement = &node;
    }

#define BAD_NODE(Type)                                  \
    static void visit##Type(ast::Node & /*node_*/,      \
                            Simplify & /*ctx*/)         \
    {                                                   \
      assert_msg(false, "should never get here.");      \
    }

    BAD_NODE(Exp)
    BAD_NODE(StringExp)
    BAD_NODE(IdExp)

    /** @internal load a symbol if it's in memory */
    static void visitSymbolExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::SymbolExp &> (node_);
      if (node.symbol->address->nodeType == ast::MemoryAddress::nodeTypeId())
      {
        ast::SymbolExpPtr sexp = new ast::SymbolExp;
        sexp->symbol          = new ast::Symbol;
        sexp->symbol->name    = "%" + node.symbol->name + "_" + ctx.nextId();
        sexp->symbol->type    = unreferencedType(node.type);
        ast::RegisterAddressPtr addr = new ast::RegisterAddress;
        addr->address         = sexp->symbol->name;
        sexp->symbol->address = addr;
        sexp->type            = sexp->symbol->type;
        ctx.replacement       = sexp;

        ast::LoadVarPtr lv = new ast::LoadVar;
        lv->to            = sexp->symbol->name;
        lv->from          = &node;
        ctx.simplifications.push_back(lv);
      }
      else
        ctx.replacement = &node;
    }

    static void visitVarDecl(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::VarDecl &> (node_);
      ast::SymbolPtr symbol = ctx.scope->findSymbol(node.name);
      ast::AllocaPtr alloc = new ast::Alloca;
      alloc->symbol = symbol;
      ctx.simplifications.push_back(alloc);
    }

    static void visitValuedVarDecl(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::ValuedVarDecl &> (node_);
      visitVarDecl(node, ctx);
      SIMPLIFY(node.value);
      ast::StoreVarPtr sv = new ast::StoreVar;
      sv->to = ctx.scope->findSymbol(node.name);
      sv->value = ctx.replacement;
      ctx.simplifications.push_back(sv);
      ctx.replacement = 0;
    }

    /** @internal when simplifying an AtExp node, you must get the address
     * of a reference. So in fact there is nothing to do ;-) */
    static void visitAtExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::AtExp &> (node_);
      assert(node.exp->type->nodeType == ast::ReferenceType::nodeTypeId());
      ctx.replacement = node.exp;
      ctx.simplifyLValue(ctx.replacement);
    }

    static void visitDereferenceExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::DereferenceExp &> (node_);
      SIMPLIFY(node.exp);

      ast::LoadVarPtr lv = new ast::LoadVar;
      lv->to            = ctx.scope->prefix + ctx.nextId();
      lv->from          = node.exp;
      ctx.simplifications.push_back(lv);

      ast::SymbolExpPtr id = new ast::SymbolExp;
      id->symbol          = new ast::Symbol;
      id->symbol->name    = lv->to;
      id->symbol->type    = node.type;
      ast::RegisterAddressPtr addr = new ast::RegisterAddress;
      addr->address       = id->symbol->name;
      id->symbol->address = addr;
      id->type            = node.type;
      ctx.replacement     = id;
    }

    /** @internal just do (node.exp + node.index)* */
    static void visitDereferenceByIndexExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::DereferenceByIndexExp &> (node_);
      ast::PointerArithExpPtr add = new ast::PointerArithExp;
      add->pointer                = node.exp;
      add->offset                 = node.index;

      ast::DereferenceExpPtr deref = new ast::DereferenceExp;
      deref->exp                   = (ast::Exp *)add;
      ast::ExpPtr tmp              = deref;

      TypeChecker v;
      v.scope = ctx.scope;
      tmp = v.check(tmp);
      SIMPLIFY(tmp);
      ctx.replacement = tmp;
    }

    static void visitPointerArithExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::PointerArithExp &> (node_);

      SIMPLIFY(node.pointer);
      SIMPLIFY(node.offset);
      ctx.makeTmpResult(node);
    }

    static void visitCallExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::CallExp &> (node_);
      BOOST_FOREACH (ast::ExpPtr & exp, (*node.args))
        SIMPLIFY(exp);

      ctx.makeTmpResult(node);
    }

    /** @internal develop the cast, and simplify it */
    static void visitCastExp(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::CastExp &> (node_);
      SIMPLIFY(node.exp);
      ast::ExpPtr exp = CastSimplifier::simplify(node);
      if (exp->nodeType == ast::NumberExp::nodeTypeId())
        ctx.replacement = exp;
      else
        ctx.makeTmpResult(*exp);
    }

    static void visitLabel(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::Label &> (node_);
      ctx.simplifications.push_back(&node);
    }

    static void visitGoto(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::Goto &> (node_);
      ctx.simplifications.push_back(&node);
    }

    static void visitReturn(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::Return &> (node_);
      SIMPLIFY(node.exp);
      ctx.simplifications.push_back(&node);
    }

    /** @internal
     * @code
     *   if (cond)
     * trueLabel:
     *   { ... }
     *   goto endLabel;
     * falseLabel:
     *   { ... }
     * endLabel:
     * @code
     */
    static void visitIf(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::If &> (node_);
      SIMPLIFY(node.branch->cond);
      ctx.simplifications.push_back(node.branch);

      node.branch->trueLabel = new ast::Label;
      node.branch->trueLabel->name = std::string("if_true_") + ctx.nextId();
      node.branch->falseLabel = new ast::Label;
      node.branch->falseLabel->name = std::string("if_false_") + ctx.nextId();
      node.endLabel = new ast::Label;
      node.endLabel->name = std::string("if_end_") + ctx.nextId();

      ctx.simplifications.push_back(node.branch->trueLabel);
      Simplify::visit(*node.trueBlock, ctx);
      ast::GotoPtr gotoNode = new ast::Goto;
      gotoNode->label = node.endLabel->name;

      ctx.simplifications.push_back(node.branch->falseLabel);
      Simplify::visit(*node.falseBlock, ctx);
      ctx.simplifications.push_back(node.endLabel);
    }

    /** @internal
     * @code
     * beginLabel:
     *   while (cond)
     * trueLabel:
     *   { ... }
     *   goto beginLabel;
     * falseLabel:
     * @code
     */
    static void visitWhile(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::While &> (node_);
      assert(node.block);
      assert(node.branch->cond);

      node.beginLabel = new ast::Label;
      node.beginLabel->name = std::string("do_while_1_") + ctx.nextId();
      node.branch->trueLabel = new ast::Label;
      node.branch->trueLabel->name = std::string("do_while_2_") + ctx.nextId();
      node.branch->falseLabel = new ast::Label;
      node.branch->falseLabel->name = std::string("do_while_3_") + ctx.nextId();

      ctx.simplifications.push_back(node.beginLabel);
      SIMPLIFY(node.branch->cond);
      ctx.simplifications.push_back(node.branch);
      ctx.simplifications.push_back(node.branch->trueLabel);
      Simplify::visit(*node.block, ctx);
      node.block = 0;

      ast::GotoPtr gt = new ast::Goto;
      gt->label = node.beginLabel->name;
      ctx.simplifications.push_back(gt);
      ctx.simplifications.push_back(node.branch->falseLabel);
    }

    /** @internal
     * @code
     * trueLabel:
     *   { ... }
     *   while (cond)
     * falseLabel:
     * @code
     */
    static void visitDoWhile(ast::Node & node_, Simplify & ctx)
    {
      auto & node = reinterpret_cast<ast::DoWhile &> (node_);
      assert(node.block);
      assert(node.branch->cond);

      node.branch->trueLabel = new ast::Label;
      node.branch->trueLabel->name = std::string("do_while_1_") + ctx.nextId();
      node.branch->falseLabel = new ast::Label;
      node.branch->falseLabel->name = std::string("do_while_2_") + ctx.nextId();
      ctx.simplifications.push_back(node.branch->trueLabel);

      Simplify::visit(*node.block, ctx);
      node.block = 0;
      SIMPLIFY(node.branch->cond);

      ctx.replacement = 0;
      ctx.simplifications.push_back(node.branch);
      ctx.simplifications.push_back(node.branch->falseLabel);
    }

    ast::ExpPtr
    Simplify::simplifyLValue(ast::ExpPtr node)
    {
      if (node->nodeType == ast::SymbolExp::nodeTypeId())
        return node;
      if (node->nodeType == ast::DereferenceExp::nodeTypeId())
      {
        ast::DereferenceExpPtr deref = ast::ast_cast<ast::DereferenceExp *>(node);
        deref->exp = simplify(deref->exp);

        assert(deref->exp->nodeType == ast::SymbolExp::nodeTypeId());
        return deref->exp;
      }
      if (node->nodeType == ast::DereferenceByIndexExp::nodeTypeId())
      {
        ast::DereferenceByIndexExpPtr deref =
          ast::ast_cast<ast::DereferenceByIndexExp *>(node);
        deref->exp = simplify(deref->exp);
        deref->index = simplify(deref->index);

        ast::PointerArithExpPtr add = new ast::PointerArithExp;
        add->pointer                = deref->exp;
        add->offset                 = deref->index;
        add->type                   = unreferencedType(deref->exp->type);

        return simplify(add);;
      }
      return node;
    }

    void
    Simplify::initBase()
    {
#define REGISTER_METHOD(Class)                                  \
      registerMethod(ast::Class::nodeTypeId(), visit##Class)

      REGISTER_METHOD(Exp);
      REGISTER_METHOD(VoidExp);
      REGISTER_METHOD(AssignExp);

      REGISTER_METHOD(EqExp);
      REGISTER_METHOD(NeqExp);
      REGISTER_METHOD(LtExp);
      REGISTER_METHOD(LtEqExp);
      REGISTER_METHOD(GtExp);
      REGISTER_METHOD(GtEqExp);
      REGISTER_METHOD(AddExp);
      REGISTER_METHOD(SubExp);
      REGISTER_METHOD(MulExp);
      REGISTER_METHOD(DivExp);
      REGISTER_METHOD(ModExp);

      REGISTER_METHOD(AndExp);
      REGISTER_METHOD(OrExp);
      REGISTER_METHOD(XorExp);

      REGISTER_METHOD(AndAndExp);
      REGISTER_METHOD(OrOrExp);

      REGISTER_METHOD(ShlExp);
      REGISTER_METHOD(AShrExp);
      REGISTER_METHOD(LShrExp);

      REGISTER_METHOD(NotExp);
      REGISTER_METHOD(NegExp);
      REGISTER_METHOD(BangExp);

      REGISTER_METHOD(NumberExp);
      REGISTER_METHOD(StringExp);
      REGISTER_METHOD(IdExp);
      REGISTER_METHOD(SymbolExp);
      REGISTER_METHOD(VarDecl);
      REGISTER_METHOD(ValuedVarDecl);
      REGISTER_METHOD(AtExp);
      REGISTER_METHOD(DereferenceExp);
      REGISTER_METHOD(DereferenceByIndexExp);
      REGISTER_METHOD(PointerArithExp);
      REGISTER_METHOD(CallExp);
      REGISTER_METHOD(Block);

      REGISTER_METHOD(CastExp);

      REGISTER_METHOD(Label);
      REGISTER_METHOD(Goto);
      REGISTER_METHOD(Return);
      REGISTER_METHOD(If);
      REGISTER_METHOD(While);
      REGISTER_METHOD(DoWhile);

      completeWith<Browser<Simplify> >();
    }
  }
}

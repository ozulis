#ifndef VISITORS_SCOPE_BUILDER_HH
# define VISITORS_SCOPE_BUILDER_HH

# include <ozulis/ast/scope.hh>
# include <ozulis/visitors/visitor.hh>

namespace ozulis
{
  namespace visitors
  {
    /// @defgroup Visitors
    /**
     * @brief Creates symbol tables
     * @ingroup Visitors
     */
    class ScopeBuilder : public Visitor<ScopeBuilder>
    {
    public:
      ScopeBuilder();

      std::string nextId();

      typedef std::vector<uint32_t> blockIds_t;

      ast::FilePtr file;
      ast::Scope * parent;
      blockIds_t   blockIds;

      static void initBase();

    private:
      int32_t nextId_;
    };
  }
}

#endif /* !VISITORS_SCOPE_BUILDER_HH */

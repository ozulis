#include <boost/foreach.hpp>
#include <boost/format.hpp>

#include <ozulis/compiler.hh>
#include <ozulis/core/assert.hh>
#include <ozulis/ast/ast-cast.hh>
#include <ozulis/ast/node-factory.hh>
#include <ozulis/ast/cast-tables.hh>
#include <ozulis/ast/scope.hh>
#include <ozulis/visitors/type-checker.hh>
#include <ozulis/visitors/browser.hh>

#if 0 // 1 to get a backtrace
# define static
#endif

#define CHECK(Node) Node = ctx.check(Node)

namespace ozulis
{
  namespace visitors
  {
    TypeChecker::TypeChecker()
      : Visitor<TypeChecker>(),
        currentFunction(0),
        scope(0),
        replacement(0)
    {
    }

    TypeChecker::~TypeChecker()
    {
    }

    template <typename T>
    T
    TypeChecker::check(T node)
    {
      replacement = 0;
      TypeChecker::visit(*node, *this);
      if (!replacement)
        return node;

      node = ast::ast_cast<typename T::ptr_t> (replacement.ptr());
      replacement = 0;
      return node;
    }

    /** @internal keep the file's scope and visit the file */
    static void visitFile(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::File &>(node_);
      ctx.scope = node.scope;
      Browser<TypeChecker>::visit(node, ctx);
    }

    static void visitFunction(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::Function &>(node_);
      ctx.scope = node.scope;
      ctx.currentFunction = &node;
      Browser<TypeChecker>::visit(node, ctx);
      /// @todo check that the last statement is a branch or a return.
      /// maybe it should be done in the simplifier ?
    }

    /** @internal get the scope, visit declarations, visit statements */
    static void visitBlock(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::Block &>(node_);
      ctx.scope = node.scope;
      // We must respect this order
      BOOST_FOREACH (ast::VarDeclPtr & varDecl, (*node.varDecls))
        CHECK(varDecl);
      BOOST_FOREACH (ast::NodePtr & statement, (*node.statements))
        CHECK(statement);
    }

    /** @internal Let's put a cast. The simplifier will  fail if the cast
     * is not possible */
    static void visitReturn(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::Return &>(node_);
      CHECK(node.exp);
      node.exp = castToType(ctx.currentFunction->returnType, node.exp);
    }

    /** @internal check that the type of the value is compatible with
     * the type of the destination. If not put a cast. */
    static void visitAssignExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::AssignExp &>(node_);
      CHECK(node.dest);
      CHECK(node.value);

      auto castExp = new ast::CastExp();
      castExp->type = unreferencedType(node.dest->type);
      castExp->exp = node.value;
      node.value = castExp;
      node.type = castExp->type;
    }

    /** @internal If the type is empty, check the symbol and assign the
     * type of the symbol to the node. */
    static void visitSymbolExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::SymbolExp &> (node_);
      if (!node.type)
      {
        TypeChecker::visit(*node.symbol, ctx);
        node.type = node.symbol->type;
      }
    }

    /** @internal resolve the symbol corresponding to the id. Then
     * create a SymbolExp, and replace IdExp by the SymbolExp */
    static void visitIdExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::IdExp &>(node_);
      assert(ctx.scope);
      ast::Symbol * s = ctx.scope->findSymbol(node.id);
      assert(s);
      assert(s->type);
      assert(s->address);
      auto exp = new ast::SymbolExp;
      exp->symbol = s;
      exp->type = s->type;
      ctx.replacement = exp;
    }

    /** @internal should never append: StringExp are removed by the
     * ScopeBuilder */
    static void visitStringExp(ast::Node & /*node_*/, TypeChecker & /*ctx*/)
    {
      assert_msg(false, "we should never come here");
    }

    /** @internal type the expression as a pointer to the child expression.
     * If the child expression is typed as a reference, use the underlying
     * type of the reference for the type of the pointer.
     * It's not the role of the type checker to check if we can get the
     * address of the child exp. The simplifier will do this job. */
    static void visitAtExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::AtExp &>(node_);
      CHECK(node.exp);
      auto type = new ast::PointerType;
      type->type = unreferencedType(node.exp->type);
      node.type = type;
    }

    /** @internal obvious, will comment it later */
    static void visitDereferenceExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::DereferenceExp &>(node_);
      CHECK(node.exp);
      ast::Type * unrefType = unreferencedType(node.exp->type);
      if (unrefType->nodeType != ast::PointerType::nodeTypeId())
        Compiler::instance().error(_("you can't dereference a non pointer type"));
      auto type = ast::ast_cast<ast::PointerType *> (unrefType);
      node.type = type->type;
    }

    /** @internal obvious, will comment it later */
    static void visitDereferenceByIndexExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::DereferenceByIndexExp &>(node_);
      CHECK(node.exp);
      CHECK(node.index);
      ast::Type * unrefType = unreferencedType(node.exp->type);
      if (unrefType->nodeType != ast::PointerType::nodeTypeId() &&
          unrefType->nodeType != ast::ArrayType::nodeTypeId())
        Compiler::instance().error(_("Error: you can't dereference a non "
                                     "pointer or array type"));
      auto type = reinterpret_cast<ast::UnaryType *> (unrefType);
      node.type = type->type;

      ast::IntegerType * itype = ast::NodeFactory::createUIntForPtr();
      node.index = castToType(itype, node.index);
    }

    static void visitPointerArithExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::PointerArithExp &>(node_);
      CHECK(node.pointer);
      CHECK(node.offset);
      node.type = unreferencedType(node.pointer->type);
      if (node.type->nodeType != ast::PointerType::nodeTypeId())
        Compiler::instance().error(
          _("you can't do pointer arithmetics on a non pointer type"));
    }

    /** @internal we should resolve named type here */
    static void visitSymbol(ast::Node & /*node_*/, TypeChecker & /*ctx*/)
    {
      //assert_msg(false, "should not come here");
    }

    /** @internal just check the child exp */
    static void visitCastExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::CastExp &>(node_);
      assert(node.exp);
      CHECK(node.exp);
    }

    /** @internal at the begining, node.function is an IdExp. We have
     * to resolve the symbol. Then put casts force the right type. */
    static void visitCallExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::CallExp &>(node_);
      CHECK(node.function);

      ast::SymbolExp * sexp = ast::ast_cast<ast::SymbolExp *>(node.function);
      ast::FunctionType * ftype = ast::ast_cast<ast::FunctionType *>(sexp->symbol->type);

      node.type = ftype->type;
      assert(ftype->argsType->size() >= node.args->size());
      for (unsigned i = 0; i < node.args->size(); i++)
      {
        CHECK((*node.args)[i]);
        (*node.args)[i] = castToType((*ftype->argsType)[i], (*node.args)[i]);
      }
    }

    /** @internal
     * 1) find the best type
     * 2) find the "bad" member and use the cast for it */
    void
    TypeChecker::homogenizeTypes(ast::BinaryExp & node)
    {
      ast::CastExp * castExp = ast::castToBestType(node.left->type,
                                                   node.right->type);
      if (!castExp)
      {
        node.type = unreferencedType(node.left->type);
        return;
      }

      if (castExp->type == unreferencedType(node.left->type))
      {
        castExp->exp = node.right;
        node.right = castExp;
      }
      else
      {
        castExp->exp = node.left;
        node.left = castExp;
      }
      node.type = castExp->type;
    }

    /**
     * @internal
     *  - compute the new address:
     *   - addrui = ptrtoui addr
     *   - offset = index * sizeof(*addr)
     *   - newaddrui = ptrtoui + offset
     *   - newaddr = cast(*, newaddrui)
     */
    void
    TypeChecker::pointerArith(ast::Exp * pointer, ast::Exp * offset)
    {
#if 0 /// move this to the simplifier or use getelementptr ?
      ast::PointerType * pointerType =
        ast::ast_cast<ast::PointerType *> (unreferencedType(pointer->type));

      /* cast node.exp to uint */
      ast::CastExp * cast1 = ast::NodeFactory::createCastPtrToUInt(pointer);

      /* compute the offset */
      ast::MulExp * mul = new ast::MulExp;
      mul->left = offset;
      mul->right = ast::NodeFactory::createSizeofValue(pointerType->type);

      /* add the offset to the uint addr */
      ast::AddExp * add = new ast::AddExp;
      add->left = cast1;
      add->right = mul;

      /* cast uint addr to ptr */
      ast::CastExp * cast2 = ast::NodeFactory::
        createCastUIntToPtr(add, unreferencedType(pointerType));
      check(cast2);

      replacement = cast2;
#endif

      ast::PointerArithExp * exp = new ast::PointerArithExp;
      exp->pointer = pointer;
      exp->offset = offset;
      exp->type = unreferencedType(pointer->type);
      replacement = exp;
    }

/** @internal there is a particular case here for pointer arithmetics */
#define VISIT_ADD_EXP(Type)                                             \
    static void visit##Type(ast::Node & node_, TypeChecker & ctx)       \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      CHECK(node.left);                                                 \
      CHECK(node.right);                                                \
      if (unreferencedType(node.left->type)->nodeType ==                \
          ast::PointerType::nodeTypeId())                               \
        ctx.pointerArith(node.left, node.right);                        \
      else if (unreferencedType(node.right->type)->nodeType ==          \
               ast::PointerType::nodeTypeId())                          \
        ctx.pointerArith(node.right, node.left);                        \
      else                                                              \
        TypeChecker::homogenizeTypes(node);                             \
    }

#define VISIT_BINARY_ARITH(Type)                                        \
    static void visit##Type(ast::Node & node_, TypeChecker & ctx)       \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      CHECK(node.left);                                                 \
      CHECK(node.right);                                                \
      TypeChecker::homogenizeTypes(node);                               \
    }

    VISIT_ADD_EXP(AddExp)
    VISIT_ADD_EXP(SubExp)
    VISIT_BINARY_ARITH(MulExp)
    VISIT_BINARY_ARITH(DivExp)
    VISIT_BINARY_ARITH(ModExp)

#define VISIT_BITWISE_EXP(Type)                                         \
    static void visit##Type(ast::Node & node_, TypeChecker & ctx)       \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      CHECK(node.left);                                                 \
      CHECK(node.right);                                                \
                                                                        \
      if (AST_IS_FLOATING_POINT_TYPE(node.left->type) ||                \
          AST_IS_FLOATING_POINT_TYPE(node.right->type))                 \
      {                                                                 \
        Compiler::instance().error(_("you can't do bitwise operation with " \
                                     "floating point value."));         \
      }                                                                 \
      TypeChecker::homogenizeTypes(node);                               \
    }

    VISIT_BITWISE_EXP(AndExp)
    VISIT_BITWISE_EXP(OrExp)
    VISIT_BITWISE_EXP(XorExp)

    VISIT_BITWISE_EXP(ShlExp)
    VISIT_BITWISE_EXP(AShrExp)
    VISIT_BITWISE_EXP(LShrExp)

#define VISIT_BINARY_CMP_EXP(Type)                                      \
    static void visit##Type(ast::Node & node_, TypeChecker & ctx)       \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      CHECK(node.left);                                                 \
      CHECK(node.right);                                                \
      /**                                                               \
       * @todo check if type match                                      \
       * @todo look for an overloaded operator '+'                      \
       * @todo the cast can be applied on both nodes                    \
       */                                                               \
      TypeChecker::homogenizeTypes(node);                               \
      node.type = ast::NodeFactory::createBoolType();                   \
    }

    VISIT_BINARY_CMP_EXP(EqExp)
    VISIT_BINARY_CMP_EXP(NeqExp)
    VISIT_BINARY_CMP_EXP(LtExp)
    VISIT_BINARY_CMP_EXP(LtEqExp)
    VISIT_BINARY_CMP_EXP(GtExp)
    VISIT_BINARY_CMP_EXP(GtEqExp)

#define VISIT_BINARY_BOOL_EXP(Type)                                     \
    static void visit##Type(ast::Node & node_, TypeChecker & ctx)       \
    {                                                                   \
      auto & node = reinterpret_cast<ast::Type &> (node_);              \
      node.type = ast::NodeFactory::createBoolType();                   \
                                                                        \
      CHECK(node.left);                                                 \
      CHECK(node.right);                                                \
                                                                        \
      if (AST_IS_FLOATING_POINT_TYPE(node.left->type) ||                \
          AST_IS_FLOATING_POINT_TYPE(node.right->type))                 \
      {                                                                 \
        Compiler::instance().error(_("you can't do bitwise operation with " \
                                     "floating point value."));         \
      }                                                                 \
      node.left = castToType(node.type, node.left);                     \
      node.right = castToType(node.type, node.right);                   \
    }

    VISIT_BINARY_BOOL_EXP(OrOrExp)
    VISIT_BINARY_BOOL_EXP(AndAndExp)

    static void visitNegExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::NegExp &>(node_);
      CHECK(node.exp);
      node.type = node.exp->type;
    }

    static void visitNotExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::NotExp &>(node_);
      CHECK(node.exp);
      if (AST_IS_FLOATING_POINT_TYPE(node.exp->type))
        Compiler::instance().error(_("you can't do bitwise operation with "
                                     "floating point value."));
      node.type = node.exp->type;
    }

    static void visitBangExp(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::BangExp &>(node_);
      CHECK(node.exp);
      if (AST_IS_FLOATING_POINT_TYPE(node.exp->type))
        Compiler::instance().error(_("you can't do bitwise operation with "
                                     "floating point value."));
      node.type = ast::NodeFactory::createBoolType();
      node.exp = castToType(node.type, node.exp);
    }

    static void visitConditionalBranch(ast::Node & node_, TypeChecker & ctx)
    {
      auto & node = reinterpret_cast<ast::ConditionalBranch &>(node_);
      CHECK(node.cond);
      node.cond = castToType(ast::NodeFactory::createBoolType(), node.cond);
    }

    void
    TypeChecker::initBase()
    {
#define REGISTER_METHOD(Class)                                  \
      registerMethod(ast::Class::nodeTypeId(), visit##Class)

      REGISTER_METHOD(AssignExp);

      REGISTER_METHOD(AddExp);
      REGISTER_METHOD(SubExp);
      REGISTER_METHOD(MulExp);
      REGISTER_METHOD(DivExp);
      REGISTER_METHOD(ModExp);

      REGISTER_METHOD(AndExp);
      REGISTER_METHOD(OrExp);
      REGISTER_METHOD(XorExp);

      REGISTER_METHOD(ShlExp);
      REGISTER_METHOD(AShrExp);
      REGISTER_METHOD(LShrExp);

      REGISTER_METHOD(OrOrExp);
      REGISTER_METHOD(AndAndExp);

      REGISTER_METHOD(EqExp);
      REGISTER_METHOD(NeqExp);
      REGISTER_METHOD(LtExp);
      REGISTER_METHOD(LtEqExp);
      REGISTER_METHOD(GtExp);
      REGISTER_METHOD(GtEqExp);

      REGISTER_METHOD(NotExp);
      REGISTER_METHOD(NegExp);
      REGISTER_METHOD(BangExp);

      REGISTER_METHOD(IdExp);
      REGISTER_METHOD(SymbolExp);
      REGISTER_METHOD(StringExp);
      REGISTER_METHOD(AtExp);
      REGISTER_METHOD(DereferenceExp);
      REGISTER_METHOD(DereferenceByIndexExp);
      REGISTER_METHOD(PointerArithExp);
      REGISTER_METHOD(CastExp);
      REGISTER_METHOD(CallExp);

      REGISTER_METHOD(Symbol);

      REGISTER_METHOD(File);
      REGISTER_METHOD(Function);
      REGISTER_METHOD(Block);

      REGISTER_METHOD(Return);

      REGISTER_METHOD(ConditionalBranch);

      completeWith<Browser<TypeChecker> >();
    }
  }
}

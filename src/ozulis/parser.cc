#include <stdio.h>
#include <string>
#include <errno.h>
#include <string.h>

#include <boost/format.hpp>
#include <boost/filesystem/operations.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/parser.hh>
#include <ozulis/compiler.hh>

namespace ozulis
{
  Parser::~Parser()
  {
  }

  ast::FilePtr
  Parser::parseFile(const boost::filesystem::path & path)
  {
    if (!boost::filesystem::is_regular_file(path))
      Compiler::instance().error(
          (boost::format(_("%1%: no such file.")) % path).str());

    assert(!path.empty());
    FILE * stream = fopen(path.string().c_str(), "r");

    if (!stream)
      Compiler::instance().error(
          (boost::format(_("%1%: %2%.")) % path % strerror(errno)).str());

    ast::FilePtr file = parse(stream);
    fclose(stream);
    return file;
  }
}

#ifndef PLUGIN_HH
# define PLUGIN_HH

# include <string>
# include <vector>

# include <ozulis/parser.hh>

# define EXPORT_PLUGIN(Class)                   \
  Class ozulis_plugin;

namespace ozulis
{
  class Plugin
  {
  public:
    enum PluginType {
      Abstract,
      Language,
    };

    /** @brief this function is called when the plugin is loaded */
    virtual void loaded() const {}
    virtual PluginType type() const { return Abstract; }
    virtual const std::string & name() const = 0;
    virtual const std::string & version() const = 0;

    void * handle;
  };

  class LanguagePlugin : public Plugin
  {
  public:
    virtual PluginType type() const { return Language; }
    virtual const std::string & languageName() const = 0;
    virtual const std::vector<std::string> & extensions() const = 0;
    virtual Parser * createParser() = 0;
  };
}

#endif /* !PLUGIN_HH */

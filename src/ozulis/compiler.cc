#include <iostream>
#include <fstream>
#include <algorithm>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/core/string.hh>
#include <ozulis/core/process.hh>
#include <ozulis/ast/ast.hh>
#include <ozulis/parser-factory.hh>
#include <ozulis/compiler.hh>
#include <ozulis/task.hh>
#include <ozulis/parse-task.hh>
#include <ozulis/plugin-manager.hh>
#include <ozulis/target-data-factory.hh>
#include <ozulis/visitors/scope-builder.hh>
#include <ozulis/visitors/browser.hh>
#include <ozulis/visitors/type-checker.hh>
#include <ozulis/visitors/ascii-printer.hh>
#include <ozulis/visitors/simplify.hh>
#include <ozulis/visitors/llvm-asm-generator.hh>

template class ozulis::core::Singleton<ozulis::Compiler>;

namespace ozulis
{
  /**
   * @class Compiler
   * @todo get the targetData from options. The default option is native.
   */
  Compiler::Compiler()
    : core::Singleton<Compiler>(),
      tasks_(),
      astDumpParser_(false),
      astDumpTypeChecker_(false),
      astDumpSimplify_(false),
      astDumpScopeBuilder_(false),
      targetData_(TargetDataFactory::native())
  {
    visitors::Browser<visitors::ScopeBuilder>::Base::instance();
    PluginManager::instance().addPluginPath(OZULIS_PLUGINDIR);
  }

  Compiler::~Compiler()
  {
  }

  void
  Compiler::error(const std::string & message)
  {
    std::cerr << _("Error: ") << message << std::endl;
    abort();
  }

  void
  Compiler::abort()
  {
    exit(1);
  }

  void
  Compiler::addTask(Task * task)
  {
    assert(task);
    std::vector<Task *> * deps = task->dependancies();
    if (deps)
    {
      BOOST_FOREACH(Task * dep, *deps)
        addTask(dep);
      delete deps;
    }
    tasks_.push_back(task);
  }

  void
  Compiler::compile()
  {
    std::sort(tasks_.begin(), tasks_.end());
    std::unique(tasks_.begin(), tasks_.end());
    BOOST_FOREACH(Task * task, tasks_)
    {
      assert(task);
      task->accept(*this);
      /// @todo do this again
      delete task;
    }
  }

  void
  Compiler::work(Task & /*task*/)
  {
    assert(false);
  }

  void
  Compiler::work(ParseTask & task)
  {
    ast::FilePtr file = lang::ParserFactory::parseFile(task.filename);
    file->path = task.filename;
    files_[file->path] = file;

    if (astDumpParser_) {
      std::cout << "Parsed AST" << std::endl;
      visitors::AsciiPrinter v;
      visitors::AsciiPrinter::visit(*file, v);
    }

    visitors::ScopeBuilder sb;
    visitors::ScopeBuilder::visit(*file.ptr(), sb);
    if (astDumpScopeBuilder_) {
      std::cout << "ScopeBuilder" << std::endl;
      visitors::AsciiPrinter v;
      visitors::AsciiPrinter::visit(*file, v);
    }

    visitors::TypeChecker tc;
    visitors::TypeChecker::visit(*file, tc);
    if (astDumpTypeChecker_) {
      std::cout << "TypeChecked AST" << std::endl;
      visitors::AsciiPrinter v;
      visitors::AsciiPrinter::visit(*file, v);
    }

    visitors::Simplify sv;
    visitors::Simplify::visit(*file, sv);
    if (astDumpSimplify_) {
      std::cout << "Simplified AST" << std::endl;
      visitors::AsciiPrinter v;
      visitors::AsciiPrinter::visit(*file, v);
    }

    std::string asm_filename(task.filename);
    core::replace_extension(asm_filename, "ll");
    std::ofstream asm_stream(asm_filename.c_str(),
                             std::ios_base::out | std::ios_base::trunc);
    visitors::LLVMAsmGenerator lag(asm_stream);
    visitors::LLVMAsmGenerator::visit(*file, lag);
    asm_stream.close();

    core::Process llvm_as;
    llvm_as.setBinary("llvm-as");
    llvm_as.addArg("-f");
    llvm_as.addArg(asm_filename);
    llvm_as.run();
    assert(!llvm_as.wait());

    std::string bc_filename(asm_filename);
    core::replace_extension(bc_filename, "bc");
    std::string bin_filename(asm_filename);
    core::replace_extension(bin_filename, "");
    core::Process llvm_ld;
    llvm_ld.setBinary("llvm-ld");
    llvm_ld.addArg("-native");
    llvm_ld.addArg("-o");
    llvm_ld.addArg(bin_filename);
    llvm_ld.addArg(bc_filename);
    llvm_ld.run();
    assert(!llvm_ld.wait());
  }
}

#ifndef TARGET_DATA_FACTORY_HH
# define TARGET_DATA_FACTORY_HH

# include <ozulis/target-data.hh>

namespace ozulis
{
  class TargetDataFactory
  {
  public:
    static const TargetData * native();
    static const TargetData * amd64();
    static const TargetData * x86();
  };
}

#endif /* !TARGET_DATA_FACTORY_HH */

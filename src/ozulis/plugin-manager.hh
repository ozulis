#ifndef PLUGIN_MANAGER_HH
# define PLUGIN_MANAGER_HH

# include <string>
# include <set>
# include <vector>

# include <boost/filesystem.hpp>

# include <ozulis/core/singleton.hh>
# include <ozulis/parser.hh>
# include <ozulis/plugin.hh>

namespace ozulis
{
  /**
   * @brief this class is used to load plugins
   */
  class PluginManager : public core::Singleton<PluginManager>
  {
  public:
    PluginManager();
    ~PluginManager();

    typedef Parser * (*CreateParser_f)();

    /**
     * @brief create a parser which can parse the language denoted by extension.
     * @param extension the file's extention
     * @return a valid parser or 0 if there is no support for this kind of file
     */
    Parser * createParserByExtension(const std::string & extension) const;

    /**
     * @brief Will load plugins in this path
     */
    void addPluginPath(const boost::filesystem::path & path);

    void registerPlugin(LanguagePlugin * plugin);

  private:
    void loadPlugin(const boost::filesystem::path & path);
    void loadPlugins(const boost::filesystem::path & path);

    typedef std::vector<LanguagePlugin *> languages_t;
    typedef std::set<boost::filesystem::path> pluginPathes_t;

    languages_t languages_;
    pluginPathes_t pluginPathes_;
  };
}

extern template class ozulis::core::Singleton<ozulis::PluginManager>;
extern template class std::vector<ozulis::LanguagePlugin *>;

#endif /* !PLUGIN_MANAGER_HH */

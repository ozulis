#ifndef LANG_PARSER_FACTORY_HH
# define LANG_PARSER_FACTORY_HH

# include <string>
# include <boost/filesystem/path.hpp>

# include <ozulis/ast/ast.hh>

namespace ozulis
{
  namespace lang
  {
    class ParserFactory
    {
    public:
      static ast::FilePtr parseFile(const boost::filesystem::path & path);
    };
  }
}

#endif /* !LANG_PARSER_FACTORY_HH */

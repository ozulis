#include <dlfcn.h>

#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/format.hpp>

#include <ozulis/compiler.hh>
#include <ozulis/core/assert.hh>
#include "plugin-manager.hh"

namespace fs = boost::filesystem;

template class ozulis::core::Singleton<ozulis::PluginManager>;
template class std::vector<ozulis::LanguagePlugin *>;

namespace ozulis
{
  PluginManager::PluginManager()
    : core::Singleton<PluginManager>(),
      languages_(),
      pluginPathes_()
  {
  }

  PluginManager::~PluginManager()
  {
    BOOST_FOREACH (Plugin * plugin, languages_)
    {
      void * handle = plugin->handle;
      dlclose(handle);
    }
  }

  void
  PluginManager::addPluginPath(const fs::path & path)
  {
    if (!fs::is_directory(path) || pluginPathes_.count(path) > 0)
      return;

    pluginPathes_.insert(path);
    loadPlugins(path);
  }

  void
  PluginManager::loadPlugin(const fs::path & path)
  {
    if (!fs::is_regular_file(path))
      return;

    void * handle = dlopen(path.string().c_str(), RTLD_LAZY);
    assert(handle);
    Plugin * plugin = reinterpret_cast<Plugin *> (dlsym(handle, "ozulis_plugin"));
    assert(plugin);
    plugin->handle = handle;

    switch (plugin->type())
    {
    case Plugin::Language:
      registerPlugin(reinterpret_cast<LanguagePlugin *> (plugin));
      break;
    default: assert(false);
    }
  }

  void
  PluginManager::loadPlugins(const fs::path & path)
  {
    assert(fs::is_directory(path));

    for (fs::directory_iterator it(path), end; it != end; ++it)
      loadPlugin(it->path());
  }

  void
  PluginManager::registerPlugin(LanguagePlugin * plugin)
  {
    languages_.push_back(plugin);
  }

  Parser *
  PluginManager::createParserByExtension(const std::string & extension) const
  {
    BOOST_FOREACH(LanguagePlugin * plugin, languages_)
      BOOST_FOREACH(const std::string & langExtension, plugin->extensions())
        if (extension == langExtension)
          return plugin->createParser();
    Compiler::instance().error(
        (boost::format(_("No plugin to handle `%1%' file extension."))
         % extension).str());
    return 0;
  }
}

#ifndef CORE_ID_HH
# define CORE_ID_HH

# include <stdint.h>

# include <vector>
# include <map>
# include <set>

# include <ozulis/core/singleton.hh>

namespace ozulis
{
  namespace core
  {
    typedef int32_t id_t;

    /**
     * @brief this template delivers ids for a Base and remember child/parent
     * associations
     */
    template <typename Base>
    class IdBase : public Singleton<IdBase<Base> >
    {
    public:
      IdBase();

      /** @brief get the next available id */
      static id_t nextId();
      /** @brief make the assocation between two types */
      static void setParent(id_t parent, id_t child);
      /** @brief get the parent of a type */
      static id_t parent(id_t child);
      /** @brief check that child is the child of parent */
      static bool check(id_t parent, id_t child);
      /** @brief get the count of delivered id */
      static inline unsigned count() { return IdBase<Base>::instance().nextId_; }

    private:
      typedef std::map<id_t, std::set<id_t> > childs_t;
      typedef std::vector<id_t>               parents_t;

      childs_t  childs_;
      parents_t parents_;
      id_t      nextId_;
    };

    /**
     * @brief associate an id to a Base and a Type
     */
    template <typename Base, typename Type>
    class Id
    {
    public:
      static id_t id();
    };

    /**
     * @brief make the association between a Parent and a Type
     */
    template <typename Base, typename Parent, typename Type>
    class IdP
    {
    public:
      static id_t id();
    };
  }
}

extern template class std::map<ozulis::core::id_t,
                               std::set<ozulis::core::id_t> >;
extern template class std::vector<ozulis::core::id_t>;

#endif /* !ID_HH */

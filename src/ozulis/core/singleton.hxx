namespace ozulis
{
  namespace core
  {
    template <typename T>
    inline T&
    Singleton<T>::instance()
    {
      static T instance;
      static bool init = true;

      if (init)
      {
        init = false;
        instance.init();
      }

      return instance;
    }
  }
}

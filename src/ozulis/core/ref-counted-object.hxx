#include <stdio.h>

namespace ozulis
{
  namespace core
  {
    RefCountedObject::RefCountedObject()
      : refCount_(0)
    {
    }

    uint32_t
    RefCountedObject::refCount() const
    {
      return refCount_;
    }

    template <class T>
    template <class V>
    RefCountedObject::Ptr<T>::Ptr(const Ptr<V> & other)
      : ptr_(const_cast<V *> (other.ptr()))
    {
      if (ptr_)
      {
        ptr_->use();
        assert(ptr_->refCount() >= 2);
      }
    }

    template <class T>
    RefCountedObject::Ptr<T>::Ptr(const Ptr<T> & other)
      : ptr_(const_cast<T *> (other.ptr()))
    {
      if (ptr_)
      {
        ptr_->use();
        assert(ptr_->refCount() >= 2);
      }
    }

    template <class T>
    RefCountedObject::Ptr<T>::Ptr()
      : ptr_(0)
    {
    }

    template <class T>
    RefCountedObject::Ptr<T>::Ptr(T * _ptr)
      : ptr_(_ptr)
    {
      if (_ptr)
        _ptr->use();
    }

    template <class T>
    void
    RefCountedObject::Ptr<T>::operator=(T const * _ptr)
    {
      if (_ptr == ptr_)
        return;

      if (ptr_)
        ptr_->release();
      ptr_ = const_cast<T *> (_ptr);
      if (ptr_)
        ptr_->use();
    }

    template <class T>
    template <class V>
    void
    RefCountedObject::Ptr<T>::operator=(Ptr<V> const & _ptr)
    {
      if (_ptr.ptr() == ptr_)
        return;

      if (ptr_)
        ptr_->release();
      ptr_ = const_cast<T *> (static_cast<const T *> (_ptr));
      if (ptr_)
        ptr_->use();
    }

    template <class T>
    void
    RefCountedObject::Ptr<T>::operator=(Ptr<T> const & _ptr)
    {
      if (_ptr.ptr() == ptr_)
        return;

      if (ptr_)
        ptr_->release();
      ptr_ = const_cast<T *>(_ptr.ptr());
      if (ptr_)
        ptr_->use();
    }

    template <class T>
    RefCountedObject::Ptr<T>::~Ptr()
    {
      if (ptr_)
        ptr_->release();
      ptr_ = 0;
    }

    template <class T>
    T *
    RefCountedObject::Ptr<T>::operator->()
    {
      return ptr_;
    }

    template <class T>
    const T *
    RefCountedObject::Ptr<T>::operator->() const
    {
      return ptr_;
    }

    template <class T>
    T &
    RefCountedObject::Ptr<T>::operator*()
    {
      return *ptr_;
    }

    template <class T>
    const T &
    RefCountedObject::Ptr<T>::operator*() const
    {
      return *ptr_;
    }

    template <class T>
    RefCountedObject::Ptr<T>::operator T *()
    {
      return ptr_;
    }

    template <class T>
    RefCountedObject::Ptr<T>::operator const T *() const
    {
      return ptr_;
    }

    template <class T>
    RefCountedObject::Ptr<T>::operator bool() const
    {
      return ptr_ != 0;
    }

    template <class T>
    template <class V>
    RefCountedObject::Ptr<T>::operator RefCountedObject::Ptr<V>()
    {
      /// @todo use static_cast ;-)
      return Ptr<V>(reinterpret_cast<V *> (ptr_));
    }

    template <class T>
    T *
    RefCountedObject::Ptr<T>::ptr()
    {
      return ptr_;
    }

    template <class T>
    const T *
    RefCountedObject::Ptr<T>::ptr() const
    {
      return ptr_;
    }
  }
}

#ifndef CORE_SINGLETON_HH
# define CORE_SINGLETON_HH

namespace ozulis
{
  namespace core
  {
    template <typename T>
    class Singleton
    {
    public:
      static inline T& instance();

    protected:
      static void init() {}
    };
  }
}

# include "singleton.hxx"

#endif /* !CORE_SINGLETON_HH */

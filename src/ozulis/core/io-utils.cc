#include "io-utils.hh"

namespace ozulis
{
  namespace core
  {
    void readAll(std::istream & is, std::string & out)
    {
      char data[64 << 10];

      while (!is.eof() && is.good())
      {
        is.read(data, sizeof (data));
        out.append(data, is.gcount());
      }
    }
  }
}

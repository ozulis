#ifndef OZULIS_CORE_REF_COUNTED_OBJECT_HH
# define OZULIS_CORE_REF_COUNTED_OBJECT_HH

# include <stdint.h>

namespace ozulis
{
  namespace core
  {
    class RefCountedObject
    {
    public:
      inline RefCountedObject();
      virtual ~RefCountedObject();

      inline uint32_t refCount() const;
      void use();
      void release();

      template <class T>
      class Ptr
      {
      public:
        typedef T * ptr_t;

        inline Ptr();
        template <class V>
        inline Ptr(const Ptr<V> &);
        inline Ptr(const Ptr<T> &);
        inline Ptr(T * ptr);
        inline ~Ptr();

        inline void operator=(T const * ptr);
        template <class V>
        inline void operator=(Ptr<V> const & ptr);
        inline void operator=(Ptr<T> const & ptr);

        inline T & operator*();
        inline const T & operator*() const;

        inline T * operator->();
        inline const T * operator->() const;

        inline operator T *();
        inline operator const T *() const;
        inline operator bool() const;
        template <class V>
        inline operator Ptr<V> ();

        inline T * ptr();
        inline const T * ptr() const;

      private:
        T * ptr_;
      };

    private:
      int32_t refCount_;
    };
  }
}

# include "ref-counted-object.hxx"

#endif /* !OZULIS_CORE_REF_COUNTED_OBJECT_HH */

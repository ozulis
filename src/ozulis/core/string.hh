#ifndef STRING_HH
# define STRING_HH

# include <string>

namespace ozulis
{
  namespace core
  {
    /*!
    ** @brief get the extension of a filename
    ** @param filename the filename
    */
    std::string get_extension(const std::string & filename);

    /*!
    ** @brief replaces an extension in a filename
    ** @param filename the filename
    ** @param extention the extention without the dot '.'
    */
    void replace_extension(std::string & filename, const std::string & extension);
  }
}

#endif /* !STRING_HH */

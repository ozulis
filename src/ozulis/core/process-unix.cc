#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include <boost/foreach.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>

#include "process.hh"

namespace bio = boost::iostreams;

namespace ozulis
{
  namespace core
  {
    class ProcessPrivate
    {
    public:
      ProcessPrivate();

      pid_t pid;
    };

    ProcessPrivate::ProcessPrivate()
      : pid(0)
    {
    }

    Process::Process()
      : binary_(),
        args_(),
        ios_(),
        setupFds_(),
        private_(new ProcessPrivate)
    {
    }

    Process::~Process()
    {
      closeStreams();
      delete private_;
    }

    std::ostream &
    Process::pipeInput(int childFd)
    {
      int fds[2];

      assert(!pipe(fds));
      bio::stream<bio::file_descriptor_sink> * os
        = new bio::stream<bio::file_descriptor_sink>;
      os->open(bio::file_descriptor_sink(fds[1], true));
      ios_[childFd] = os;
      setupFds_.push_back(Fds(fds[1], fds[0], childFd));
      return *os;
    }

    std::istream &
    Process::pipeOutput(int childFd)
    {
      int fds[2];

      assert_msg(!pipe(fds), "%s", strerror(errno));
      bio::stream<bio::file_descriptor_source> * os
        = new bio::stream<bio::file_descriptor_source>;
      os->open(bio::file_descriptor_source(fds[0], true));
      ios_[childFd] = os;
      setupFds_.push_back(Fds(fds[0], fds[1], childFd));
      return *os;
    }

    void
    Process::run()
    {
      private_->pid = fork();
      if (private_->pid != 0)
      {
        BOOST_FOREACH (Fds & fds, setupFds_)
          close(fds.child);
        return;
      }

      BOOST_FOREACH (Fds fds, setupFds_)
      {
        close(fds.parent);
        dup2(fds.child, fds.dest);
      }

      char ** argv = static_cast<char **> (calloc(sizeof (*argv), args_.size() + 2));
      argv[0] = strdup(binary_.string().c_str());
      for (unsigned i = 0; i < args_.size(); ++i)
        argv[i + 1] = strdup(args_[i].c_str());

      execvp(argv[0], argv);
      assert_msg(false, _("execution of `%s' failed: %s"), argv[0], strerror(errno));
    }

    int
    Process::wait()
    {
      int status = 0;

      assert(private_->pid != 0);
      while (true) {
        assert(waitpid(private_->pid, &status, 0) != -1);
        if (WIFSTOPPED(status))
          assert_msg(false, _("stopped is not handled yet"));
        if (WIFSIGNALED(status))
          return 128 + WTERMSIG(status);
        if (WIFEXITED(status))
          return WEXITSTATUS(status);
      }
    }

    void
    Process::terminate()
    {
      assert(private_->pid != 0);
      kill(private_->pid, SIGKILL);
    }
  }
}

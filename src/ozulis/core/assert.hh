#ifndef CORE_ASSERT_H
# define CORE_ASSERT_H

namespace ozulis
{
  namespace core
  {
    /** @brief This function is a trap on which you can put a breakpoint. */
    void assert_trap(void);
    /** @brief print the stack trace, and demangle it */
    void assert_print_stack_trace(void);
  }
}

# ifndef NDEBUG

#  include <stdio.h>
#  include <stdlib.h>

#  ifdef assert
#   undef assert
#  endif

/** @brief run time assertion */
#  define assert(Cond)                                                  \
  do {                                                                  \
    if (!(Cond))                                                        \
    {                                                                   \
      fprintf(stderr,                                                   \
              "\e[1;31m"                                                \
              "======== ASSERT FAILED (\e[0;33m%s\e[1;31m) ========\n"  \
              "function: \e[0;34m%s\e[1;31m\n"                          \
              "location: \e[0;34m%s\e[1;31m:\e[1;37m%d\e[1;31m\n"       \
              "======== BACKTRACE ========\n\e[0;36m",                  \
              #Cond, __PRETTY_FUNCTION__, __FILE__, __LINE__);          \
      ::ozulis::core::assert_print_stack_trace();                       \
      ::ozulis::core::assert_trap();                                    \
      fprintf(stderr, "\e[m");                                          \
      abort();                                                          \
    }                                                                   \
  } while (0)

/** @brief run time assertion with custom message */
#  define assert_msg(Cond, Msg...)                                      \
  do {                                                                  \
    if (!(Cond))                                                        \
    {                                                                   \
      char msg[1024];                                                   \
      snprintf(msg, sizeof (msg), Msg);                                 \
      fprintf(stderr,                                                   \
              "\e[1;31m"                                                \
              "======== ASSERT FAILED (\e[0;33m%s\e[1;31m) ========\n"  \
              "function: \e[0;34m%s\e[1;31m\n"                          \
              "location: \e[0;34m%s\e[1;31m:\e[1;37m%d\e[1;31m\n"       \
              "message:  \e[0;34m%s\e[1;31m\n"                          \
              "======== BACKTRACE ========\n\e[0;36m",                  \
              #Cond, __PRETTY_FUNCTION__, __FILE__, __LINE__, msg);     \
      ::ozulis::core::assert_print_stack_trace();                       \
      ::ozulis::core::assert_trap();                                    \
      fprintf(stderr, "\e[m");                                          \
      abort();                                                          \
    }                                                                   \
  } while (0)

# else /* !NDEBUG */

#  define assert(Cond)
#  define assert_msg(Cond, Msg)

# endif /* !NDEBUG */

#endif /* !CORE_ASSERT_H */

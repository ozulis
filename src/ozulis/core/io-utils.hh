#ifndef CORE_IO_UTILS_HH
# define CORE_IO_UTILS_HH

# include <istream>
# include <string>

namespace ozulis
{
  namespace core
  {
    void readAll(std::istream & is, std::string & out);
  }
}

#endif /* !CORE_IO_UTILS_HH */

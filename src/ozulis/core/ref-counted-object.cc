#include <ozulis/core/assert.hh>

#include "ref-counted-object.hh"

namespace ozulis
{
  namespace core
  {
    RefCountedObject::~RefCountedObject()
    {
      assert_msg(refCount_ == 0,
                 _("refCount_ should be equals to 0 not to %d"),
                 refCount_);
    }

    void
    RefCountedObject::use()
    {
      refCount_++;
    }

    void
    RefCountedObject::release()
    {
      assert(refCount_ > 0);
      --refCount_;
      if (refCount_ == 0)
        delete this;
    }
  }
}

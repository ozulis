#include "string.hh"

namespace ozulis
{
  namespace core
  {
    std::string get_extension(const std::string & filename)
    {
      size_t slash = filename.rfind('/');
      size_t dot = filename.rfind('.');

      /// @todo windows compatibility with '\'
      if (dot > slash + 1)
        return filename.substr(dot + 1);
      return "";
    }

    void replace_extension(std::string & filename, const std::string & extension)
    {
      size_t slash = filename.rfind('/');
      size_t dot = filename.rfind('.');

      /// @todo windows compatibility with '\'
      if (dot > slash + 1)
        filename.erase(dot + !extension.empty());
      filename.append(extension);
    }
  }
}

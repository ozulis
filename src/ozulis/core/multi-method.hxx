namespace ozulis
{
  namespace core
  {
    template<typename Value, typename ...Keys>
    void
    DispatchTable<Value, Keys...>::addEntry(Value value, Keys... keys)
    {
      key_t key(keys...);
      table_[key] = value;
    }

    template<typename Value, typename ...Keys>
    Value &
    DispatchTable<Value, Keys...>::dispatch(Keys... keys)
    {
      key_t key(keys...);
      assert(table_.count(key) > 0);
      return table_[key];
    }
  }
}

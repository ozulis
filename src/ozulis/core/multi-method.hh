#ifndef CORE_MULTI_METHOD_HH
# define CORE_MULTI_METHOD_HH

# include <map>
# include <tr1/tuple>
# include <ozulis/core/assert.hh>

namespace ozulis
{
  namespace core
  {
    template<typename Value, typename ...Keys>
    class DispatchTable
    {
    public:
      void addEntry(Value value, Keys... keys);
      Value& dispatch(Keys... keys);

    protected:
      typedef std::tr1::tuple<Keys...> key_t;
      std::map<key_t, Value> table_;
    };
  }
}

# include "multi-method.hxx"

#endif /* !CORE_MULTI_METHOD_HH */

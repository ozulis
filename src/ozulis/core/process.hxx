#include <ozulis/core/assert.hh>

namespace ozulis
{
  namespace core
  {
    inline void
    Process::setBinary(const std::string & path)
    {
      binary_ = path;
    }

    inline const boost::filesystem::path &
    Process::binary() const
    {
      return binary_;
    }

    inline void
    Process::addArg(const std::string & arg)
    {
      args_.push_back(arg);
    }

    inline const Process::args_t &
    Process::args() const
    {
      return args_;
    }

    inline std::ostream &
    Process::pipeStdin()
    {
      return pipeInput(STDIN_FILENO);
    }

    inline std::istream &
    Process::pipeStdout()
    {
      return pipeOutput(STDOUT_FILENO);
    }

    inline std::istream &
    Process::pipeStderr()
    {
      return pipeOutput(STDERR_FILENO);
    }
  }
}

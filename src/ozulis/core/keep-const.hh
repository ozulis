#ifndef KEEP_CONST_HH
# define KEEP_CONST_HH

namespace ozulis
{
  namespace core
  {
    template <typename T, typename U>
    struct KeepConst
    {
      typedef U type;
    };

    template <typename T, typename U>
    struct KeepConst<const T, U>
    {
      typedef const U type;
    };
  }
}

#endif /* !KEEP_CONST_HH */

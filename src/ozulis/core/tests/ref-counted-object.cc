#include <vector>

#include <ozulis/ast/ast.hh>
#include <ozulis/core/ref-counted-object.hh>

class Obj : public ozulis::core::RefCountedObject
{
};

typedef ozulis::core::RefCountedObject::Ptr<Obj> Ptr;

void test1()
{
  Ptr p = new Obj;
  Ptr p0 = p.ptr();
  Ptr p2(p);
  Ptr p1 = p;
}

int main()
{
  test1();

  return 0;
}

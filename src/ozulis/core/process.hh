#ifndef PROCESS_HH
# define PROCESS_HH

# include <iostream>
# include <vector>
# include <map>
# include <boost/filesystem/path.hpp>

namespace ozulis
{
  namespace core
  {
    class ProcessPrivate;

    class Process
    {
    public:
      Process();
      ~Process();

      typedef std::vector<std::string> args_t;

      inline const boost::filesystem::path & binary() const;
      inline void setBinary(const std::string & path);
      inline const args_t & args() const;
      inline void addArg(const std::string & arg);
      std::string commandLine() const;

      void run();
      int wait();
      void terminate();

      inline std::ostream & pipeStdin();
      inline std::istream & pipeStdout();
      inline std::istream & pipeStderr();

      std::ostream & pipeInput(int childFd);
      std::istream & pipeOutput(int childFd);
      std::iostream & socket(int childFd);

      void closeStreams();

    protected:
      struct Fds
      {
        Fds(int parent_, int child_, int dest_)
          : parent(parent_), child(child_), dest(dest_) {}
        int parent;
        int child;
        int dest;
      };

      typedef std::map<int /*childFd*/, std::ios *> ios_t;
      typedef std::vector<Fds>                      setupFds_t;

      boost::filesystem::path binary_;
      args_t                  args_;
      ios_t                   ios_;
      setupFds_t              setupFds_; // 0: us, 1: child
      ProcessPrivate *        private_;
    };
  }
}

# include "process.hxx"

#endif /* !PROCESS_HH */

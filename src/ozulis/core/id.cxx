#include <ozulis/core/assert.hh>

#include "id.hh"

namespace ozulis
{
  namespace core
  {
    template <typename H>
    IdBase<H>::IdBase()
      : Singleton<IdBase<H> > (),
        childs_(),
        parents_(),
        nextId_(1)
    {
    }

    template <typename H>
    id_t
    IdBase<H>::nextId()
    {
      return IdBase<H>::instance().nextId_++;
    }

    template <typename H>
    void
    IdBase<H>::setParent(id_t parent, id_t child)
    {
      if (parent == child)
        return;
      assert(parent < child);

      IdBase<H> & thiz = IdBase<H>::instance();
      if (thiz.parents_.capacity() < (unsigned)child)
        thiz.parents_.resize((child + 128) & ~0x7f);

      thiz.parents_[child] = parent;
      thiz.childs_[parent].insert(child);
    }

    template <typename H>
    id_t
    IdBase<H>::parent(id_t child)
    {
      IdBase<H> & thiz = IdBase<H>::instance();

      return thiz.parents_[child];
    }

    template <typename H>
    bool
    IdBase<H>::check(id_t parent, id_t child)
    {
      IdBase<H> & thiz = IdBase<H>::instance();

      assert(parent < child);
      id_t currentParent = thiz.parents_[child];
      while (currentParent > parent)
      {
        child = currentParent;
        currentParent = thiz.parents_[child];
      }
      return currentParent == parent;
    }

    template <typename H, typename T>
    id_t
    Id<H, T>::id()
    {
      static id_t id_ = IdBase<H>::nextId();
      return id_;
    }

    template <typename H, typename P, typename T>
    id_t
    IdP<H, P, T>::id()
    {
      static id_t id_ = -1;

      if (id_ != -1)
        return id_;

      id_t parent = Id<H, P>::id();
      id_ = Id<H, T>::id();

      IdBase<H>::setParent(parent, id_);
      return id_;
    }
  }
}

#include <ozulis/core/assert.hh>

namespace ozulis
{
  namespace core
  {
    template <class T, class Node, class GetId, typename ...Args>
    inline void
    IdVisitor<T, Node, GetId, Args...>::visit(Node & node, Args... args)
    {
      assert(&node);
      assert(Base::instance().methods[GetId::id(node)]);
      Base::instance().methods[GetId::id(node)](node, args...);
    }

    template <class T, class Node, class GetId, typename ...Args>
    void
    IdVisitor<T, Node, GetId, Args...>::
    registerMethod(id_t id, IdVisitor<T, Node, GetId, Args...>::method_f method)
    {
      assert(id >= 0);
      assert(method);
      if (Base::instance().methods.capacity() <= (unsigned)id)
        Base::instance().methods.resize(id + 42, 0);
      Base::instance().methods[id] = method;
    }

    template <class T, class Node, class GetId, typename ...Args>
    void
    IdVisitor<T, Node, GetId, Args...>::completeWithParents()
    {
      Base & base = Base::instance();

      for (unsigned i = 0; i < GetId::limit(); i++)
        if (!base.methods[i])
          base.methods[i] = base.methods[GetId::parent(i)];
    }
  }
}

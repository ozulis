#include <execinfo.h>
#include <string.h>
#include <cxxabi.h>

#include <ozulis/core/assert.hh>

namespace ozulis
{
  namespace core
  {
    void assert_print_stack_trace(void)
    {
      void * addrlist[64];
      int    addrlen;

      // get the address
      addrlen = backtrace(addrlist, sizeof (addrlist) / sizeof (addrlist[0]));

      // resolve addresses into strings containing "filename(function+address)",
      // this array must be free()-ed
      char ** symbollist = backtrace_symbols(addrlist, addrlen);

      // allocate string which will be filled with the demangled function name
      size_t funcnamesize = 1024;
      char * funcname = (char*)malloc(funcnamesize);

      // iterate over the returned symbol lines. skip the first, it is the
      // address of this function.
      for (int i = 1; i < addrlen; i++)
      {
        char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

        // find parentheses and +address offset surrounding the mangled name:
        // ./module(function+0x15c) [0x8048a6d]
        for (char *p = symbollist[i]; *p; ++p)
        {
          if (*p == '(')
            begin_name = p;
          else if (*p == '+')
            begin_offset = p;
          else if (*p == ')' && begin_offset) {
            end_offset = p;
            break;
          }
        }

        if (begin_name && begin_offset && end_offset
            && begin_name < begin_offset)
        {
          *begin_name++ = '\0';
          *begin_offset++ = '\0';
          *end_offset = '\0';

          // mangled name is now in [begin_name, begin_offset) and caller
          // offset in [begin_offset, end_offset). now apply
          // __cxa_demangle():

          int status;
          char* ret = abi::__cxa_demangle(begin_name,
                                          funcname, &funcnamesize, &status);
          if (status == 0) {
            funcname = ret; // use possibly realloc()-ed string
            fprintf(stderr, "%s: %s+%s\n",
                    symbollist[i], funcname, begin_offset);
          }
          else {
            // demangling failed. Output function name as a C function with
            // no arguments.
            fprintf(stderr, "%s: %s()+%s\n",
                    symbollist[i], begin_name, begin_offset);
          }
        }
        else
        {
          // couldn't parse the line? print the whole line.
          fprintf(stderr, "%s\n", symbollist[i]);
        }
      }

      free(funcname);
      free(symbollist);
    }

    void assert_trap(void)
    {
    }
  }
}

#include <boost/foreach.hpp>
#include <sstream>

#include "process.hh"

namespace ozulis
{
  namespace core
  {
    void
    Process::closeStreams()
    {
      typedef std::pair<int, std::ios *> it_t;

      BOOST_FOREACH (it_t it, ios_)
        delete it.second;
      ios_.clear();
    }

    std::string
    Process::commandLine() const
    {
      std::stringstream ss;

      ss << "\"" << binary_ << "\"";
      BOOST_FOREACH (const std::string & arg, args_)
        ss << " \"" << arg << "\"";
      return ss.str();
    }
  }
}

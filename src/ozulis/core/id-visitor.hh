#ifndef CORE_ID_VISITOR_HH
# define CORE_ID_VISITOR_HH

# include <vector>

# include <ozulis/core/singleton.hh>
# include <ozulis/core/id.hh>

namespace ozulis
{
  namespace core
  {
    template <class T, class Node, class GetId, typename ...Args>
    class IdVisitor
    {
    public:
      typedef void (*method_f)(Node & node, Args... args);
      typedef IdVisitor<T, Node, GetId, Args...> IdVisitor_t;

      static void visit(Node & node, Args...);
      static void registerMethod(id_t id, method_f method);
      static void completeWithParents();

      template <class V>
      static void completeWith()
      {
        Base & base = Base::instance();
        typename V::Base & vbase = V::Base::instance();

        for (unsigned i = 0; i < vbase.methods.size(); i++)
          if (!base.methods[i] && vbase.methods[i])
            base.methods[i] = vbase.methods[i];
      }

      class Base : public Singleton<Base>
      {
      public:
        Base() : Singleton<Base>(), methods() { methods.resize(256, 0); }
        inline void init() { T::initBase(); }

        typedef std::vector<method_f> methods_t;
        methods_t methods;
      };

    public:
      inline static void initBase() {}
    };
  }
}

# include "id-visitor.hxx"

#endif /* !CORE_ID_VISITOR_HH */

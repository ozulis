#ifndef TASK_HH
# define TASK_HH

# include <vector>

namespace ozulis
{
  class Compiler;

  class Task
  {
  public:
    virtual ~Task();
    virtual void accept(Compiler & compiler) = 0;
    virtual std::vector<Task *> * dependancies() = 0;
  };
}

extern template class std::vector<ozulis::Task *>;

#endif /* !TASK_HH */

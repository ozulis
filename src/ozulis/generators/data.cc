#include "generator.hh"

namespace ag = ozulis::ast::generator;

const ag::Node ag::nodes[] = {
  {"Node", "core::RefCountedObject", "The root node", {
      {"core::id_t", "nodeType"}
    }
  },

  {"File", "Node", "Represent a file", {
      {"Scope *", "scope"},
      {"boost::filesystem::path", "path"},
      {"std::vector<VarDeclPtr> *", "varDecls"},
      {"std::vector<NodePtr> *", "decls"},
    }
  },

  {"Symbol", "Node", "Represent a symbol", {
      {"std::string", "name"},
      {"TypePtr", "type"},
      {"AddressPtr", "address"}
    }
  },

  {"Address", "Node", "Represent an address", {
      {"", ""}
    }
  },

  {"RegisterAddress", "Address", "Represent a register address", {
      {"std::string", "address"},
      {"", ""}
    }
  },

  {"MemoryAddress", "Address", "Represent a memory address", {
      {"std::string", "address"},
      {"", ""}
    }
  },

  /* Types */
  {"Type", "Node", "Represent a type", {
      {"bool", "isConst"}
    }
  },

  {"NamedType", "Type", "The name of a type that will be resolved later", {
      {"bool", "isConst"},
      {"std::string", "name"}
    }
  },

  {"VoidType", "Type", "Represent the void type", {
      {"", ""}
    }
  },

  {"NumberType", "Type", "Represent an abstract number", {
      {"", ""}
    }
  },

  {"BoolType", "NumberType", "Represent a boolean value", {
      {"", ""}
    }
  },

  {"IntegerType", "NumberType", "Represent an integer", {
      {"bool", "isSigned"},
      {"int", "size"}
    }
  },

  {"FloatType", "NumberType", "Represent a float", {
      {"", ""}
    }
  },

  {"DoubleType", "NumberType", "Represent a float", {
      {"", ""}
    }
  },

  {"UnaryType", "Type", "A type which contains another one", {
      {"TypePtr", "type"},
      {"", ""}
    }
  },

  {"PointerType", "UnaryType", "Represent a pointer", {
      {"", ""}
    }
  },

  {"ReferenceType", "UnaryType", "Represent a reference", {
      {"", ""}
    }
  },

  {"TypedefType", "UnaryType", "Represent a typedef type", {
      {"", ""}
    }
  },

  {"EnumType", "UnaryType", "Represent an enum", {
      {"unsigned", "count"},
      /* FIXME: elements */
      {"", ""}
    }
  },

  {"ArrayType", "UnaryType", "Represent an array", {
      {"uint16_t", "size"},
      {"", ""}
    }
  },

  {"FunctionType", "UnaryType", "Represent a function", {
      {"std::vector<TypePtr> *", "argsType"},
      {"", ""}
    }
  },

  {"VarDecl", "Node", "Represent a variable declaration", {
      {"TypePtr", "type"},
      {"std::string", "name"},
      {"", ""}
    }
  },

  {"ValuedVarDecl", "VarDecl", "Represent a variable declaration with an initial value", {
      {"ExpPtr", "value"},
      {"", ""}
    }
  },

  /* Expressions */
  {"Exp", "Node", "Represent an abstract expression", {
      {"TypePtr", "type"}
    }
  },

  {"VoidExp", "Exp", "Represent a void expression", {
      {"", ""}
    }
  },


  {"AssignExp", "Exp", "Represent an assignment expression", {
      {"ExpPtr", "dest"},
      {"ExpPtr", "value"},
    }
  },

  {"UnaryExp", "Exp", "Base class for unary expression", {
      {"ExpPtr", "exp"},
    }
  },

  {"NegExp", "UnaryExp", "Unary expression `-'", {
      {"", ""}
    }
  },

  {"BangExp", "UnaryExp", "Unary expression `!'", {
      {"", ""}
    }
  },

  {"NotExp", "UnaryExp", "Unary expression `~'", {
      {"", ""}
    }
  },

  {"CastExp", "UnaryExp", "A cast from exp->type to type", {
      {"", ""}
    }
  },

  {"CastBoolToIntegerExp", "CastExp", "A cast from bool to integer", {
      {"", ""}
    }
  },

  {"CastBoolToFloatExp", "CastExp", "A cast from bool to float", {
      {"", ""}
    }
  },

  {"CastBoolToDoubleExp", "CastExp", "A cast from bool to double", {
      {"", ""}
    }
  },

  {"CastIntegerToBoolExp", "CastExp", "A cast from integer to bool", {
      {"", ""}
    }
  },

  {"CastIntegerToIntegerExp", "CastExp", "A cast from integer to integer", {
      {"", ""}
    }
  },

  {"CastIntegerToFloatExp", "CastExp", "A cast from integer to float", {
      {"", ""}
    }
  },

  {"CastIntegerToDoubleExp", "CastExp", "A cast from integer to double", {
      {"", ""}
    }
  },

  {"CastIntegerToPointerExp", "CastExp", "A cast from integer to pointer", {
      {"", ""}
    }
  },

  {"CastFloatToIntegerExp", "CastExp", "A cast from float to integer", {
      {"", ""}
    }
  },

  {"CastFloatToDoubleExp", "CastExp", "A cast from float to double", {
      {"", ""}
    }
  },

  {"CastDoubleToIntegerExp", "CastExp", "A cast from double to integer", {
      {"", ""}
    }
  },

  {"CastDoubleToFloatExp", "CastExp", "A cast from double to float", {
      {"", ""}
    }
  },

  {"CastPointerToIntegerExp", "CastExp", "A cast from pointer to integer", {
      {"", ""}
    }
  },

  {"CastPointerToPointerExp", "CastExp", "A cast from pointer to pointer", {
      {"", ""}
    }
  },

  {"AtExp", "UnaryExp", "Get the address of an expression", {
      {"", ""}
    }
  },

  {"DereferenceExp", "UnaryExp", "Derefence a pointer", {
      {"", ""}
    }
  },

  {"DereferenceByIndexExp", "UnaryExp", "Derefence a pointer", {
      {"ExpPtr", "index"}
    }
  },

  {"BinaryExp", "Exp", "Base class for binary expression", {
      {"ExpPtr", "left"},
      {"ExpPtr", "right"},
    }
  },

  {"EqExp", "BinaryExp", "Expression `=='", {
      {"", ""}
    }
  },

  {"NeqExp", "BinaryExp", "Expression `!='", {
      {"", ""}
    }
  },

  {"LtExp", "BinaryExp", "Expression `<'", {
      {"", ""}
    }
  },

  {"LtEqExp", "BinaryExp", "Expression `<='", {
      {"", ""}
    }
  },

  {"GtExp", "BinaryExp", "Expression `>'", {
      {"", ""}
    }
  },

  {"GtEqExp", "BinaryExp", "Expression `>='", {
      {"", ""}
    }
  },

  {"AddExp", "BinaryExp", "Expression `+'", {
      {"", ""}
    }
  },

  {"SubExp", "BinaryExp", "Expression `-'", {
      {"", ""}
    }
  },

  {"MulExp", "BinaryExp", "Expression `*'", {
      {"", ""}
    }
  },

  {"DivExp", "BinaryExp", "Expression `/'", {
      {"", ""}
    }
  },

  {"ModExp", "BinaryExp", "Expression `%'", {
      {"", ""}
    }
  },

  {"AndExp", "BinaryExp", "Expression `&'", {
      {"", ""}
    }
  },

  {"OrExp", "BinaryExp", "Expression `|'", {
      {"", ""}
    }
  },

  {"XorExp", "BinaryExp", "Expression `^'", {
      {"", ""}
    }
  },

  {"AndAndExp", "BinaryExp", "Expression `&&'", {
      {"", ""}
    }
  },

  {"OrOrExp", "BinaryExp", "Expression `||'", {
      {"", ""}
    }
  },

  {"ShlExp", "BinaryExp", "Expression `>>'", {
      {"", ""}
    }
  },

  {"AShrExp", "BinaryExp", "Expression `<<'", {
      {"", ""}
    }
  },

  {"LShrExp", "BinaryExp", "Expression `<<<'", {
      {"", ""}
    }
  },

  {"PointerArithExp", "Exp", "ptr + integer", {
      {"ExpPtr", "pointer"},
      {"ExpPtr", "offset"}
    }
  },

  {"NumberExp", "Exp", "A number", {
      {"double", "number"}
    }
  },

  {"IdExp", "Exp", "An identifier", {
      {"std::string", "id"}
    }
  },

  {"SymbolExp", "Exp", "A reference to a symbol", {
      {"SymbolPtr", "symbol"}
    }
  },

  {"StringExp", "Exp", "A string litteral", {
      {"std::string", "string"}
    }
  },

  {"CallExp", "Exp", "A call to a function", {
      {"ExpPtr", "function"},
      {"std::vector<ExpPtr> *", "args"},
    }
  },

  /* Statements */
  {"Block", "Node", "Represent a block with a new scope", {
      {"Scope *", "scope"},
      {"std::vector<VarDeclPtr> *", "varDecls"},
      {"std::vector<NodePtr> *", "statements"},
      {"", ""}
    }
  },

  {"EmptyStatement", "Node", "An empty statement", {
      {"", ""}
    }
  },

  {"Label", "Node", "A label", {
      {"std::string", "name"}
    }
  },

  {"Goto", "Node", "goto statement", {
      {"std::string", "label"}
    }
  },

  {"Return", "Node", "return statement", {
      {"ExpPtr", "exp"},
    }
  },

  {"ConditionalBranch", "Node", "Factorisation code for loops", {
      {"ExpPtr", "cond"},
      {"LabelPtr", "trueLabel"},
      {"LabelPtr", "falseLabel"},
    },
  },

  {"If", "Node", "If statement", {
      {"ConditionalBranchPtr", "branch"},
      {"NodePtr", "trueBlock"},
      {"NodePtr", "falseBlock"},
      {"LabelPtr", "endLabel"}
    },
  },

  {"While", "Node", "While statement", {
      {"ConditionalBranchPtr", "branch"},
      {"NodePtr", "block"},
      {"LabelPtr", "beginLabel"}
    },
  },

  {"DoWhile", "Node", "Do while statement", {
      {"ConditionalBranchPtr", "branch"},
      {"NodePtr", "block"},
    },
  },

  /* Declarations */
  {"Alias", "Node", "Represent an alias declaration", {
      {"std::string", "name"},
      {"TypePtr", "type"},
      {"", ""}
    }
  },

  {"Typedef", "Node", "Represent a typedef declaration", {
      {"std::string", "name"},
      {"TypePtr", "type"},
      {"", ""}
    }
  },

  /* Functions */
  {"FunctionDecl", "Node", "Represent a function implementation", {
      {"std::string", "name"},
      {"TypePtr", "returnType"},
      {"std::vector<VarDeclPtr> *", "args"},
      {"", ""}
    }
  },

  {"Function", "FunctionDecl", "Represent a function implementation", {
      {"Scope *", "scope"},
      {"BlockPtr", "block"},
      {"", ""}
    }
  },

  /* 3 address decomposition */
  {"Alloca", "Node", "Alloc a variable on the stack", {
      {"SymbolPtr", "symbol"}
    }
  },

  {"LoadVar", "Node", "A temporary result, used for 3 address", {
      {"ExpPtr", "from"},
      {"std::string", "to"}
    }
  },

  {"StoreVar", "Node", "A temporary result, used for 3 address", {
      {"SymbolPtr", "to"},
      {"ExpPtr", "value"}, // must be IdExp or NumberExp
    }
  },

  {"", "", "", {{"", ""}}}
};

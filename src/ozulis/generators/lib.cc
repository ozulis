#include <iostream>

#include <ozulis/core/assert.hh>
#include "generator.hh"

namespace ag = ozulis::ast::generator;

bool ag::isNode(const std::string & type)
{
  return
    type == "AddressPtr" ||
    type == "BlockPtr" ||
    type == "ConditionalBranchPtr" ||
    type == "ExpPtr" ||
    type == "FunctionTypePtr" ||
    type == "LabelPtr" ||
    type == "NodePtr" ||
    type == "SymbolPtr" ||
    type == "TypePtr" ||
    type == "VarDeclPtr" ||
    type == "ValuedVarDeclPtr" ||
    false;
}

bool ag::isPrintable(const std::string & type)
{
  return
    type != "Scope *";
}

bool ag::isVectorOfNode(const std::string & type)
{
  return
    type == "std::vector<NodePtr> *" ||
    type == "std::vector<ExpPtr> *" ||
    type == "std::vector<TypePtr> *" ||
    type == "std::vector<VarDeclPtr> *" ||
    false;
}

bool ag::needsDelete(const std::string & type)
{
  return
    type == "std::vector<NodePtr> *" ||
    type == "std::vector<ExpPtr> *" ||
    type == "std::vector<TypePtr> *" ||
    type == "std::vector<VarDeclPtr> *" ||
    type == "Scope *" ||
    false;
}

const ag::Node *
ag::findNodeByName(const std::string & name)
{
  for (const ag::Node * node = ag::nodes; !node->name.empty() > 0; node++)
    if (node->name == name)
      return node;
  return 0;
}

std::vector<const ag::Attribute *>
ag::allAttributes(const ag::Node * node)
{
  std::vector<const ag::Attribute *> attrs;

  assert(node);
  do {
    for (const ag::Attribute * attr = node->attrs; !attr->type.empty(); ++attr)
      attrs.push_back(attr);
  } while ((node = findNodeByName(node->parent)));
  return attrs;
}

void ag::appendGeneratedWarning(std::ostream & out)
{
  out << "/* DO NOT MODIFY THIS FILE (GENERATED) */" << std::endl;
}

std::string
ag::deref(std::string type)
{
  std::string::iterator it = type.end() - 1;

  while (it != type.begin() && (*it == '*' || *it == ' '))
    --it;
  type.erase(it + 1, type.end());
  return type;
}

template class std::vector<ag::Attribute *>;

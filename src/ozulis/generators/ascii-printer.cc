#include <ozulis/core/assert.hh>
#include <iostream>
#include <fstream>
#include <boost/foreach.hpp>

#include "generator.hh"

namespace ag = ozulis::ast::generator;

static void
generateAsciiPrinterHeader(const char * working_directory)
{
  assert(working_directory);

  std::fstream out;
  std::string path(working_directory);

  path.append("/ascii-printer.hh");
  out.open(path.c_str(), std::fstream::out | std::fstream::trunc);
  assert(out.is_open() && out.good());

  ag::appendGeneratedWarning(out);
  out << "#ifndef VISITORS_ASCII_PRINTER_HH" NL
      << "# define VISITORS_ASCII_PRINTER_HH" NL NL
      << "#include <ozulis/visitors/visitor.hh>" NL NL
      << "namespace ozulis" NL
      << "{" NL
      << "namespace visitors" NL
      << "{" NL;

  out << "  /** @defgroup Printers */" NL
      << "  /**" NL
      << "   * @brief A printer which dump the AST on the standard output." NL
      << "   * @ingroup Visitors" NL
      << "   */" NL
      << "  class AsciiPrinter : public ConstVisitor<AsciiPrinter>" NL
      << "  {" NL
      << "  public:" NL
      << "    AsciiPrinter();" NL
      << "    virtual ~AsciiPrinter();" NL NL
      << "    static void initBase();" NL NL
      << "    int depth; ///< The depht in the ast." NL
      << "    std::string name; ///< The name of the node." NL
      << "    std::string prefix; ///< The name of the node." NL
      << "  };" NL NL;

  out << "} // namespace visitors" NL
      << "} // namespace ozulis" NL NL
      << "extern template class ozulis::visitors::Visitor<"
      << "ozulis::visitors::AsciiPrinter>;" NL NL
      << "#endif // !VISITORS_ASCII_PRINTER_HH" NL;
  out.close();
}

#define cclear "\e[m"
#define ctype "\e[34m"
#define cchev "\e[33m"
#define cbrack "\e[35m"

static void
generateAsciiPrinterSource(const char * working_directory)
{
  assert(working_directory);

  std::fstream out;
  std::string path(working_directory);

  path.append("/ascii-printer.cc");
  out.open(path.c_str(), std::fstream::out | std::fstream::trunc);
  assert(out.is_open() && out.good());

  const ag::Node *node;

  ag::appendGeneratedWarning(out);
  out << "#include <iostream>" NL
      << "#include <sstream>" NL
      << "#include <boost/foreach.hpp>" NL
      << "#include <ozulis/visitors/ascii-printer.hh>" NL NL
      << "#define NL << std::endl" NL NL
      << "template class ozulis::visitors::Visitor<"
      << "ozulis::visitors::AsciiPrinter>;" NL NL
      << "namespace ozulis" NL
      << "{" NL
      << "namespace visitors" NL
      << "{" NL;

  out << "  AsciiPrinter::AsciiPrinter()" NL
      << "   : ConstVisitor_t(), depth(-1), name(), prefix()"
      << "{}" NL NL
      << "  AsciiPrinter::~AsciiPrinter() {}" NL NL;

  for (node = ag::nodes; !node->name.empty() > 0; node++)
  {
    out << "  static void visit" << node->name << "(const ast::Node & node_, AsciiPrinter & ctx)" NL
        << "  {" NL
        << "    const ast::" << node->name << " & node = reinterpret_cast<const ast::"
        << node->name << " &> (node_);" NL
        << "    std::string oldName = ctx.name;" NL
        << "    std::string oldPrefix = ctx.prefix;" NL NL
        << "    ++ctx.depth;" NL
        << "    std::cout << ctx.prefix << \"`- \";" NL
        << "    if (!ctx.name.empty())" NL
        << "      std::cout << ctx.name << \" \";" NL
        << "    std::cout << \""cchev"<"ctype << node->name << cchev">\e[m\" NL;" NL;

    unsigned int i = 0;
    std::vector<const ag::Attribute *> allAttrs = ag::allAttributes(node);
    BOOST_FOREACH(const ag::Attribute * attr, allAttrs)
    {
      ++i;
      out << "    ctx.name = \"" << attr->name << "\";" NL;
      if (ag::isNode(attr->type))
        out << "    if (node." << attr->name << ")" NL
            << "    {" NL
            << "      ctx.prefix = oldPrefix;" NL
            << "      ctx.prefix.append(\"   "
            << (i < allAttrs.size() ? "|" : " ") << "\");" NL
            << "      AsciiPrinter::visit(*node." << attr->name << ", ctx);" NL
            << "    }" NL;
      else if (ag::isVectorOfNode(attr->type))
      {
        out << "    if (node." << attr->name << ")" NL
            << "    {" NL
            << "      int i = 0;" NL NL
            << "      BOOST_FOREACH(ast::Node * n, (*node." << attr->name << "))" NL
            << "      {" NL
            << "        std::stringstream ss;" NL
            << "        ss << \""cbrack"["cclear"\" << i << \""cbrack"]"cclear"\";" NL
            << "        ctx.name = \"" << attr->name << "\";" NL
            << "        ctx.name.append(ss.str());" NL
            << "        ctx.prefix = oldPrefix;" NL;

        if (i < allAttrs.size())
          out << "        ctx.prefix.append(\"   |\");" NL;
        else
          out << "        if (i == node." << attr->name << "->size() - 1)"
              << "          ctx.prefix.append(\"    \");" NL
              << "        else" NL
              << "          ctx.prefix.append(\"   |\");" NL;
        out << "        AsciiPrinter::visit(*n, ctx);" NL
            << "        i++;" NL
            << "      }" NL
            << "    }" NL;
      }
      else if (ag::isPrintable(attr->type))
        out << "    std::cout << oldPrefix << \"   "
            << (i < allAttrs.size() ? "|" : " ") <<  "\";" NL
            << "    std::cout << \"`- " << attr->name << cchev" <" ctype << attr->type
            << cchev">\e[m = \" << node." << attr->name << " NL;" NL;
    }
    out << "    --ctx.depth;" NL
        << "    ctx.name = oldName;" NL;

    out << "  }" NL NL;
  }

  /* Creating initBase() */
  out << "  void AsciiPrinter::initBase()" NL
      << "  {" NL;
  for (node = ag::nodes; !node->name.empty() > 0; node++)
    out << "    registerMethod(ast::" << node->name << "::nodeTypeId(), "
        << "visit" << node->name << ");" NL;
  out << "  }" NL NL;


  out << "} // namespace visitors" NL
      << "} // namespace ozulis" NL;
  out.close();
}

int main(int argc, char **argv)
{
  assert(argc == 2);
  generateAsciiPrinterHeader(argv[1]);
  generateAsciiPrinterSource(argv[1]);
  return 0;
}

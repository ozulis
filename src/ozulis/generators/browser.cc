#include <iostream>
#include <fstream>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include "generator.hh"

namespace ag = ozulis::ast::generator;

static void
generateBrowseVisitorHeader(const char * working_directory)
{
  assert(working_directory);

  std::fstream out;
  std::string path(working_directory);

  path.append("/browser.hh");
  out.open(path.c_str(), std::fstream::out | std::fstream::trunc);
  assert(out.is_open() && out.good());

  ag::appendGeneratedWarning(out);
  out << "#ifndef VISITORS_BROWSER_HH" NL
      << "# define VISITORS_BROWSER_HH" NL NL
      << "#include <ozulis/visitors/visitor.hh>" NL NL
      << "namespace ozulis" NL
      << "{" NL
      << "namespace visitors" NL
      << "{" NL NL;

  out << "  /** @defgroup Visitors */" NL
      << "  /**" NL
      << "   * @brief A visitor which browse the AST from the top to the bottom." NL
      << "   * @ingroup Visitors" NL
      << "   */" NL
      << "  template <class V, class Node, typename ...Args>" NL
      << "  class BrowserBase : public core::IdVisitor<BrowserBase<V, Node, Args...>, Node, GetNodeId, V &, Args...>" NL
      << "  {" NL
      << "  public:" NL
      << "    static void initBase();" NL
      << "  };" NL NL

      << "  template <class V, typename ...Args>" NL
      << "  class Browser : public BrowserBase<V, ast::Node, Args...>" NL
      << "  {" NL
      << "  };" NL NL

      << "  template <class V, typename ...Args>" NL
      << "  class ConstBrowser : public BrowserBase<V, const ast::Node, Args...>" NL
      << "  {" NL
      << "  };" NL NL;

  out << "} // namespace ast" NL
      << "} // namespace ozulis" NL NL
      << "# include \"browser.hxx\"" NL NL
      << "#endif // !VISITORS_BROWSER_HH" NL;
  out.close();
}

static void
generateBrowseVisitorSource(const char * working_directory)
{
  assert(working_directory);

  std::fstream out;
  std::string path(working_directory);

  path.append("/browser.hxx");
  out.open(path.c_str(), std::fstream::out | std::fstream::trunc);
  assert(out.is_open() && out.good());

  const ag::Node *node;

  ag::appendGeneratedWarning(out);
  out << "#include <boost/foreach.hpp>" NL
      << "#include <ozulis/core/keep-const.hh>" NL
      << "#include <ozulis/core/assert.hh>" NL
      << "#include <ozulis/ast/ast.hh>" NL NL
      << "#include \"browser.hh\"" NL NL
      << "#define NL << std::endl" NL NL
      << "namespace ozulis" NL
      << "{" NL
      << "namespace visitors" NL
      << "{" NL;

  for (node = ag::nodes; !node->name.empty() > 0; node++)
  {
    out << "  template <class V, class Node, typename...Args>" NL
        << "  static void browse" << node->name << "(" NL
        << "             Node &  node_ __attribute__((unused))," NL
        << "             V &     v     __attribute__((unused)),"NL
        << "             Args... args  __attribute__((unused)))" NL
        << "  {" NL
        << "    typedef typename core::KeepConst<Node, ast::" << node->name << ">::type type_t;" NL
        << "    type_t & node __attribute__((unused)) = reinterpret_cast<type_t &> (node_);"NL;

    BOOST_FOREACH(const ag::Attribute * attr, allAttributes(node))
    {
      if (ag::isNode(attr->type))
        out << "    if (node." << attr->name << ")" NL
            << "    {" NL
            << "      v.parentRef = (ast::Node const **) (&node."
            << attr->name << ");" NL
            << "      V::visit(*node." << attr->name << ", v, args...);" NL
            << "    }" NL;
      else if (ag::isVectorOfNode(attr->type))
        out << "    if (node." << attr->name << ")" NL
            << "    {" NL
            << "      for (typename type_t::" << attr->name << "_t::iterator it = node."
            << attr->name << "->begin();" NL
            << "           it != node." << attr->name << "->end();" NL
            << "           ++it)" NL
            << "      {" NL
            << "        assert(*it);" NL
            << "        v.parentRef = (ast::Node const **) (&*it);" NL
            << "        V::visit(**it, v, args...);" NL
            << "      }" NL
            << "    }" NL;
    }

    out << "  }" NL NL;
  }

  out << "  template <class V, class Node, typename...Args>" NL
      << "  void" NL
      << "  BrowserBase<V, Node, Args...>::initBase()" NL
      << "  {" NL;
  for (node = ag::nodes; !node->name.empty() > 0; node++)
    out << "    registerMethod(ast::" << node->name << "::nodeTypeId(), "
        << "browse" << node->name << "<V, Node, Args...>);" NL;
  out << "  }" NL NL;

  out << "} // namespace visitors" NL
      << "} // namespace ozulis" NL;
  out.close();
}

int main(int argc, char **argv)
{
  assert(argc == 2);
  generateBrowseVisitorHeader(argv[1]);
  generateBrowseVisitorSource(argv[1]);
  return 0;
}

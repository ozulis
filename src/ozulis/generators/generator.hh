#ifndef GENERATOR_HH
# define GENERATOR_HH

# include <string>
# include <vector>

namespace ozulis
{
  namespace ast
  {
    namespace generator
    {
      struct Attribute
      {
        const std::string type;
        const std::string name;
      };

      struct Node
      {
        const std::string name;
        const std::string parent;
        const std::string description;
        const Attribute attrs[100];
      };

      extern const Node nodes[];

      bool isNode(const std::string & type);
      bool isVectorOfNode(const std::string & type);
      bool isPrintable(const std::string & type);
      bool needsDelete(const std::string & type);
      const Node * findNodeByName(const std::string & name);
      std::vector<const Attribute *> allAttributes(const Node * node);
      void appendGeneratedWarning(std::ostream & out);
      std::string deref(std::string type);
    }
  }
}

# define NL << std::endl

extern template class std::vector<ozulis::ast::generator::Attribute *>;

#endif /* !GENERATOR_HH */

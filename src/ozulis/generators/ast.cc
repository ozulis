#include "generator.hh"
#include <ozulis/core/assert.hh>
#include <iostream>
#include <fstream>

namespace ag = ozulis::ast::generator;

void
generateAstHeader(const char * working_directory)
{
  assert(working_directory);

  std::fstream out;
  std::string path(working_directory);

  path.append("/ast.hh");
  out.open(path.c_str(), std::fstream::out | std::fstream::trunc);
  assert(out.is_open() && out.good());

  const ag::Node * node;
  const ag::Attribute * attr;

  ag::appendGeneratedWarning(out);
  out << "#ifndef AST_AST_HH" NL
      << "# define AST_AST_HH" NL NL
      << "# include <vector>" NL
      << "# include <string>" NL NL
      << "# include <boost/filesystem/path.hpp>" NL NL
      << "# include <ozulis/core/ref-counted-object.hh>" NL
      << "# include <ozulis/core/id.hh>" NL NL
      << "namespace ozulis" NL
      << "{" NL
      << "namespace ast" NL
      << "{" NL
      << "  class Scope;" NL
      << "  /// @defgroup AST" NL;

  for (node = ag::nodes; node->name.length() > 0; node++)
    out << "  struct " << node->name << ";" NL;

  for (node = ag::nodes; node->name.length() > 0; node++)
    out << "  typedef core::RefCountedObject::Ptr<" << node->name
        << "> " << node->name << "Ptr;" NL;

  out NL
    << "# define AST_IS_FLOATING_POINT_TYPE(Type)                        \\" NL
    << "  ((Type)->nodeType == ::ozulis::ast::FloatType::nodeTypeId() || \\" NL
    << "   (Type)->nodeType == ::ozulis::ast::DoubleType::nodeTypeId())" NL NL

    << "# define AST_PTR_CAST(Type, Value)                               \\" NL
    << "  reinterpret_cast< ::ozulis::ast::Type *> (Value.ptr())" NL NL;

  for (node = ag::nodes; !node->name.empty() > 0; node++)
  {
    out << "  /// @ingroup AST" NL
        << "  /// @brief " << node->description NL
        << "  struct " << node->name;
    if (!node->parent.empty())
      out << " : public " << node->parent;
    out NL << "  {" NL
           << "  public:" NL NL
           << "    " << node->name << "();" NL
           << "    virtual ~" << node->name << "();" NL NL
           << "    /// Returns the node name" NL
           << "    virtual const std::string & nodeName() const;" NL
           << "    /// Returns the node type" NL
           << "    static inline core::id_t nodeTypeId() { return core::Id<Node, "
           << node->name << ">::id(); }" NL NL;

    for (attr = node->attrs; !attr->type.empty(); attr++)
    {
      out << "    " << attr->type << " " << attr->name << ";" NL
          << "    typedef " << ag::deref(attr->type) << " " << attr->name << "_t;" NL;
    }
    out << "  };" NL NL;
  }

  out << "} // namespace ast" NL
      << "} // namespace ozulis" NL NL
      << "extern template class std::vector<ozulis::ast::Node *>;" NL
      << "extern template class std::vector<ozulis::ast::Type *>;" NL
      << "extern template class std::vector<ozulis::ast::VarDecl *>;" NL
      << "#endif // !AST_AST_HH" NL;
  out.close();
}

static void
generateAstSource(const char * working_directory)
{
  assert(working_directory);

  std::fstream out;
  std::string path(working_directory);

  path.append("/ast.cc");
  out.open(path.c_str(), std::fstream::out | std::fstream::trunc);
  assert(out.is_open() && out.good());

  ag::appendGeneratedWarning(out);
  out << "#include <ozulis/ast/ast.hh>" NL
      << "#include <ozulis/ast/scope.hh>" NL
      << "#include <ozulis/core/id.cxx>" NL NL
      << "#define NL << std::endl" NL NL
      << "namespace ozulis" NL
      << "{" NL
      << "namespace ast" NL
      << "{" NL;

  const ag::Node *node;
  for (node = ag::nodes; !node->name.empty() > 0; node++)
  {
    bool hasParent = !node->parent.empty();
    out << "  " << node->name << "::" << node->name << "()";
    if (hasParent)
      out NL << "    : " << node->parent << "()";
    for (const ag::Attribute * attr = node->attrs; !attr->type.empty(); ++attr)
      if (hasParent++)
        out << ", " << attr->name << "()";
      else
        out << ": " << attr->name << "()";

    out NL
      << "  {" NL
      << "    nodeType = core::IdP<Node, "
      << (node->name == "Node" ? "Node" : node->parent) << ", "
      << node->name << ">::id();" NL
      << "  }" NL NL;

    out << "  " << node->name << "::~" << node->name << "()" NL
        << "  {" NL;
    for (const ag::Attribute * attr = node->attrs; !attr->type.empty(); ++attr)
      if (ag::needsDelete(attr->type))
        out << "    delete " << attr->name << ";" NL;
    out << "  }" NL NL;

    out << "  const std::string & " << node->name << "::nodeName() const" NL
        << "  {" NL
        << "    static const std::string name__ = \"" << node->name << "\";" NL
        << "    return name__;" NL
        << "  }" NL NL;
  }

  out << "} // namespace ast" NL
      << "} // namespace ozulis" NL
      << "template class std::vector<ozulis::ast::NodePtr>;" NL
      << "template class std::vector<ozulis::ast::TypePtr>;" NL
      << "template class std::vector<ozulis::ast::VarDeclPtr>;" NL;
  out.close();
}


int main(int argc, char **argv)
{
  assert(argc == 2);
  generateAstHeader(argv[1]);
  generateAstSource(argv[1]);
  return 0;
}

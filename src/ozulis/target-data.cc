#include <boost/foreach.hpp>

#include "target-data.hh"

template class std::vector<ozulis::TargetData::TypeAlignment>;

namespace ozulis
{
  const TargetData::TypeAlignment *
  TargetData::find(TargetData::TypeAlignment::Type type, uint8_t size) const
  {
    BOOST_FOREACH(const TargetData::TypeAlignment & align, typeAlignments)
      if (align.type == type && align.size == size)
        return &align;
    return 0;
  }

  const TargetData::TypeAlignment *
  TargetData::findPointer() const
  {
    BOOST_FOREACH(const TargetData::TypeAlignment & align, typeAlignments)
      if (align.type == TypeAlignment::Pointer)
        return &align;
    return 0;
  }
}

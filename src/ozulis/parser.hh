#ifndef LANG_PARSER_HH
# define LANG_PARSER_HH

# include <string>
# include <boost/filesystem/path.hpp>

# include <ozulis/ast/ast.hh>

namespace ozulis
{
  class Parser
  {
  public:
    virtual ~Parser();

    virtual ast::FilePtr parseFile(const boost::filesystem::path & path);
    virtual ast::FilePtr parse(FILE * stream) = 0;
  };
}

#endif /* !LANG_PARSER_HH */

#include <boost/filesystem.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/core/string.hh>
#include <ozulis/parser-factory.hh>
#include <ozulis/plugin-manager.hh>

namespace ozulis
{
  namespace lang
  {
    ast::FilePtr
    ParserFactory::parseFile(const boost::filesystem::path & path)
    {
      const std::string extension = boost::filesystem::extension(path);
      Parser * parser = ::ozulis::PluginManager::instance().createParserByExtension(extension);
      assert_msg(parser, "unhandled language");
      ast::FilePtr file = parser->parseFile(path);
      delete parser;
      return file;
    }
  }
}

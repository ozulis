#include <iostream>
#include <vector>
#include <boost/program_options.hpp>
#include <boost/foreach.hpp>

#include <ozulis/core/assert.hh>
#include <ozulis/compiler.hh>
#include <ozulis/parse-task.hh>

namespace po = boost::program_options;

static void
initGettext()
{
  setlocale (LC_ALL, "");
  bindtextdomain ("ozulis", OZULIS_LOCALEDIR);
  textdomain ("ozulis");
}

int
main(int argc, char **argv)
{
  initGettext();

  po::options_description desc("Help");
  desc.add_options()
    ("help,h", _("produce help message"))
    ("out,o", _("specify the output"))
    ("include-path,I", po::value< std::vector<std::string> >(), _("include path"))
    ("input-file,i", po::value< std::vector<std::string> >(), _("input sources"))
    ("ast-dump", _("dump the AST at each pass"))
    ("ast-dump-parser", _("dump the AST after the parser"))
    ("ast-dump-scope-builder", _("dump the AST after scope-builder"))
    ("ast-dump-type-checker", _("dump the AST after the type checker"))
    ("ast-dump-simplify", _("dump the AST after the simplification"));

  po::positional_options_description pdesc;
  pdesc.add("input-file", -1);

  po::variables_map vm;
  //po::store(po::parse_command_line(argc, argv, desc), vm);
  po::store(po::command_line_parser(argc, argv).
            options(desc).positional(pdesc).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 0;
  }

  ozulis::Compiler & compiler = ozulis::Compiler::instance();

#define SET_OPT(Method, Opt)                    \
  do {                                          \
    if (vm.count(Opt))                          \
      compiler.Method(true);                    \
  } while (0)

  /* compiler settings */
  SET_OPT(enableAstDump, "ast-dump");
  SET_OPT(enableAstDumpParser, "ast-dump-parser");
  SET_OPT(enableAstDumpScopeBuilder, "ast-dump-scope-builder");
  SET_OPT(enableAstDumpTypeChecker, "ast-dump-type-checker");
  SET_OPT(enableAstDumpSimplify, "ast-dump-simplify");

  /* tasks */
  if (vm.count("input-file"))
  {
    BOOST_FOREACH(const std::string & filename,
                  vm["input-file"].as< std::vector<std::string> >())
    {
      ozulis::ParseTask * parseTask = new ozulis::ParseTask();
      parseTask->filename = filename;
      compiler.addTask(parseTask);
    }
  }

  compiler.compile();
  return 0;
}

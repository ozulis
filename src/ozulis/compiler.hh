#ifndef COMPILER_HH
# define COMPILER_HH

# include <vector>
# include <string>
# include <map>

# include <ozulis/core/singleton.hh>
# include <ozulis/ast/ast.hh>

namespace ozulis
{
  class TargetData;
  class Task;
  class ParseTask;

  class Compiler : public core::Singleton<Compiler>
  {
  public:
    Compiler();
    ~Compiler();

    void addTask(Task * task);
    void compile();
    void warning(const std::string & message);
    void error(const std::string & message);
    void abort(); /// @todo keep it ?

    void work(Task & task);
    void work(ParseTask & task);

    inline void enableAstDump(bool enable);
    inline void enableAstDumpParser(bool enable);
    inline void enableAstDumpScopeBuilder(bool enable);
    inline void enableAstDumpTypeChecker(bool enable);
    inline void enableAstDumpSimplify(bool enable);
    inline const TargetData & targetData() const;

  private:
    std::vector<Task *> tasks_;
    std::map<boost::filesystem::path, ast::FilePtr> files_;
    bool astDumpParser_;
    bool astDumpTypeChecker_;
    bool astDumpSimplify_;
    bool astDumpScopeBuilder_;
    const TargetData * targetData_;
  };
}

extern template class ozulis::core::Singleton<ozulis::Compiler>;
extern template class std::vector<ozulis::Task *>;

# include "compiler.hxx"

#endif /* !COMPILER_HH */

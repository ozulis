#ifndef PARSE_TASK_HH
# define PARSE_TASK_HH

# include <string>

# include <ozulis/task.hh>

namespace ozulis
{
  class ParseTask : public Task
  {
  public:
    virtual ~ParseTask();
    virtual void accept(Compiler & compiler);
    virtual std::vector<Task *> * dependancies();

    std::string filename;
  };
}

#endif /* !PARSE_TASK_HH */

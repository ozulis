#ifndef OZULIS_MESSAGE_HH
# define OZULIS_MESSAGE_HH

# include <ozulis/core/id.hh>
# include <ozulis/location.hh>

namespace ozulis
{
  class Message
  {
  public:
    Message();
    virtual ~Message() {}

    static inline core::id_t msgTypeId() { return core::Id<Message, Message>::id(); }

    core::id_t msgType;
  };

  class LocatedMessage : public Message
  {
  public:
    LocatedMessage();

    static inline core::id_t msgTypeId() { return core::Id<Message,
                                           LocatedMessage>::id(); }

    Location location;
  };

  class Warning : public LocatedMessage
  {
  public:
    Warning();

    static inline core::id_t msgTypeId() { return core::Id<Message, Warning>::id(); }

    std::string warning;
  };

  class Error : public LocatedMessage
  {
  public:
    Warning();

    static inline core::id_t msgTypeId() { return core::Id<Message, Error>::id(); }

    std::string error;
  };
}

#endif /* !OZULIS_MESSAGE_HH */

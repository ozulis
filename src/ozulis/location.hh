#ifndef OZULIS_LOCATION_HH
# define OZULIS_LOCATION_HH

# include <string>

namespace ozulis
{
  struct Location
  {
    std::string path;
    uint32_t start_line;
    uint32_t start_col;
    uint32_t end_line;
    uint32_t end_col;
  };
}

#endif /* !OZULIS_LOCATION_HH */

#include <sys/utsname.h>
#include <string.h>

#include <ozulis/core/assert.hh>
#include <ozulis/target-data.hh>
#include <ozulis/target-data-factory.hh>

#define ADD_ALIGNMENT(Type, Size, AbiAlign, PrefAlign)                  \
  data.typeAlignments.push_back(                                        \
    TargetData::TypeAlignment(                                          \
      TargetData::TypeAlignment::Type, Size, AbiAlign, PrefAlign))

namespace ozulis
{
  const TargetData *
  TargetDataFactory::native()
  {
    static const struct {
      const char * machine;
      const TargetData * (*create)();
    } table[] = {
      {"x86_64", TargetDataFactory::amd64},
      {"i686", TargetDataFactory::x86},
      {0, 0}
    };
    struct utsname u;

    assert(!uname(&u));
    for (int i = 0; table[i].machine; i++)
      if (!strcmp(u.machine, table[i].machine))
        return table[i].create();
    assert_msg(false, "unknown machine");
  }

  const TargetData *
  TargetDataFactory::x86()
  {
    static TargetData data;
    static bool init = true;

    if (!init)
      return &data;

    init = false;
    data.isLittleEndian = true;

    ADD_ALIGNMENT(Pointer, 64, 64, 64);

    ADD_ALIGNMENT(Integer, 1, 8, 8);
    ADD_ALIGNMENT(Integer, 8, 8, 8);
    ADD_ALIGNMENT(Integer, 16, 16, 16);
    ADD_ALIGNMENT(Integer, 32, 32, 32);
    ADD_ALIGNMENT(Integer, 64, 32, 64);

    ADD_ALIGNMENT(Float, 32, 32, 32);
    ADD_ALIGNMENT(Float, 64, 32, 64);
    ADD_ALIGNMENT(Float, 80, 32, 32);

    ADD_ALIGNMENT(Vector, 64, 64, 64);
    ADD_ALIGNMENT(Vector, 128, 128, 128);

    ADD_ALIGNMENT(Agregate, 0, 0, 64);

    return &data;
  }

  const TargetData *
  TargetDataFactory::amd64()
  {
    static TargetData data;
    static bool init = true;

    if (!init)
      return &data;

    init = false;
    data.isLittleEndian = true;

    ADD_ALIGNMENT(Pointer, 64, 64, 64);

    ADD_ALIGNMENT(Integer, 1, 8, 8);
    ADD_ALIGNMENT(Integer, 8, 8, 8);
    ADD_ALIGNMENT(Integer, 16, 16, 16);
    ADD_ALIGNMENT(Integer, 32, 32, 32);
    ADD_ALIGNMENT(Integer, 64, 32, 64);

    ADD_ALIGNMENT(Float, 32, 32, 32);
    ADD_ALIGNMENT(Float, 64, 32, 64);
    ADD_ALIGNMENT(Float, 80, 32, 32);

    ADD_ALIGNMENT(Vector, 64, 64, 64);
    ADD_ALIGNMENT(Vector, 128, 128, 128);

    ADD_ALIGNMENT(Agregate, 0, 0, 64);

    return &data;
  }
}


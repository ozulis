#ifndef TARGET_DATA_HH
# define TARGET_DATA_HH

# include <stdint.h>
# include <vector>

namespace ozulis
{
  struct TargetData
  {
    struct TypeAlignment
    {
      enum Type {
        Pointer = 'p',
        Integer = 'i',
        Float = 'f',
        Vector = 'v',
        Agregate = 'a',
      };

      TypeAlignment(Type _type,
                    uint8_t _size,
                    uint8_t _abiAlignment,
                    uint8_t _preferedAlignment)
        : type(_type),
          size(_size),
          abiAlignment(_abiAlignment),
          preferedAlignment(_preferedAlignment)
      {}

      Type type;
      uint8_t size;
      uint8_t abiAlignment;
      uint8_t preferedAlignment;
    };

    const TargetData::TypeAlignment * find(TargetData::TypeAlignment::Type type, uint8_t size) const;
    const TargetData::TypeAlignment * findPointer() const;
    inline const TypeAlignment * findInteger(uint8_t size) const;
    inline const TypeAlignment * findFloat(uint8_t size) const;
    inline const TypeAlignment * findVector(uint8_t size) const;
    inline const TypeAlignment * findAgregate(uint8_t size) const;

    bool isLittleEndian;
    std::vector<TypeAlignment> typeAlignments;
  };
}

extern template class std::vector<ozulis::TargetData::TypeAlignment>;

#include "target-data.hxx"

#endif /* !TARGET_DATA_HH */

#include <ozulis/ast/ast.hh>
#include <ozulis/core/assert.hh>

#include "bison/parser.hh"
#include "bison/lexer.hh"
#include "parser.hh"

int mugiwara_parse(yyscan_t yyscanner, ozulis::ast::FilePtr & module);

namespace mugiwara
{
  ozulis::ast::FilePtr
  Parser::parse(FILE * stream)
  {
    ozulis::ast::FilePtr file;
    yyscan_t scanner;

    mugiwara_lex_init(&scanner);
    mugiwara_set_in(stream, scanner);
    assert_msg(!mugiwara_parse(scanner, file), "parse error");
    mugiwara_lex_destroy(scanner);
    return file;
  }
}

#ifndef LANG_MUGIWARA_PLUGIN_HH
# define LANG_MUGIWARA_PLUGIN_HH

# include <ozulis/plugin.hh>

namespace mugiwara
{
  class MugiwaraPlugin : public ozulis::LanguagePlugin
  {
  public:
    virtual const std::string & name() const;
    virtual const std::string & version() const;
    virtual const std::string & languageName() const;
    virtual const std::vector<std::string> & extensions() const;
    virtual ozulis::Parser * createParser();
  };
}

#endif /* !LANG_MUGIWARA_PLUGIN_HH */

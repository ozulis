#include "mugiwara-plugin.hh"
#include "parser.hh"

namespace mugiwara
{
  const std::string & MugiwaraPlugin::name() const
  {
    static const std::string name_("lang.mugiwara");
    return name_;
  }

  const std::string & MugiwaraPlugin::version() const
  {
    static const std::string version_("0.5");
    return version_;
  }

  const std::string & MugiwaraPlugin::languageName() const
  {
    static const std::string name_("Mugiwara");
    return name_;
  }

  const std::vector<std::string> & MugiwaraPlugin::extensions() const
  {
    static std::vector<std::string> extensions_;
    static bool init = 1;

    if (init)
    {
      extensions_.push_back(".mgw");
      init = 0;
    }

    return extensions_;
  }

  ozulis::Parser * MugiwaraPlugin::createParser()
  {
    return new mugiwara::Parser();
  }
}

EXPORT_PLUGIN(mugiwara::MugiwaraPlugin)

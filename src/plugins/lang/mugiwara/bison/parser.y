%glr-parser
%pure-parser
%locations
%start file
%defines
%error-verbose
%name-prefix "mugiwara_"

%lex-param {yyscan_t yyscanner}
%parse-param {yyscan_t yyscanner}
%parse-param {ozulis::ast::FilePtr & file}

%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <iostream>
#include <algorithm>

#include <ozulis/core/assert.hh>
#include <ozulis/ast/ast.hh>
#include <ozulis/ast/node-factory.hh>

#include "parser.hh"
#include "lexer.hh"

  static void yyerror(YYLTYPE                *yyloccp,
                      yyscan_t             /*yyscanner*/,
                      ozulis::ast::FilePtr & file,
                      const char             *str)
  {
    if (file)
      std::cerr << file->path << ":";
    std::cerr << yyloccp->first_line << "." << yyloccp->first_column << "-"
              << yyloccp->last_line << "." << yyloccp->last_column
              << ": " << str << std::endl;
  }

#define MAKE_BINARY_EXP(Type, Out, Left, Right)                    \
  do {                                                             \
    ozulis::ast::Type * exp = new ozulis::ast::Type;               \
    assert(Left);                                                  \
    assert(Right);                                                 \
    exp = new ozulis::ast::Type;                                   \
    exp->left = (Left);                                            \
    exp->right = (Right);                                          \
    (Out) = exp;                                                   \
  } while (0)

#define MAKE_UNARY_EXP(Type, Out, Exp)                             \
  do {                                                             \
    ozulis::ast::Type * exp = new ozulis::ast::Type;               \
    assert(Exp);                                                   \
    exp = new ozulis::ast::Type;                                   \
    exp->exp = (Exp);                                              \
    (Out) = exp;                                                   \
  } while (0)

static ozulis::ast::Type *
createType(const char * name)
{
  static const struct {
    const char * name;
    bool         isSigned;
    int          size;
  } integerTypes[] = {
    { "int8", true, 8 },
    { "uint8", false, 8 },
    { "int16", true, 16 },
    { "uint16", false, 16 },
    { "int32", true, 32 },
    { "uint32", false, 32 },
    { "int64", true, 64 },
    { "uint64", false, 64 },
    { "int128", true, 128 },
    { "uint128", false, 128 },
    { 0, false, 0 }
  };

  for (int i = 0; integerTypes[i].name; i++)
    if (!strcmp(name, integerTypes[i].name))
    {
      auto itype = new ozulis::ast::IntegerType;
      itype->isSigned = integerTypes[i].isSigned;
      itype->size = integerTypes[i].size;
      return itype;
    }
  if (!strcmp("bool", name))
    return ozulis::ast::NodeFactory::createBoolType();
  if (!strcmp("float", name))
    return ozulis::ast::NodeFactory::createFloatType();
  if (!strcmp("double", name))
    return ozulis::ast::NodeFactory::createDoubleType();
  if (!strcmp("void", name))
    return ozulis::ast::NodeFactory::createVoidType();
  auto type = new ozulis::ast::NamedType;
  type->name = name;
  return type;
}

%}

%union
{
  ozulis::ast::Node *                    node;
  std::vector<ozulis::ast::NodePtr> *    nodes;
  ozulis::ast::Function *                funcDec;
  ozulis::ast::Type *                    type;
  std::vector<ozulis::ast::VarDeclPtr> * varDecls;
  ozulis::ast::VarDecl *                 varDecl;
  ozulis::ast::Block *                   block;
  ozulis::ast::Exp *                     exp;
  std::vector<ozulis::ast::ExpPtr> *     exps;
  ozulis::ast::NumberExp *               nbExp;

  int    number;
  char * string;
  bool   boolean;
}

%token <nbExp> NUMBER
%token <string> ID STRING
%token EQEQ "=="
%token NEQ "!="
%token LTEQ "<="
%token GTEQ ">="
%token SHL "<<"
%token ASHR ">>>"
%token LSHR ">>"
%token OROR "||"
%token ANDAND "&&"
%token GOTO "goto"
%token CONST "const"
%token CAST "cast"
%token RETURN "return"
%token IF "if"
%token ELSE "else"
%token WHILE "while"
%token DO "do"
%token FOR "for"
%token ALIAS "alias"
%token TYPEDEF "typedef"

%type <nodes> decls
%type <node> decl
%type <type> type
%type <varDecls> var_decls func_params func_params_non_empty
%type <varDecl> var_decl
%type <node> func_decl alias_decl typedef_decl
%type <block> block
%type <nodes> statements
%type <node> statement label_statement goto_statement
%type <node> if_statement else_statement while_statement
%type <node> do_while_statement return_statement
%type <node> exp_statement
%type <exp> exp assign_exp oror_exp andand_exp or_exp xor_exp and_exp
%type <exp> cmp_exp shift_exp add_exp mul_exp unary_exp
%type <exp> call_exp
%type <exps> call_exp_args call_exp_args_non_empty

%%

file: decls {
  file = new ozulis::ast::File;
  file->decls = $1;
  if (!file->varDecls)
    file->varDecls = new ozulis::ast::File::varDecls_t;
  std::reverse(file->decls->begin(), file->decls->end());
};

decls: decl decls {
  assert($1);
  assert($2);
  $$ = $2;
  $$->push_back($1);
 } | /* epsilon */ { $$ = new std::vector<ozulis::ast::NodePtr>; };

decl: func_decl { $$ = $1; }
| alias_decl { $$ = $1; }
| typedef_decl { $$ = $1; }
;

alias_decl: "alias" ID '=' type {
  auto alias = new ozulis::ast::Alias;
  alias->name = $2;
  alias->type = $4;
  $$ = alias;
 };

typedef_decl: "typedef" ID '=' type {
  auto node = new ozulis::ast::Typedef;
  node->name = $2;
  node->type = $4;
  $$ = node;
 };

func_decl: type ID '(' func_params ')' ';' {
  auto func = new ozulis::ast::FunctionDecl;
  func->returnType         = $1;
  func->name               = $2;
  func->args               = $4;
  $$                       = func;
  free($2);
} | type ID '(' func_params ')' block {
  auto func = new ozulis::ast::Function;
  func->returnType     = $1;
  func->name           = $2;
  func->args           = $4;
  func->block          = $6;
  $$                   = func;
  free($2);
};

func_params: func_params_non_empty { $$ = $1; }
| /* epsilon */ { $$ = new std::vector<ozulis::ast::VarDeclPtr>; };

func_params_non_empty: var_decl {
  $$ = new std::vector<ozulis::ast::VarDeclPtr>;
  $$->push_back($1);
} | func_params_non_empty ',' var_decl {
  $$ = $1;
  $$->push_back($3);
};

block: '{' var_decls statements '}' {
  $$ = new ozulis::ast::Block;
  $$->varDecls = $2;
  $$->statements = $3;
 };

var_decls: var_decls var_decl ';' {
  assert($1);
  assert($2);
  $$ = $1;
  $$->push_back($2);
 } | /* epsilon */ { $$ = new std::vector<ozulis::ast::VarDeclPtr>; };

var_decl: type ID {
  $$ = new ozulis::ast::VarDecl;
  $$->type = $1;
  $$->name = $2;
  free($2);
};

statements: statements statement {
  assert($1);
  assert($2);
  $$ = $1;
  $$->push_back($2);
} | /* epsilon */ { $$ = new std::vector<ozulis::ast::NodePtr>; };

statement: exp_statement { $$ = $1; }
| label_statement { $$ = $1; }
| goto_statement { $$ = $1; }
| if_statement { $$ = $1; }
| while_statement { $$ = $1; }
| do_while_statement { $$ = $1; }
| return_statement { $$ = $1; }
| block { $$ = $1; }
| ';' { $$ = new ozulis::ast::EmptyStatement; };

exp_statement: exp ';' {
  assert($1);
  $$ = $1;
};

label_statement: ID ':' {
  auto label = new ozulis::ast::Label;
  label->name        = $1;
  $$                 = label;
  free($1);
};

goto_statement: "goto" ID ';' {
  auto gt = new ozulis::ast::Goto;
  gt->label      = $2;
  $$             = gt;
  free($2);
 };

if_statement: "if" '(' exp ')' statement else_statement {
  auto ifStmt = new ozulis::ast::If;
  assert($3);
  assert($5);
  assert($6);
  ifStmt->branch       = new ozulis::ast::ConditionalBranch;
  ifStmt->branch->cond = $3;
  ifStmt->trueBlock    = $5;
  ifStmt->falseBlock   = $6;
  $$                   = ifStmt;
};

else_statement: "else" statement {
  $$ = $2;
} | /* epsylon */ { $$ = new ozulis::ast::EmptyStatement; };

while_statement: "while" '(' exp ')' statement {
  auto whileStmt = new ozulis::ast::While;
  assert($3);
  assert($5);
  whileStmt->branch       = new ozulis::ast::ConditionalBranch;
  whileStmt->branch->cond = $3;
  whileStmt->block        = $5;
  $$                      = whileStmt;
};

do_while_statement: "do" statement "while" '(' exp ')' ';' {
  auto doWhileStmt = new ozulis::ast::DoWhile;
  assert($2);
  assert($5);
  doWhileStmt->branch       = new ozulis::ast::ConditionalBranch;
  doWhileStmt->branch->cond = $5;
  doWhileStmt->block        = $2;
  $$                        = doWhileStmt;
};

return_statement: "return" ';' {
  auto ret = new ozulis::ast::Return;
  ret->exp          = new ozulis::ast::VoidExp;
  ret->exp->type    = new ozulis::ast::VoidType;
  $$                = ret;
} | "return" exp ';' {
  auto ret = new ozulis::ast::Return;
  ret->exp          = $2;
  $$                = ret;
}

type: ID {
  $$          = createType($1);
  free($1);
} | CONST type {
  $2->isConst = true;
  $$ = $2;
} | '@' type {
  auto type = new ozulis::ast::PointerType;
  type->type = $2;
  $$ = type;
} | '[' NUMBER ']' type {
  auto type = new ozulis::ast::ArrayType;
  type->type = $4;
  type->size = $2->number;
  $$ = type;
}

exp: assign_exp {
  assert($1);
  $$ = $1;
 };

assign_exp: exp '=' oror_exp {
  auto exp = new ozulis::ast::AssignExp;
  exp->dest            = $1;
  exp->value           = $3;
  $$                   = exp;
 } | oror_exp { assert($1); $$ = $1; };

oror_exp: oror_exp "||" andand_exp { MAKE_BINARY_EXP(OrOrExp, $$, $1, $3); }
| andand_exp { assert($1); $$ = $1; };

andand_exp: andand_exp "&&" or_exp { MAKE_BINARY_EXP(AndAndExp, $$, $1, $3); }
| or_exp { assert($1); $$ = $1; };

or_exp: or_exp '|' xor_exp { MAKE_BINARY_EXP(OrExp, $$, $1, $3); }
| xor_exp { assert($1); $$ = $1; };

xor_exp: xor_exp '^' and_exp { MAKE_BINARY_EXP(XorExp, $$, $1, $3); }
| and_exp { assert($1); $$ = $1; };

and_exp: and_exp '&' cmp_exp { MAKE_BINARY_EXP(AndExp, $$, $1, $3); }
| cmp_exp { assert($1); $$ = $1; };

cmp_exp: shift_exp "==" shift_exp { MAKE_BINARY_EXP(EqExp, $$, $1, $3); }
| shift_exp "!=" shift_exp { MAKE_BINARY_EXP(NeqExp, $$, $1, $3); }
| shift_exp '<' shift_exp { MAKE_BINARY_EXP(LtExp, $$, $1, $3); }
| shift_exp "<=" shift_exp { MAKE_BINARY_EXP(LtEqExp, $$, $1, $3); }
| shift_exp '>' shift_exp { MAKE_BINARY_EXP(GtExp, $$, $1, $3); }
| shift_exp ">=" shift_exp { MAKE_BINARY_EXP(GtEqExp, $$, $1, $3); }
| shift_exp { assert($1); $$ = $1; };

shift_exp: shift_exp "<<" add_exp { MAKE_BINARY_EXP(ShlExp, $$, $1, $3); }
| shift_exp ">>>" add_exp { MAKE_BINARY_EXP(AShrExp, $$, $1, $3); }
| shift_exp ">>" add_exp { MAKE_BINARY_EXP(LShrExp, $$, $1, $3); }
| add_exp { assert($1); $$ = $1; };

add_exp: add_exp '+' mul_exp { MAKE_BINARY_EXP(AddExp, $$, $1, $3); }
| add_exp '-' mul_exp { MAKE_BINARY_EXP(SubExp, $$, $1, $3); }
| mul_exp { assert($1); $$ = $1; };

mul_exp: mul_exp '*' unary_exp { MAKE_BINARY_EXP(MulExp, $$, $1, $3); }
| mul_exp '/' unary_exp { MAKE_BINARY_EXP(DivExp, $$, $1, $3); }
| mul_exp '%' unary_exp { MAKE_BINARY_EXP(ModExp, $$, $1, $3); }
| unary_exp { assert($1); $$ = $1; };

unary_exp: NUMBER { $$ = $1; }
| ID {
  auto exp = new ozulis::ast::IdExp;
  exp->id                  = $1;
  $$                       = exp;
  free($1);
} | STRING {
  auto exp = new ozulis::ast::StringExp;
  exp->string          = $1;
  $$                   = exp;
  free($1);
} | '(' exp ')' { assert($2); $$ = $2; }
| '!' unary_exp { MAKE_UNARY_EXP(BangExp, $$, $2); }
| '~' unary_exp { MAKE_UNARY_EXP(NotExp, $$, $2); }
| '-' unary_exp { MAKE_UNARY_EXP(NegExp, $$, $2); }
| call_exp { $$ = $1; }
| "cast" '(' type ',' exp ')' {
  auto cast = new ozulis::ast::CastExp;
  assert($3);
  assert($5);
  cast->type = $3;
  cast->exp  = $5;
  $$         = cast;
} | unary_exp '$' {
  auto exp = new ozulis::ast::DereferenceExp;
  exp->exp = $1;
  $$ = exp;
} | '@' unary_exp {
  auto exp = new ozulis::ast::AtExp;
  exp->exp = $2;
  $$ = exp;
} | unary_exp '[' exp ']' {
  auto exp = new ozulis::ast::DereferenceByIndexExp;
  exp->exp = $1;
  exp->index = $3;
  $$ = exp;
};

call_exp: ID '(' call_exp_args ')' {
  assert($1);
  assert($3);

  auto callExp = new ozulis::ast::CallExp;
  auto id = new ozulis::ast::IdExp;
  id->id = $1;
  callExp->function = id;
  callExp->args = $3;
  $$ = callExp;
  free($1);
 };

call_exp_args: call_exp_args_non_empty { $$ = $1; }
| /* empty */ { $$ = new std::vector<ozulis::ast::ExpPtr>; };

call_exp_args_non_empty: exp {
  assert($1);
  $$ = new std::vector<ozulis::ast::ExpPtr>;
  $$->push_back($1);
} | call_exp_args_non_empty ',' exp {
  assert($1);
  assert($3);
  $1->push_back($3);
  $$ = $1;
};

%%

%option reentrant noyywrap batch stack
%option bison-bridge bison-locations yylineno
%option warn
%option prefix="mugiwara_"

%{
#include <stdio.h>
#include <stdlib.h>

#include <ozulis/core/assert.hh>
#include <ozulis/ast/ast.hh>
#include <ozulis/ast/node-factory.hh>
#include "parser.hh"

#define YY_USER_ACTION                                  \
   yylloc->first_line = yylloc->last_line;              \
   yylloc->last_line = yylineno;                        \
   yylloc->first_column = yylloc->last_column;          \
   yylloc->last_column += yyleng;

#define MAKE_BOOL(Value)                                                \
   do {                                                                 \
     yylval->nbExp = new ozulis::ast::NumberExp();                      \
     yylval->nbExp->type = ozulis::ast::NodeFactory::createBoolType();  \
     yylval->nbExp->number = Value;                                     \
     return NUMBER;                                                     \
   } while (0)

static ozulis::ast::NumberExp *
makeInteger(double value, int size, bool isSigned);

static ozulis::ast::NumberExp *
makeFloat(double value);

%}

%x comment
%x rcomment

%%

\n                      /* ignore end of line */yylloc->last_column = 0;
[ \t]+                  /* ignore whitespace */;
\/\/.*$                 /* line comment */
#.*$                    /* line comment */

"/*"                    yy_push_state(comment, yyscanner);
<comment>[^\*]+         /* eat */
<comment>\*+[^\*\/]     /* eat */
<comment>\*+\/          yy_pop_state(yyscanner);

<INITIAL,rcomment>"/+"  yy_push_state(rcomment, yyscanner);
<rcomment>[^\+\/]+      /* eat */
<rcomment>\+/[^\/]      /* eat */
<rcomment>\//[^\+]      /* eat */
<rcomment>"+/"          yy_pop_state(yyscanner);

;                       return ';';
\.                      return '.';
:                       return ':';
,                       return ',';

=                       return '=';
==                      return EQEQ;
!=                      return NEQ;
\<                      return '<';
\<=                     return LTEQ;
>                       return '>';
>=                      return GTEQ;

\(                      return '(';
\)                      return ')';

\[                      return '[';
\]                      return ']';

\{                      return '{';
\}                      return '}';

\+                      return '+';
-                       return '-';
\*                      return '*';
\/                      return '/';
%                       return '%';

\|                      return '|';
&                       return '&';
\^                      return '^';
\<\<                    return SHL;
>>                      return LSHR;
>>>                     return ASHR;
~                       return '~';

!                       return '!';
\|\|                    return OROR;
&&                      return ANDAND;

@                       return '@';
\$                      return '$';

\"[^\"]*\"              {
  yytext[strlen(yytext) - 1] = '\0';
  yylval->string = strdup(yytext + 1);
  return STRING;
}

const                   return CONST;
goto                    return GOTO;
cast                    return CAST;
if                      return IF;
else                    return ELSE;
while                   return WHILE;
do                      return DO;
for                     return FOR;
return                  return RETURN;
alias                   return ALIAS;
typedef                 return TYPEDEF;
true                    MAKE_BOOL(1);
false                   MAKE_BOOL(0);

[a-zA-Z][a-zA-Z0-9_]*   yylval->string = strdup(yytext); return ID;

([0-9]+)?\.[0-9]+    {
  yylval->nbExp = makeFloat(strtod(yytext, 0));
  return NUMBER;
}

[0-9]+    {
  yylval->nbExp = makeInteger(strtod(yytext, 0), 32, true);
  return NUMBER;
}

[0-9]+U    {
  yylval->nbExp = makeInteger(strtod(yytext, 0), 32, false);
  return NUMBER;
}

[0-9]+L    {
  yylval->nbExp = makeInteger(strtod(yytext, 0), 64, true);
  return NUMBER;
}

[0-9]+UL    {
  yylval->nbExp = makeInteger(strtod(yytext, 0), 64, false);
  return NUMBER;
}

.               return *yytext;

%%

static ozulis::ast::NumberExp *
makeInteger(double value, int size, bool isSigned)
{
  ozulis::ast::NumberExp * nbExp = new ozulis::ast::NumberExp();
  ozulis::ast::IntegerType * nbType = new ozulis::ast::IntegerType();
  nbType->size = size;
  nbType->isSigned = isSigned;
  nbExp->type = nbType;
  nbExp->number = value;
  return nbExp;
}

static ozulis::ast::NumberExp *
makeFloat(double value)
{
  ozulis::ast::NumberExp * nbExp = new ozulis::ast::NumberExp();
  ozulis::ast::FloatType * nbType = new ozulis::ast::FloatType();
  nbExp->type = nbType;
  nbExp->number = value;
  return nbExp;
}

ADD_SUBDIRECTORY(bison)
ADD_LIBRARY(lang-mugiwara SHARED
  parser.cc parser.hh
  mugiwara-plugin.cc mugiwara-plugin.hh)
TARGET_LINK_LIBRARIES(lang-mugiwara lang-mugiwara-bison ozulis-core ozulis-ast)
INSTALL(TARGETS lang-mugiwara DESTINATION lib/ozulis/)

#ifndef MUGIWARA_PARSER_HH
# define MUGIWARA_PARSER_HH

# include <ozulis/parser.hh>

namespace mugiwara
{
  class Parser : public ozulis::Parser
  {
    virtual ozulis::ast::FilePtr parse(FILE * stream);
  };
}

#endif /* !MUGIWARA_PARSER_HH */

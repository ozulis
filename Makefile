all:

BUILDDIR = build
MAKEOPT = --quiet

-include config.mk

all:
	test -d $(BUILDDIR) || ./configure --build-dir=$(BUILDDIR)
	make $(MAKEOPT) -C $(BUILDDIR) generators
	make $(MAKEOPT) -C $(BUILDDIR)

install: all
	make $(MAKEOPT) -C $(BUILDDIR) install

doc:
	$(DOXYGEN) doc/Doxyfile && cp doc/tabs.css doc/html/

make_check = \
test -d reports || mkdir reports && \
ts=$$(date +%s); \
nice -n 15 ./moulette $1 \
           --out-xml reports/report-$${ts}.xml \
           --out-json reports/report-$${ts}.json \
| tee reports/report-$${ts}.out && \
cd reports && \
ln -sf report-$${ts}.xml report-latest.xml && \
ln -sf report-$${ts}.json report-latest.json && \
ln -sf report-$${ts}.out report-latest.out

check: install
	@echo
	@echo "    ---------------------------------"
	@echo "    |          TEST SUITE           |"
	@echo "    ---------------------------------"
	@echo
	$(call make_check,tests/test-suite tests/lang/mugiwara/expr-test-suite)

check-no-expr: install
	$(call make_check,tests/test-suite)

clean:
	make $(MAKEOPT) -C $(BUILDDIR) clean
	find . \( -name '*~' \) -exec rm -f {} \;

distclean:
	rm -rf $(BUILDDIR) config.mk

.PHONY: doc help

help:
	@echo "build		build the project"
	@echo "check		check the project"
	@echo "clean		clean the project"
	@echo "distclean	distclean the project"
	@echo "doc		generate the doxygen html documentation"
	@echo "lighttpd-start	starts the lighttpd server"
	@echo "lighttpd-restart	restarts the lighttpd server"
	@echo "lighttpd-stop	stops the lighttpd server"

doc/lighttpd/local.conf: Makefile
	echo var.doc_dir = \"$$PWD/doc\" >$@

lighttpd-start: doc/lighttpd/local.conf
	lighttpd -f doc/lighttpd/lighttpd.conf

lighttpd-stop:
	killall -15 lighttpd

lighttpd-restart: lighttpd-stop lighttpd-start

clean: tests-clean
tests-clean:
	find tests \( -name '*.ll' -o -name '*.bc' -o -name '*.bin' \) -exec rm {} \;
	for i in tests/lang/mugiwara/input/expr/*-*-*; do i=$${i/.mgw/}; if [[ -f $$i ]] ; then rm $$i; fi; done

remove-trailing-whitespaces:
	find src -type f -exec sed -i 's/ \+$$//g' {} \;

tags-vim:
	ctags -R --c++-kinds=+p --fields=+iaS --extra=+q src /usr/include/c++/4.3.3/ /usr/include/llvm && mv tags $@

tags-emacs:
	find src \( -name '*.hh' -o -name '*.hxx' -o -name '*.cc' \) -print0 | xargs -0 ctags.emacs -T --members --globals --declarations -o$@

messages:
	cd po && find ../src/ \( -name '*.cc' -o -name '*.hh' -o -name '*.h' -o -name '*.c' -o -name '*.hxx' -o -name '*.cxx' \) -print0 | xargs -0 xgettext -o ozulis.pot  --default-domain=ozulis --keyword=_ --foreign-user --package-name=ozulis --package-version=0.6 --msgid-bugs-address=bique.alexandre@gmail.com

.PHONY: tags-vim tags-emacs messages

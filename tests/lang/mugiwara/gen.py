#! /usr/bin/python

unary_ops = {
    '-':'neg'
}

unary_bitwise_ops = {
    '!':'bang',
    '~':'not'
}

binary_ops = {
    '+':'add',
    '-':'sub',
    '/':'div',
    '*':'mul',
    '%':'mod',
    '==':'eq',
    '!=':'ne',
    '>':'gt',
    '>=':'ge',
    '<':'lt',
    '<=':'le'
    }

binary_bitwise_ops = {
    '|':'or',
    '&':'and',
    '^':'xor',
    '&&':'andand',
    '||':'oror',
    '<<':'shl',
    '>>':'lshr',
    '>>>':'ashr'
}

# todo: not bang
int_types = {
    'bool':'true',
    'int8':'13',
    'uint8':'13',
    'int16':'13',
    'uint16':'13',
    'int32':'13',
    'uint32':'13U',
    'int64':'13L',
    'uint64':'13UL'
    }

float_types = {
    'float':'0.2',
    'double':'0.42'
}

number_types = dict(int_types.items() + float_types.items())

def unary_exp(folder, name, type, val, op):
    filename = folder + '/' + name + '-' + type + '.mgw'
    content = "int32 main()\n" + "{\n" + \
        "  " + op + "cast(" + type + ", " + val + ");\n  return 0;\n}\n"
    file = open(filename, "w")
    file.write(content)
    file.close()

def binary_exp(folder, name, type1, val1, type2, val2, op):
    filename = folder + '/' + name + '-' + type1 + '-' + type2 + '.mgw'
    content = "int32 main()\n" + "{\n" + \
        "  cast(" + type1 + ", " + val1 + ") " + op + \
        " cast(" + type2 + ", " + val2 + ");\n  return 0;\n}\n"
    file = open(filename, "w")
    file.write(content)
    file.close()

for op, name in binary_ops.iteritems():
    for type1, val1 in number_types.iteritems():
        for type2, val2 in number_types.iteritems():
            binary_exp('expr', name, type1, val1, type2, val2, op)

for op, name in binary_bitwise_ops.iteritems():
    for type1, val1 in int_types.iteritems():
        for type2, val2 in int_types.iteritems():
            binary_exp('expr', name, type1, val1, type2, val2, op)

for op, name in binary_bitwise_ops.iteritems():
    for type1, val1 in float_types.iteritems():
        for type2, val2 in number_types.iteritems():
            binary_exp('bad-expr', name, type1, val1, type2, val2, op)

for op, name in unary_bitwise_ops.iteritems():
    for type, val in float_types.iteritems():
        unary_exp('bad-expr', name, type, val, op)
    for type, val in int_types.iteritems():
        unary_exp('expr', name, type, val, op)

for op, name in unary_ops.iteritems():
    for type, val in number_types.iteritems():
        unary_exp('expr', name, type, val, op)
